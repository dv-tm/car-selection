Handlebars.registerHelper('equals', function (one, two, options)
{
  if (one == two) {
    return options.fn(this);
  }

  return options.inverse(this);
});

Handlebars.registerHelper('currency', function (money)
{
  return parseInt(money).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1 ').split('.')[0];
});

Handlebars.registerHelper('dateFormat', function (ms)
{
  return UTL.date.format(ms);
});

Money = {
  format: function (money)
  {
    return parseInt(money).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1 ').split('.')[0];
  },

  parse: function (money)
  {
    if (money) {
      return money.split(' ').join('');
    }

    return 'ошибка';
  }
};

UTL = {};

UTL.date = {
  format: function (ms)
  {
    var date = new Date(ms);

    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    var hour = date.getHours();
    var min = date.getMinutes();
    var sec = date.getSeconds();

    day < 10 && (day = '0' + day);
    month < 10 && (month = '0' + month);
    hour < 10 && (hour = '0' + hour);
    min < 10 && (min = '0' + min);
    sec < 10 && (sec = '0' + sec);

    return day + '.' + month + '.' + year + ' ' + hour + ':' + min + ':' + sec;
  }
};

Request = {
  execute: function (hash, params, callback)
  {
    var $this = this;
    var contentType = 'application/json; charset=UTF-8';

    if (typeof params == 'object') {
      contentType = 'application/x-www-form-urlencoded; charset=UTF-8';
    }

    $.ajax({
      url: PATH + hash,
      type: 'POST',
      contentType: contentType,
      data: params,
      dataType: 'json',
      async: true,

      success: function (data)
      {
        $this.handler(data, callback);
      },

      error: function (xhr)
      {
        if (xhr.status === 200) {
          var login = xhr.getResponseHeader('login-page');

          if (login && login == 'true') {
            window.location.reload();
            return; // Только обновление.
          }
        }

        var data = {
          errors: ['Произошла неизвестная ошибка.']
        };

        $this.handler(data, callback);
      }
    });
  },

  handler: function (data, callback)
  {
    if (!data.errors) {
      data.errors = [];
    }

    Errors.show(data.errors);

    if (typeof callback == 'function') {
      callback(data);
    }
  }
};

Template = function (path)
{
  var $this = this;

  $this.path = path;
  $this.source = null;

  $this.render = function (data)
  {
    $this.load();
    return $this.source(data);
  };

  $this.load = function (async)
  {
    if ($this.source) {
      return;
    }

    $.ajax({
      url: PATH + $this.path,
      success: function (data)
      {
        $this.source = Handlebars.compile(data);
      },
      dataType: 'text',
      async: async === true
    });
  };
};

Templates = {
  PARAM_PANE: new Template('templates/params/param_pane.hbs'),
  PARAM_LIST: new Template('templates/params/param_list.hbs'),
  PARAM_VERSIONS: new Template('templates/params/param_versions.hbs'),
  PARAM_PERFECTS: new Template('templates/params/param_perfects.hbs'),
  PARAM_BINDS: new Template('templates/params/param_binds.hbs'),

  EVENT_PANE: new Template('templates/events/event_pane.hbs'),
  EVENT_LIST: new Template('templates/events/event_list.hbs'),
  EVENT_DESC: new Template('templates/events/event_desc.hbs'),

  SCENARIO_PANE: new Template('templates/scenarios/scenario_pane.hbs'),
  SCENARIO_LIST: new Template('templates/scenarios/scenario_list.hbs'),

  SEQUENCE_PANE: new Template('templates/sequences/sequence_pane.hbs'),
  SEQUENCE_LIST: new Template('templates/sequences/sequence_list.hbs'),

  CALCULATION: new Template('templates/events/calc_result.hbs')
};

Errors = {
  show: function (errors)
  {
    var $this = this;

    for (var i = errors.length - 1; i > -1; i--) {
      $('.menu').after('<div class="alert ico-close">' + errors[i] + '</div>');
    }

    $(window).scrollTop(0, 0);
  },

  hide: function ()
  {
    $('.alert').remove();
  },

  bind: function ()
  {
    $(document).on('click', '.alert', function (event)
    {
      var $alert = $(this);

      var w = $alert.outerWidth();
      var x = event.offsetX;

      if (x > w - 30) {
        $alert.remove();
      }
    });
  }
};

Users = {
  update: function (id)
  {
    var $user = $('#user_' + id);

    var user = {
      id: id,
      alias: $user.find('.user-alias').val(),
      password: $user.find('.user-password').val() || null
    };

    var params = JSON.stringify(user);

    Request.execute('users/update', params, function (data)
    {
      if (!data.errors.length) {
        //
      }
    });
  }
};

Users = {
  update: function (id)
  {
    var $this = this;
    $this.view.wait(id);

    var $user = $('#user_' + id);

    var user = {
      id: id,
      alias: $user.find('.user-alias').val(),
      password: $user.find('.user-password').val()
    };

    var params = JSON.stringify(user);

    Request.execute('users/update', params, function (data)
    {
      if (!data.errors.length) {
        $user.find('.user-info-alias').text($.trim(user.alias));
        $this.view.norm(id);
      } else {
        $this.view.edit(id);
      }
    });
  },

  remove: function (id)
  {
    var $this = this;
    $this.view.wait(id);

    Request.execute('users/delete', {id: id}, function (data)
    {
      if (!data.errors.length) {
        $('#user_' + id).remove();
      } else {
        $this.view.edit(id);
      }
    });
  },

  view: {
    norm: function (id)
    {
      var $user = $('#user_' + id);
      var $user_update = $user.find('.user-update');
      var $user_delete = $user.find('.user-delete');
      var $user_info = $user.find('.user-info');
      var $user_alias = $user.find('.user-alias');
      var $user_username = $user.find('.user-username');
      var $user_password = $user.find('.user-password');

      $user_update.removeClass('btn-blue ico-refresh').addClass('btn-green ico-update');
      $user_delete.removeClass('btn-red').addClass('btn-gray');
      $user_alias.parent().hide();
      $user_username.parent().hide();
      $user_password.parent().hide();
      $user_info.parent().show();
    },

    wait: function (id)
    {
      var $user = $('#user_' + id);
      var $user_update = $user.find('.user-update');

      $user_update.removeClass('btn-green ico-update').addClass('btn-blue ico-refresh');
    },

    edit: function (id)
    {
      var $user = $('#user_' + id);
      var $user_update = $user.find('.user-update');
      var $user_delete = $user.find('.user-delete');
      var $user_info = $user.find('.user-info');
      var $user_alias = $user.find('.user-alias');
      var $user_username = $user.find('.user-username');
      var $user_password = $user.find('.user-password');

      $user_update.removeClass('btn-green ico-refresh').addClass('btn-blue ico-update');
      $user_delete.removeClass('btn-gray').addClass('btn-red');
      $user_info.parent().hide();
      $user_alias.parent().show();
      $user_username.parent().show();
      $user_password.parent().show();
    }
  },

  binds: function ()
  {
    var $this = this;

    $(document).on('click', '.user-update', function ()
    {
      var $user_update = $(this);
      var $user = $user_update.parents('tr');

      var id = $user.data('id');

      if ($user_update.hasClass('btn-blue')) {
        $this.update(id);
      } else {
        $this.view.edit(id);
      }
    });

    $(document).on('click', '.user-delete', function ()
    {
      var $user_delete = $(this);
      var $user = $user_delete.parents('tr');

      var id = $user.data('id');

      if ($user_delete.hasClass('btn-red')) {
        $this.remove(id);
      }
    });

    $(document).on('keyup', '.user-alias, .user-username, .user-password', function (event)
    {
      var $user = $(this).parents('tr');
      var code = event.keyCode || event.which;

      if (code == 13) {
        $user.find('.user-update').click();
      }

      if (code == 27) {
        $this.view.norm($user.data('id'));
      }
    });
  }
};

Params = {
  list: function ()
  {
    Request.execute('params/list', {}, function (data)
    {
      if (!data.errors.length) {
        $('#param_pane').html(Templates.PARAM_PANE.render(data));
        $('#param_list').html(Templates.PARAM_LIST.render(data));
      }
    });
  },

  insert: function ()
  {
    var $param = $('#param');
    var $param_insert = $param.find('.param-insert');

    $param_insert.removeClass('ico-plus').addClass('ico-refresh');

    var param = {
      title: $param.find('.param-title').val(),
      uid: $param.find('.param-uid').val(),
      min: $param.find('.param-min').val(),
      max: $param.find('.param-max').val(),
      minDelta: $param.find('.param-minDelta').val(),
      maxDelta: $param.find('.param-maxDelta').val(),
      quality: $param.find('.param-quality').val(),
      type: $param.find('.param-type').val(),
      ma: $param.find('.param-ma').val(),
      mv: $param.find('.param-mv').val(),
      mk: $param.find('.param-mk').val()
    };

    var params = JSON.stringify(param);

    Request.execute('params/insert', params, function (data)
    {
      if (!data.errors.length) {
        $('#param_pane').html(Templates.PARAM_PANE.render(data));
      }

      $('#param_list').html(Templates.PARAM_LIST.render(data));
      $param_insert.removeClass('ico-refresh').addClass('ico-plus');
    });
  },

  update: function (id)
  {
    var $this = this;
    $this.view.wait(id);

    var $param = $('#param_' + id);

    var param = {
      id: id,
      title: $param.find('.param-title').val(),
      uid: $param.find('.param-uid').val(),
      min: $param.find('.param-min').val(),
      max: $param.find('.param-max').val(),
      minDelta: $param.find('.param-minDelta').val(),
      maxDelta: $param.find('.param-maxDelta').val(),
      quality: $param.find('.param-quality').val(),
      type: $param.find('.param-type').val(),
      ma: $param.find('.param-ma').val(),
      mv: $param.find('.param-mv').val(),
      mk: $param.find('.param-mk').val()
    };

    var params = JSON.stringify(param);

    Request.execute('params/update', params, function (data)
    {
      if (!data.errors.length) {
        $param.find('.param-info').text($.trim(param.title));
        $this.view.norm(id);
      } else {
        $this.view.edit(id);
      }
    });
  },

  remove: function (id)
  {
    var $this = this;
    $this.view.wait(id);

    Request.execute('params/delete', {param: id}, function (data)
    {
      if (!data.errors.length) {
        $('#param_' + id).remove();
      } else {
        $this.view.edit(id);
      }
    });
  },

  view: {
    norm: function (id)
    {
      var $param = $('#param_' + id);
      var $param_update = $param.find('.param-update');
      var $param_delete = $param.find('.param-delete');
      var $param_info = $param.find('.param-info');
      var $param_title = $param.find('.param-title');

      $param.find('input, .ui-select-value').attr('disabled', true);
      $param_update.removeClass('btn-blue ico-refresh').addClass('btn-green ico-update');
      $param_delete.removeClass('btn-red').addClass('btn-gray');

      $param_title.hide();
      $param_info.show();
    },

    wait: function (id)
    {
      var $param = $('#param_' + id);
      var $param_update = $param.find('.param-update');

      $param_update.removeClass('btn-green ico-update').addClass('btn-blue ico-refresh');
    },

    edit: function (id)
    {
      var $param = $('#param_' + id);
      var $param_update = $param.find('.param-update');
      var $param_delete = $param.find('.param-delete');
      var $param_info = $param.find('.param-info');
      var $param_title = $param.find('.param-title');

      $param.find('input, .ui-select-value').attr('disabled', false);
      $param_update.removeClass('btn-green ico-refresh').addClass('btn-blue ico-update');
      $param_delete.removeClass('btn-gray').addClass('btn-red');

      $param_info.hide();
      $param_title.show();
    }
  },

  binds: function ()
  {
    var $this = this;

    $(document).on('click', '.param-insert', function ()
    {
      $this.insert();
    });

    $(document).on('click', '.param-update', function ()
    {
      var $param_update = $(this);
      var $param = $param_update.parents('tr');

      var id = $param.data('param');

      if ($param_update.hasClass('btn-blue')) {
        $this.update(id);
      } else {
        $this.view.edit(id);
      }
    });

    $(document).on('click', '.param-delete', function ()
    {
      var $param_delete = $(this);
      var $param = $param_delete.parents('tr');

      var id = $param.data('param');

      if ($param_delete.hasClass('btn-red')) {
        $this.remove(id);
      }
    });

    $(document).on('keydown', '.param-title', function (event)
    {
      var $param = $(this).parents('tr');
      var code = event.keyCode || event.which;

      if (code == 13) {
        $param.find('.param-update').click();
      }

      if (code == 27) {
        $this.view.norm($param.data('param'));
      }
    });

    $(document).on('click', '.cw-param-types .ui-select-option', function ()
    {
      var $option = $(this);
      var $param = $option.parents('tr');

      var type = ParamType[$option.attr('value')];

      if (type == ParamType.STATIC) {
        return; // TODO: Статика не реализована.

        $param.find('.cw-param-range').hide();
        $param.find('.cw-param-quality').hide();
        $param.find('.cw-param-radicals').show();
        $param.find('.cw-param-entries').show();
      } else {
        $param.find('.cw-param-entries').hide();
        $param.find('.cw-param-radicals').hide();
        $param.find('.cw-param-range').show();
        $param.find('.cw-param-quality').show();
      }
    });
  }
};

Versions = {

  entity: {
    id: null,
    type: 'EVENT',
    params: []
  },

  show: function (id, type)
  {
    Request.execute('versions/show', {id: id, type: type}, function (data)
    {
      if (!data.errors.length) {
        Versions.entity = {id: id, type: type, params: data.params};
        $('#versions').html(Templates.PARAM_VERSIONS.render(data));
      }
    });
  },

  insert: function ()
  {
    var $this = this;

    var $param = $('.params .ui-select-value');
    var $parent = $param.parents('tr');

    $this.unbinds();
    $parent.find('.version-insert').toggleClass('ico-plus ico-refresh');

    var param = $this.entity.params[$param.attr('value')];

    var version = {
      param: {id: param.id},

      event: $this.entity.type == 'EVENT' ? {id: $this.entity.id} : null,
      scenario: $this.entity.type == 'SCENARIO' ? {id: $this.entity.id} : null,
      sequence: $this.entity.type == 'SEQUENCE' ? {id: $this.entity.id} : null,

      min: $parent.find('.version-min').val(),
      max: $parent.find('.version-max').val(),
      minDelta: $parent.find('.version-minDelta').val(),
      maxDelta: $parent.find('.version-maxDelta').val()
    };

    var params = JSON.stringify(version);

    Request.execute('versions/insert', params, function (data)
    {
      if (!data.errors.length) {
        $this.show($this.entity.id, $this.entity.type);
      }

      $this.binds();
    });
  },

  update: function (id)
  {
    var $this = this;
    $this.view.wait(id);

    var $version = $('#version_' + id);

    var version = {
      id: id,
      min: $version.find('.version-min').val(),
      max: $version.find('.version-max').val(),
      minDelta: $version.find('.version-minDelta').val(),
      maxDelta: $version.find('.version-maxDelta').val()
    };

    var params = JSON.stringify(version);

    Request.execute('versions/update', params, function (data)
    {
      if (!data.errors.length) {
        $this.view.norm(id);
      } else {
        $this.view.edit(id);
      }
    });
  },

  remove: function (id)
  {
    var $this = this;
    $this.view.wait(id);

    Request.execute('versions/delete', {version: id}, function ()
    {
      $this.show($this.entity.id, $this.entity.type);
    });
  },

  view: {
    norm: function (id)
    {
      var $version = $('#version_' + id);
      var $version_update = $version.find('.version-update');
      var $version_delete = $version.find('.version-delete');

      $version.find('.version-min').attr('disabled', true);
      $version.find('.version-max').attr('disabled', true);
      $version.find('.version-minDelta').attr('disabled', true);
      $version.find('.version-maxDelta').attr('disabled', true);

      $version_update.removeClass('btn-blue ico-refresh').addClass('btn-green ico-update');
      $version_delete.removeClass('btn-red').addClass('btn-gray');
    },

    wait: function (id)
    {
      var $version = $('#version_' + id);
      var $version_update = $version.find('.version-update');

      $version_update.removeClass('btn-green ico-update').addClass('btn-blue ico-refresh');
    },

    edit: function (id)
    {
      var $version = $('#version_' + id);
      var $version_update = $version.find('.version-update');
      var $version_delete = $version.find('.version-delete');

      $version.find('.version-min').attr('disabled', false);
      $version.find('.version-max').attr('disabled', false);
      $version.find('.version-minDelta').attr('disabled', false);
      $version.find('.version-maxDelta').attr('disabled', false);

      $version_update.removeClass('btn-green ico-refresh').addClass('btn-blue ico-update');
      $version_delete.removeClass('btn-gray').addClass('btn-red');
    }
  },

  binds: function ()
  {
    var $this = this;

    $(document).on('click', '.version-insert', function ()
    {
      $this.insert();
    });

    $(document).on('click', '.params .ui-select-option', function ()
    {
      var $param = $(this);
      var $parent = $param.parents('tr');

      var param = $this.entity.params[$param.attr('value')];

      $parent.find('.version-min').val(param.min);
      $parent.find('.version-max').val(param.max);
      $parent.find('.version-minDelta').val(param.minDelta);
      $parent.find('.version-maxDelta').val(param.maxDelta);
      $parent.find('.version-type').val(param.type.title);
    });

    $(document).on('click', '.version-update', function ()
    {
      var $version_update = $(this);
      var $version = $version_update.parents('tr');

      var version = $version.data('version');

      if ($version_update.hasClass('btn-blue')) {
        $this.update(version);
      } else {
        $this.view.edit(version);
      }
    });

    $(document).on('click', '.version-delete', function ()
    {
      var $version_delete = $(this);
      var $version = $version_delete.parents('tr');

      var version = $version.data('version');

      if ($version_delete.hasClass('btn-red')) {
        $this.remove(version);
      }
    });
  },

  unbinds: function ()
  {
    $(document).off('click', '.version-insert');
    $(document).off('click', '.params .ui-select-option');
    $(document).off('click', '.version-update');
    $(document).off('click', '.version-delete');
  }
};

Events = {
  list: function ()
  {
    Request.execute('events/list', {}, function (data)
    {
      if (!data.errors.length) {
        $('#event_pane').html(Templates.EVENT_PANE.render(data));
        $('#event_list').html(Templates.EVENT_LIST.render(data));
      }
    });
  },

  one: function (id)
  {
    Request.execute('events/one', {event: id}, function (data)
    {
      if (!data.errors.length) {
        if (data.event.scenarios) {
          // Поиск отмеченных в эвенте сценариев.
          for (var i = 0; i < data.event.scenarios.length; i++) {
            var eventScenario = data.event.scenarios[i];

            for (var j = 0; j < data.scenarios.length; j++) {
              var scenario = data.scenarios[j];
              if (eventScenario.id == scenario.id) {
                scenario.active = true;
                break;
              }
            }
          }
        }

        data.versions = data.event.params;

        $('#event_desc').html(Templates.EVENT_DESC.render(data));

        Versions.entity = {id: id, type: 'EVENT', params: data.params};
        $('#versions').html(Templates.PARAM_VERSIONS.render(data));
      }
    });
  },

  insert: function ()
  {
    var $event = $('#event');
    var $event_insert = $event.find('.event-insert');

    $event_insert.removeClass('ico-plus').addClass('ico-refresh');

    var event = {
      title: $event.find('.event-title').val(),
      radicals: $event.find('.event-radicals').val(),
      modality: $event.find('.event-modality').val(),
      money: $event.find('.event-money').val()
    };

    var params = JSON.stringify(event);

    Request.execute('events/insert', params, function (data)
    {
      if (!data.errors.length) {
        $('#event_pane').html(Templates.EVENT_PANE.render(data));
      }

      $('#event_list').html(Templates.EVENT_LIST.render(data));
      $event_insert.removeClass('ico-refresh').addClass('ico-plus');
    });
  },

  update: function (id)
  {
    var $this = this;
    $this.view.wait(id);

    var $event = $('#event_' + id);

    var event = {
      id: id,
      title: $event.find('.event-title').val(),
      radicals: $event.find('.event-radicals').val(),
      modality: $event.find('.event-modality').val(),
      money: $event.find('.event-money').val()
    };

    var params = JSON.stringify(event);

    Request.execute('events/update', params, function (data)
    {
      if (!data.errors.length) {
        $event.find('.event-info').text($.trim(event.title));
        $this.view.norm(id);
      } else {
        $this.view.edit(id);
      }
    });
  },

  merge: function (id)
  {
    var $scenarios = $('#scenarios').find('.ui-select-option.is-active');

    var scenarios = $scenarios.map(function ()
    {
      return {id: $(this).attr('value')};
    }).get();

    var event = {
      id: id,
      scenarios: scenarios
    };

    var params = JSON.stringify(event);

    Request.execute('events/merge', params, function (data)
    {
      if (!data.errors.length) {
        /** @namespace data.perfects */
        for (var i = 0; i < data.perfects.length; i++) {
          var perfect = data.perfects[i];
          perfect.static = (ParamType[perfect.type] == ParamType.STATIC);
        }

        $('#perfects').html(Templates.PARAM_PERFECTS.render(data));
      }
    });
  },

  remove: function (id)
  {
    var $this = this;
    $this.view.wait(id);

    Request.execute('events/delete', {event: id}, function (data)
    {
      if (!data.errors.length) {
        $('#event_' + id).remove();
      } else {
        $this.view.edit(id);
      }
    });
  },

  calculate: function (id)
  {
    window.open(PATH + 'events/searchCarXlsx?event=' + id);
    //Request.execute('events/searchCar', {event: id}, function (data)
    //{
    //  if (!data.errors.length) {
    //    $('#calculation').html(Templates.CALCULATION.render(data));
    //  } else {
    //    $('#calculation').html('');
    //  }
    //});
  },

  view: {
    norm: function (id)
    {
      var $event = $('#event_' + id);

      var $event_update = $event.find('.event-update');
      var $event_delete = $event.find('.event-delete');
      var $event_info = $event.find('.event-info');
      var $event_title = $event.find('.event-title');
      var $event_money = $event.find('.event-money');

      $event.find('input, .ui-select-value').attr('disabled', true);
      $event_update.removeClass('btn-blue ico-refresh').addClass('btn-green ico-update');
      $event_delete.removeClass('btn-red').addClass('btn-gray');

      $event_title.hide();
      $event_info.show();

      $event_money.val(Money.format($event_money.val()));
    },

    wait: function (id)
    {
      var $event = $('#event_' + id);
      var $event_update = $event.find('.event-update');

      $event_update.removeClass('btn-green ico-update').addClass('btn-blue ico-refresh');
    },

    edit: function (id)
    {
      var $event = $('#event_' + id);

      var $event_update = $event.find('.event-update');
      var $event_delete = $event.find('.event-delete');
      var $event_info = $event.find('.event-info');
      var $event_title = $event.find('.event-title');
      var $event_money = $event.find('.event-money');

      $event.find('input, .ui-select-value').attr('disabled', false);
      $event_update.removeClass('btn-green ico-refresh').addClass('btn-blue ico-update');
      $event_delete.removeClass('btn-gray').addClass('btn-red');

      $event_info.hide();
      $event_title.show();

      $event_money.val(Money.parse($event_money.val()));
    }
  },

  binds: function ()
  {
    var $this = this;

    $(document).on('click', '.event-insert', function ()
    {
      $this.insert();
    });

    $(document).on('click', '.event-update', function ()
    {
      var $event_update = $(this);
      var $event = $event_update.parents('tr');

      var id = $event.data('event');

      if ($event_update.hasClass('btn-blue')) {
        $this.update(id);
      } else {
        $this.view.edit(id);
      }
    });

    $(document).on('click', '.event-delete', function ()
    {
      var $event_delete = $(this);
      var $event = $event_delete.parents('tr');

      var id = $event.data('event');

      if ($event_delete.hasClass('btn-red')) {
        $this.remove(id);
      }
    });

    $(document).on('click', '.event-info', function ()
    {
      var id = $(this).parents('tr').data('event');
      window.location.href = PATH + 'events/show?event=' + id;
    });

    $(document).on('keydown', '.event-title', function (event)
    {
      var $event = $(this).parents('tr');
      var code = event.keyCode || event.which;

      if (code == 13) {
        $event.find('.event-update').click();
      }

      if (code == 27) {
        $this.view.norm($event.data('event'));
      }
    });

    $(document).on('click', '.event-merge', function ()
    {
      $this.merge($(this).data('event'));
    });

    $(document).on('click', '#perfects-tab', function ()
    {
      $('#calculation').hide();
      $('#perfects').show();
    });

    $(document).on('click', '#calculation-tab', function ()
    {
      $('#perfects').hide();
      $('#calculation').html(loading).show();

      $this.calculate($(this).data('event'));
    });
  }
};

Scenarios = {
  list: function ()
  {
    Request.execute('scenarios/list', {}, function (data)
    {
      if (!data.errors.length) {
        $('#scenario_pane').html(Templates.SCENARIO_PANE.render(data));
        $('#scenario_list').html(Templates.SCENARIO_LIST.render(data));
      }
    });
  },

  insert: function ()
  {
    var $scenario = $('#scenario');
    var $scenario_insert = $scenario.find('.scenario-insert');

    $scenario_insert.removeClass('ico-plus').addClass('ico-refresh');

    var scenario = {
      uid: $scenario.find('.scenario-uid').val(),
      title: $scenario.find('.scenario-title').val()
    };

    var params = JSON.stringify(scenario);

    Request.execute('scenarios/insert', params, function (data)
    {
      if (!data.errors.length) {
        $('#scenario_pane').html(Templates.SCENARIO_PANE.render(data));
      }

      $('#scenario_list').html(Templates.SCENARIO_LIST.render(data));
      $scenario_insert.removeClass('ico-refresh').addClass('ico-plus');
    });
  },

  update: function (id)
  {
    var $this = this;
    $this.view.wait(id);

    var $scenario = $('#scenario_' + id);

    var scenario = {
      id: id,
      uid: $scenario.find('.scenario-uid').val(),
      title: $scenario.find('.scenario-title').val()
    };

    var params = JSON.stringify(scenario);

    Request.execute('scenarios/update', params, function (data)
    {
      if (!data.errors.length) {
        $scenario.find('.scenario-info').text($.trim(scenario.title));
        $this.view.norm(id);
      } else {
        $this.view.edit(id);
      }
    });
  },

  remove: function (id)
  {
    var $this = this;
    $this.view.wait(id);

    Request.execute('scenarios/delete', {scenario: id}, function (data)
    {
      if (!data.errors.length) {
        $('#scenario_' + id).remove();
      } else {
        $this.view.edit(id);
      }
    });
  },

  view: {
    norm: function (id)
    {
      var $scenario = $('#scenario_' + id);
      var $scenario_update = $scenario.find('.scenario-update');
      var $scenario_delete = $scenario.find('.scenario-delete');
      var $scenario_info = $scenario.find('.scenario-info');
      var $scenario_title = $scenario.find('.scenario-title');

      $scenario.find('input, .ui-select-value').attr('disabled', true);
      $scenario_update.removeClass('btn-blue ico-refresh').addClass('btn-green ico-update');
      $scenario_delete.removeClass('btn-red').addClass('btn-gray');

      $scenario_title.hide();
      $scenario_info.show();
    },

    wait: function (id)
    {
      var $scenario = $('#scenario_' + id);
      var $scenario_update = $scenario.find('.scenario-update');

      $scenario_update.removeClass('btn-green ico-update').addClass('btn-blue ico-refresh');
    },

    edit: function (id)
    {
      var $scenario = $('#scenario_' + id);
      var $scenario_update = $scenario.find('.scenario-update');
      var $scenario_delete = $scenario.find('.scenario-delete');
      var $scenario_info = $scenario.find('.scenario-info');
      var $scenario_title = $scenario.find('.scenario-title');

      $scenario.find('input, .ui-select-value').attr('disabled', false);
      $scenario_update.removeClass('btn-green ico-refresh').addClass('btn-blue ico-update');
      $scenario_delete.removeClass('btn-gray').addClass('btn-red');

      $scenario_info.hide();
      $scenario_title.show();
    }
  },

  binds: function ()
  {
    var $this = this;

    $(document).on('click', '.scenario-insert', function ()
    {
      $this.insert();
    });

    $(document).on('click', '.scenario-update', function ()
    {
      var $scenario_update = $(this);
      var $scenario = $scenario_update.parents('tr');

      var id = $scenario.data('scenario');

      if ($scenario_update.hasClass('btn-blue')) {
        $this.update(id);
      } else {
        $this.view.edit(id);
      }
    });

    $(document).on('click', '.scenario-delete', function ()
    {
      var $scenario_delete = $(this);
      var $scenario = $scenario_delete.parents('tr');

      var id = $scenario.data('scenario');

      if ($scenario_delete.hasClass('btn-red')) {
        $this.remove(id);
      }
    });

    $(document).on('keydown', '.scenario-title', function (event)
    {
      var $scenario = $(this).parents('tr');
      var code = event.keyCode || event.which;

      if (code == 13) {
        $scenario.find('.scenario-update').click();
      }

      if (code == 27) {
        $this.view.norm($scenario.data('scenario'));
      }
    });

    $(document).on('click', '.scenario-info', function ()
    {
      var id = $(this).parents('tr').data('scenario');
      window.location.href = PATH + 'scenarios/show?scenario=' + id;
    });
  }
};

Sequences = {
  list: function ()
  {
    Request.execute('sequences/list', {}, function (data)
    {
      if (!data.errors.length) {
        $('#sequence_pane').html(Templates.SEQUENCE_PANE.render(data));
        $('#sequence_list').html(Templates.SEQUENCE_LIST.render(data));
      }
    });
  },

  insert: function ()
  {
    Errors.hide();

    var $sequence = $('#sequence');
    var $sequence_insert = $sequence.find('.sequence-insert');

    $sequence_insert.removeClass('ico-plus').addClass('ico-refresh');

    var sequence = {
      title: $sequence.find('.sequence-title').val(),
      radicals: $sequence.find('.sequence-radicals').val(),
      ARule: $sequence.find('.sequence-ARule').val(),
      BRule: $sequence.find('.sequence-BRule').val(),
      PRule: $sequence.find('.sequence-PRule').val()
    };

    var params = JSON.stringify(sequence);

    Request.execute('sequences/insert', params, function (data)
    {
      if (!data.errors.length) {
        $('#sequence_pane').html(Templates.SEQUENCE_PANE.render(data));
      }

      $('#sequence_list').html(Templates.SEQUENCE_LIST.render(data));
      $sequence_insert.removeClass('ico-refresh').addClass('ico-plus');
    });
  },

  update: function (id)
  {
    var $this = this;
    $this.view.wait(id);

    var $sequence = $('#sequence_' + id);

    var sequence = {
      id: id,
      title: $sequence.find('.sequence-title').val(),
      radicals: $sequence.find('.sequence-radicals').val(),
      ARule: $sequence.find('.sequence-ARule').val(),
      BRule: $sequence.find('.sequence-BRule').val(),
      PRule: $sequence.find('.sequence-PRule').val()
    };

    var params = JSON.stringify(sequence);

    Request.execute('sequences/update', params, function (data)
    {
      if (!data.errors.length) {
        $sequence.find('.sequence-info').text($.trim(sequence.title));
        $this.view.norm(id);
      } else {
        $this.view.edit(id);
      }
    });
  },

  remove: function (id)
  {
    var $this = this;
    $this.view.wait(id);

    Request.execute('sequences/delete', {sequence: id}, function (data)
    {
      if (!data.errors.length) {
        $('#sequence_' + id).remove();
      } else {
        $this.view.edit(id);
      }
    });
  },

  view: {
    norm: function (id)
    {
      var $sequence = $('#sequence_' + id);
      var $sequence_update = $sequence.find('.sequence-update');
      var $sequence_delete = $sequence.find('.sequence-delete');
      var $sequence_info = $sequence.find('.sequence-info');
      var $sequence_title = $sequence.find('.sequence-title');

      $sequence.find('input, .ui-select-value').attr('disabled', true);
      $sequence_update.removeClass('btn-blue ico-refresh').addClass('btn-green ico-update');
      $sequence_delete.removeClass('btn-red').addClass('btn-gray');

      $sequence_title.hide();
      $sequence_info.show();
    },

    wait: function (id)
    {
      var $sequence = $('#sequence_' + id);
      var $sequence_update = $sequence.find('.sequence-update');

      $sequence_update.removeClass('btn-green ico-update').addClass('btn-blue ico-refresh');
    },

    edit: function (id)
    {
      var $sequence = $('#sequence_' + id);
      var $sequence_update = $sequence.find('.sequence-update');
      var $sequence_delete = $sequence.find('.sequence-delete');
      var $sequence_info = $sequence.find('.sequence-info');
      var $sequence_title = $sequence.find('.sequence-title');

      $sequence.find('input, .ui-select-value').attr('disabled', false);
      $sequence_update.removeClass('btn-green ico-refresh').addClass('btn-blue ico-update');
      $sequence_delete.removeClass('btn-gray').addClass('btn-red');

      $sequence_info.hide();
      $sequence_title.show();
    }
  },

  binds: function ()
  {
    var $this = this;

    $(document).on('click', '.sequence-insert', function ()
    {
      $this.insert();
    });

    $(document).on('click', '.sequence-update', function ()
    {
      var $sequence_update = $(this);
      var $sequence = $sequence_update.parents('tr');

      var id = $sequence.data('sequence');

      if ($sequence_update.hasClass('btn-blue')) {
        $this.update(id);
      } else {
        $this.view.edit(id);
      }
    });

    $(document).on('click', '.sequence-delete', function ()
    {
      var $sequence_delete = $(this);
      var $sequence = $sequence_delete.parents('tr');

      var id = $sequence.data('sequence');

      if ($sequence_delete.hasClass('btn-red')) {
        $this.remove(id);
      }
    });

    $(document).on('keydown', '.sequence-title', function (event)
    {
      var $sequence = $(this).parents('tr');
      var code = event.keyCode || event.which;

      if (code == 13) {
        $sequence.find('.sequence-update').click();
      }

      if (code == 27) {
        $this.view.norm($sequence.data('sequence'));
      }
    });

    $(document).on('click', '.sequence-info', function ()
    {
      var id = $(this).parents('tr').data('sequence');
      window.location.href = PATH + 'sequences/show?sequence=' + id;
    });
  }
};

Binds = {

  pairs: function (propId, paramId)
  {
    if (!propId) {
      return; // Идентификатор характеристики обязателен.
    }

    var params = {
      propId: propId,
      paramId: paramId || null
    };

    Request.execute('params/pairs', params, function (data)
    {
      if (!data.errors.length) {
        $('#prop-binds').replaceWith(Templates.PARAM_BINDS.render(data));
      }
    });
  },

  binds: function ()
  {
    var $this = this;

    $(document).on('click', '.prop-groups .ui-select-option', function ()
    {
      $('.prop-group').hide();
      $('#group_' + $(this).attr('value')).show();

      UI.scrolls.init();
    });

    $(document).on('click', '.prop-group .ui-select-option', function ()
    {
      var $prop = $(this);
      var $param = $('#params').find('.ui-select-value');

      var prop = $prop.attr('value');
      var param = $param.attr('value');

      $this.pairs(prop, param);
    });

    $(document).on('click', '#params .ui-select-option', function ()
    {
      var $prop = $('.prop-group:visible').find('.ui-select-value');
      var $param = $(this);

      var propId = $prop.attr('value');
      var paramId = $param.attr('value');

      $this.pairs(propId, paramId);
    });
  }
};

ParamType = {
  PERCENT: 'Процентный',
  DISCRETE: 'Дискретный',
  STATIC: 'Статический'
};

var loading = '<div class="ico-refresh">Выполнение операции...</div>';

Users.binds();
Params.binds();
Versions.binds();
Events.binds();
Scenarios.binds();
Sequences.binds();
Binds.binds();

$(document).ready(function ()
{
  Errors.bind();
});