UI = {};

UI.checkbox = {
  binds: function ()
  {
    $(document).on('click keydown', '.ui-checkbox', function (event)
    {
      var code = event.keyCode || event.which;

      if (event.type == 'click' || code == 32) {
        var $checkbox = $(this);

        $checkbox.toggleClass('ico-check');
        var checked = $checkbox.hasClass('ico-check');

        $checkbox.find('input[type="hidden"]').val(checked);
      }
    });

    $(document).on('click', '.ui-checkbox-title', function ()
    {
      $(this).prev('.ui-checkbox').click();
    });
  }
};

UI.select = {
  binds: function ()
  {
    $(document).on('click', '.ui-select-option', function ()
    {
      var $option = $(this);
      var $select = $option.parents('.ui-select, .ui-select-list');
      var $value = $select.find('.ui-select-value');
      var $options = $select.find('.ui-select-option');

      if ($select.hasClass("is-multiple")) {
        $(this).toggleClass('is-active');
      } else {
        $options.removeClass('is-active');
        $option.addClass('is-active');

        var value = $option.attr('value');

        $value.attr('value', value).html($option.html());
        $select.find('input[type="hidden"]').val(value);
      }
    });

    $(document).on('click', '.ui-select .ui-select-value', function ()
    {
      var $value = $(this);

      if ($value.is('[disabled]')) {
        return; // Заблокированный элемент.
      }

      var $select = $value.parents('.ui-select');
      var $options = $select.find('.ui-select-options');

      $options.toggle().is('[size]') && UI.scrolls.init();
    });

    $(document).on('click', function (event)
    {
      var $element = $(event.target);

      if ($element.hasClass('ui-select-value')) {
        var $options = $element.parents('.ui-select').find('.ui-select-options');
        $('.ui-select .ui-select-options').not($options).hide();
      } else {
        $('.ui-select .ui-select-options').hide();
      }
    });
  }
};

UI.scrolls = {
  init: function ($elements)
  {
    if (!$elements || !$elements.length) {
      $elements = $('.ui-select[size], .ui-select-list[size]')
        .find('.ui-select-options:visible:not(".nano")');
    }

    $elements.each(function ()
    {
      var $element = $(this).addClass('nano');
      $element.html('<div class="nano-content">' + $element.html() + '</div>');
    });

    $elements.nanoScroller()
      .find('.nano-pane:hidden').css('display', 'block')
      .find('.nano-slider').css('height', '100%');
  }
};

UI.tabs = {
  binds: function ()
  {
    $(document).on('click', '.ui-tab:not(".is-active")', function ()
    {
      var $tab = $(this);
      var $tabs = $tab.parents('.ui-tabs');

      $tabs.find('.ui-tab').removeClass('is-active');
      $tab.addClass('is-active');
    });
  }
};

UI.file = {
  binds: function ()
  {
    $(document).on('click', '.ui-file-open', function ()
    {
      $(this).parent().find('.ui-file').click();
    });

    $(document).on('change', '.ui-file', function ()
    {
      var $file = $(this);

      var name = $file.val().split(/(\\|\/)/g).pop();
      $file.parent().find('.ui-file-name').val(name);

      // TODO: Перенести в функцию handler.
      var $button = $file.parent().find('[class*="-import"]');

      if ($file.val()) {
        $button.removeClass('btn-gray').addClass('btn-red');
      } else {
        $button.removeClass('btn-red').addClass('btn-gray');
      }
    });

    $(document).on('click', '.ui-file-export', function ()
    {
      var action = $(this).data('action');
      window.open(PATH + action, '_self');
    });

    $(document).on('click', '.ui-file-import', function ()
    {
      var $import = $(this);

      if ($import.hasClass('btn-red')) {
        var action = $import.data('action');
        var $file = $import.parent().find('.ui-file');

        var $form = $('<form method="POST">');
        $form.attr('action', PATH + action);
        $form.attr('enctype', 'multipart/form-data');
        $form.append($file).submit();
      }
    });
  }
};

UI.select.binds();
UI.checkbox.binds();
UI.tabs.binds();
UI.file.binds();