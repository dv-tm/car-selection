<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ include file="/pages/base/header.jsp" %>

<div class="content-title">Эвенты</div>

<div id="event_pane"></div>
<div id="event_list"></div>

<script type="text/javascript">Events.list();</script>

<%@ include file="/pages/base/footer.jsp" %>