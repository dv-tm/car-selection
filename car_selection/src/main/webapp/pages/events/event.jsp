<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ include file="/pages/base/header.jsp" %>

<script type="text/javascript">Events.one(${param.event});</script>

<div id="event_desc"></div>
<div id="versions"></div>

<div class="ui-tabs">
  <div id="perfects-tab" class="ui-tab is-active">Идеальная сущность</div>
  <div id="calculation-tab" class="ui-tab" data-event="${param.event}">Расчет</div>
</div>

<div id="perfects"></div>
<div id="calculation" style="display: none;"></div>

<%@ include file="/pages/base/footer.jsp" %>