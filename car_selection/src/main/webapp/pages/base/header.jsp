<%@ page import="app.DataMapper" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%!
  private String uri;

  private String active(String page)
  {
    return uri.contains("/" + page) ? " active" : "";
  }
%>

<%
  uri = request.getRequestURI();
  DataMapper mapper = new DataMapper();
%>

<!doctype html>
<html lang="ru">

<head>
  <meta charset="UTF-8">
  <meta name=viewport content="width=640">

  <title>Конфигуратор</title>

  <link type="text/css" rel="stylesheet" href="<c:url value='/styles/ui-basic.css'/>">
  <link type="text/css" rel="stylesheet" href="<c:url value='/styles/ui-custom.css'/>">
  <link type="text/css" rel="stylesheet" href="<c:url value='/styles/application.css'/>">

  <script type="text/javascript">var PATH = "<c:url value='/'/>";</script>
  <script type="text/javascript" src="<c:url value='/scripts/jquery-1.11.2.min.js'/>"></script>
  <script type="text/javascript" src="<c:url value='/scripts/handlebars-v3.0.3.js'/>"></script>
  <script type="text/javascript" src="<c:url value='/scripts/nanoscroller.min.js'/>"></script>
  <script type="text/javascript" src="<c:url value='/scripts/ui-elements.js'/>"></script>
  <script type="text/javascript" src="<c:url value='/scripts/application.js'/>"></script>
</head>

<body>

<div class="content">

  <div class="menu pane ui-box">
    <div class="menu-logo" style="width: 104px;">Конфигуратор</div>
    <div class="menu-item<%=active("events")%>" style="width: 74px;">
      <a href="<c:url value="/events/show"/>" class="ico-events">Эвенты</a>
    </div>
    <div class="menu-item<%=active("scenarios")%>" style="width: 88px;">
      <a href="<c:url value="/scenarios/show"/>" class="ico-scenarios">Сценарии</a>
    </div>
    <div class="menu-item<%=active("sequences")%>" style="width: 94px;">
      <a href="<c:url value="/sequences/show"/>" class="ico-sequences">Секвенции</a>
    </div>
    <div class="menu-item menu-down<%=active("params")%>" style="width: 98px;">
      <a href="<c:url value="/params/show"/>" class="ico-params">Параметры</a>
      <div class="menu-down-items">
        <div class="menu-down-item">
          <a href="<c:url value="/params/binds"/>" class="ico-binds">Привязка</a>
        </div>
      </div>
    </div>
    <div class="menu-item last<%=active("users")%>" style="width: 119px;">
      <a href="<c:url value="/users/list"/>" class="ico-users">Пользователи</a>
    </div>
    <div class="menu-logout" style="width: 57px;">
      <a href="<c:url value="/logout"/>" class="ico-logout">Выйти</a>
    </div>
  </div>

  <c:if test="${! empty errors}">
    <c:forEach items="${errors}" var="error">
      <div class="alert ico-close">${error}</div>
    </c:forEach>
  </c:if>

  <div class="content-page ui-box">