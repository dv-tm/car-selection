<%@ page import="app.Exceptions" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%
  request.setAttribute("exceptions", new Exceptions()); // Обработчик ошибок.
  response.setHeader("login-page", "true"); // Указатель на страницу для AJAX запросов.
%>

<!doctype html>
<html lang="ru">

<head>
  <meta charset="UTF-8">
  <meta name=viewport content="width=320">

  <title>Конфигуратор</title>

  <link type="text/css" rel="stylesheet" href="<c:url value='/styles/ui-basic.css'/>">
  <link type="text/css" rel="stylesheet" href="<c:url value='/styles/ui-custom.css'/>">
  <link type="text/css" rel="stylesheet" href="<c:url value='/styles/application.css'/>">

  <script type="text/javascript" src="<c:url value='/scripts/jquery-1.11.2.min.js'/>"></script>
  <script type="text/javascript" src="<c:url value='/scripts/ui-elements.js'/>"></script>
  <script type="text/javascript" src="<c:url value='/scripts/application.js'/>"></script>
</head>

<body style="overflow: hidden;">

<div class="auth">
  <c:if test="${not empty param.error}">
    <div class="auth-alert alert ico-close">
      <c:out value='${exceptions.auth(sessionScope["SPRING_SECURITY_LAST_EXCEPTION"])}'/>
    </div>
  </c:if>

  <form class="auth-form pane" action="<c:url value='/auth'/>" method="POST">
    <label class="ico-username ui-row">
      <input name="username" type="text" size="" placeholder="Логин" value="">
    </label>
    <label class="ico-password ui-row">
      <input name="password" type="password" size="" placeholder="Пароль" value="">
    </label>
    <div class="ui-row">
      <button class="ui-checkbox ico-check" type="button">
        <input name="remember" type="hidden" value="true">
      </button>
      <div class="ui-checkbox-title" style="width: 75px;">Запомнить</div>
      <button name="authorize" type="submit" class="btn btn-menu btn-text ico-login" style="width: 89px;">Войти</button>
    </div>
  </form>
</div>

</body>
</html>