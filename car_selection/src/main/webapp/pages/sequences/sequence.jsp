<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ include file="/pages/base/header.jsp" %>

<div class="content-title">Секвенция «${sequence.title}»</div>

<script type="text/javascript">Versions.show(${sequence.id}, 'SEQUENCE');</script>
<div id="versions"></div>

<%@ include file="/pages/base/footer.jsp" %>