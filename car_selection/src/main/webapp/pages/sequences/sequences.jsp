<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ include file="/pages/base/header.jsp" %>

<div class="content-title" style="margin-bottom: 39px;">Секвенции</div>

<div class="pane-files pane ui-box">
  <button class="btn btn-green ico-file ui-file-open" title="Выбрать"></button>
  <input type="text" class="ui-file-name" placeholder="Выберите файл">
  <input type="file" class="ui-file" name="file">
  <button class="btn btn-gray ico-import ui-file-import" data-action="sequences/import" title="Отправить"></button>
  <button class="btn btn-blue ico-export ui-file-export" data-action="sequences/export" title="Скачать"></button>
</div>

<div id="sequence_pane"></div>
<div id="sequence_list"></div>

<script type="text/javascript">Sequences.list();</script>

<%@ include file="/pages/base/footer.jsp" %>