<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ include file="/pages/base/header.jsp" %>

<div class="content-title">Пользователи</div>

<c:url value="/users/insert" var="url"/>
<form:form action="${url}" modelAttribute="user" class="pane">
  <table class="table-form">
    <tr>
      <td>
        <form:input path="alias" type="text" maxlength="40" placeholder="Псевдоним"/>
      </td>
      <td class="cw-username">
        <form:input path="username" type="text" maxlength="40" placeholder="Логин"/>
      </td>
      <td class="cw-password">
        <form:input path="password" type="password" maxlength="16" placeholder="Пароль"/>
      </td>
      <td class="cw-btn-one">
        <button type="submit" class="btn btn-blue ico-plus" title="Добавить"></button>
      </td>
    </tr>
  </table>
</form:form>

<c:if test="${! empty users}">
  <table class="table-data">
    <c:forEach items="${users}" var="user">
      <tr id="user_${user.id}" data-id="${user.id}">
        <td class="cw-btn-one">
          <button class="btn btn-green ico-update user-update" title="Редактировать"></button>
        </td>
        <td colspan="3">
          <div class="user-info">
            <div class="user-info-alias">${user.alias}</div>
            <div class="user-info-name"><a href="mailto:${user.username}">${user.username}</a></div>
            <div class="user-info-date">
              <div style="display: inline-block; width: 55px; text-align: right">Создан:</div>
              <div style="display: inline-block; width: 95px;"><fmt:formatDate pattern="dd.MM.yyyy HH:mm" value="${user.dateInsert}"/></div><br>
              <div style="display: inline-block; width: 55px; text-align: right;">Изменен:</div>
              <div style="display: inline-block; width: 95px;"><fmt:formatDate pattern="dd.MM.yyyy HH:mm" value="${user.dateUpdate}"/></div>
            </div>
          </div>
        </td>
        <td style="display: none;">
          <input type="text" class="user-alias" value="${user.alias}" maxlength="40">
        </td>
        <td class="cw-username" style="display: none;">
          <input type="text" class="user-username" value="${user.username}" maxlength="40" disabled>
        </td>
        <td class="cw-password" style="display: none;">
          <input type="password" class="user-password" value="" maxlength="16">
        </td>
        <td class="cw-btn-one">
          <button class="btn btn-gray ico-delete user-delete" title="Удалить"></button>
        </td>
      </tr>
    </c:forEach>
  </table>
</c:if>

<%@ include file="/pages/base/footer.jsp" %>