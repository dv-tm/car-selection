<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ include file="/pages/base/header.jsp" %>

<div class="ui-box">
  <div class="content-title">Привязка параметров и характеристик</div>

  <div class="pane-files ui-box">
    <button class="btn btn-green ico-file ui-file-open" title="Выбрать"></button>
    <input type="text" class="ui-file-name" placeholder="Выберите файл">
    <input type="file" class="ui-file binds-file" name="file">
    <button class="btn btn-gray ico-import binds-import" title="Отправить"></button>
    <button class="btn btn-blue ico-export binds-export" title="Скачать"></button>
  </div>
</div>

<div class="pane ui-box">
  <a href="<c:url value="/params/getXlsx"/>" class="link link-binds">Скачать в XLS формате</a>
  <form action="<c:url value="/params/loadXlsx" />" enctype="multipart/form-data" class="form-inline" method="POST">
    <input type="file" name="xlsFile">
    <input type="submit" name="submit" value="Загрузить excel" class="">
  </form>
  <table class="table-form">
    <tr>
      <td class="cw-param-entries">
        <div class="ui-select-list prop-groups" size="7">
          <div class="ui-select-title">Группы характеристик</div>
          <div class="ui-select-options">
            <c:forEach items="${groups}" var="group" varStatus="loop">
              <div class="ui-select-option" value="${group.id}">
                <span class="ui-select-meta">#${group.id}</span>${group.title}
              </div>
            </c:forEach>
          </div>
          <div class="ui-select-value"></div>
        </div>
      </td>

      <td class="cw-param-entries">
        <div class="ui-select-list prop-group" size="7">
          <div class="ui-select-title">Характеристики</div>
          <div class="ui-select-options scroll"></div>
          <div class="ui-select-value"></div>
        </div>
        <c:forEach items="${groups}" var="group" varStatus="loop">
          <div class="ui-select-list prop-group" size="7" style="display: none;" id="group_${group.id}">
            <div class="ui-select-title">Характеристики</div>
            <div class="ui-select-options">
              <c:forEach items="${group.props}" var="prop">
                <div class="ui-select-option" value="${prop.id}">
                  <span class="ui-select-meta">#${prop.id}</span>${prop.title}
                </div>
              </c:forEach>
            </div>
            <div class="ui-select-value"></div>
          </div>
        </c:forEach>
      </td>

      <td class="cw-param-entries">
        <div class="ui-select-list" size="7" id="params">
          <div class="ui-select-title">Параметры</div>
          <div class="ui-select-options">
            <c:forEach items="${params}" var="prm">
              <div class="ui-select-option" value="${prm.id}">
                <span class="ui-select-meta">#${prm.id}</span>${prm.title}
              </div>
            </c:forEach>
          </div>
          <div class="ui-select-value"></div>
        </div>
      </td>
    </tr>
  </table>
</div>

<div id="prop-binds" style="display: none;"></div>

<%@ include file="/pages/base/footer.jsp" %>