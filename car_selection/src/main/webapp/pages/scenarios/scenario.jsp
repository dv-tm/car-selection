<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ include file="/pages/base/header.jsp" %>

<div class="content-title">Сценарий «${scenario.title}»</div>

<script type="text/javascript">Versions.show(${scenario.id}, 'SCENARIO');</script>
<div id="versions"></div>

<%@ include file="/pages/base/footer.jsp" %>