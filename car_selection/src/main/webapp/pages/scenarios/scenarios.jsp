<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ include file="/pages/base/header.jsp" %>

<div class="content-title" style="margin-bottom: 39px;">Сценарии</div>

<div class="pane-files pane ui-box">
  <button class="btn btn-green ico-file ui-file-open" title="Выбрать"></button>
  <input type="text" class="ui-file-name" placeholder="Выберите файл">
  <input type="file" class="ui-file" name="file">
  <button class="btn btn-gray ico-import ui-file-import" data-action="scenarios/import" title="Отправить"></button>
  <button class="btn btn-blue ico-export ui-file-export" data-action="scenarios/export" title="Скачать"></button>
</div>

<div id="scenario_pane"></div>
<div id="scenario_list"></div>

<script type="text/javascript">Scenarios.list();</script>

<%@ include file="/pages/base/footer.jsp" %>