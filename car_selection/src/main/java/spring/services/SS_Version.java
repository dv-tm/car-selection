package spring.services;

import app.HandleException;
import app.Messages;
import entities.main.Event;
import entities.main.Scenario;
import entities.main.Sequence;
import entities.params.Param;
import entities.params.ParamVersion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import spring.repositories.SR_Event;
import spring.repositories.SR_Param;
import spring.repositories.SR_Scenario;
import spring.repositories.SR_Sequence;
import spring.repositories.SR_Version;

@Service
@Transactional
public class SS_Version
{
  @Autowired
  private SR_Version versionRepo;

  @Autowired
  private SR_Param paramRepo;

  @Autowired
  private SR_Event eventRepo;

  @Autowired
  private SR_Scenario scenarioRepo;

  @Autowired
  private SR_Sequence sequenceRepo;

  /** Создание версии параметра. */
  public void insert(ParamVersion version) throws HandleException
  {
    Param param = paramRepo.one(version.getParam().getId());

    if (param == null) {
      throw new HandleException(Messages.PARAM_UPDATE_MISSED);
    }

    Event event = version.getEvent();
    Scenario scenario = version.getScenario();
    Sequence sequence = version.getSequence();

    if (event != null) {
      event = eventRepo.one(event.getId());

      if (event == null) {
        throw new HandleException(Messages.EVENT_UPDATE_MISSED);
      }
    } else if (scenario != null) {
      scenario = scenarioRepo.one(scenario.getId());

      if (scenario == null) {
        throw new HandleException(Messages.SCENARIO_UPDATE_MISSED);
      }
    } else if (sequence != null) {
      sequence = sequenceRepo.one(sequence.getId());

      if (sequence == null) {
        throw new HandleException(Messages.SEQUENCE_UPDATE_MISSED);
      }
    }

    version.setParam(param);
    version.setEvent(event);
    version.setScenario(scenario);
    version.setSequence(sequence);

    versionRepo.save(version);
  }

  /** Изменение версии параметра. */
  public void update(ParamVersion version) throws HandleException
  {
    ParamVersion previous = versionRepo.one(version.getId());

    if (previous == null) {
      throw new HandleException(Messages.VERSION_UPDATE_MISSED);
    }

    previous.setMin(version.getMin());
    previous.setMax(version.getMax());
    previous.setMinDelta(version.getMinDelta());
    previous.setMaxDelta(version.getMaxDelta());

    versionRepo.update(previous);
  }

  /** Удаление версии параметра. */
  public void delete(Long id)
  {
    versionRepo.delete(new ParamVersion(id));
  }
}