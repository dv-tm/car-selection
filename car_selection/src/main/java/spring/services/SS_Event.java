package spring.services;

import app.HandleException;
import app.Messages;
import db.CalcCar;
import db.CalcProp;
import db.CalcPropValue;
import db.Calculate;
import entities.main.Event;
import entities.main.Sequence;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import spring.repositories.SR_Event;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class SS_Event
{
  @Autowired
  private SR_Event eventRepo;

  @Autowired
  private SS_Perfect perfectService;

  @Autowired
  private SS_Sequence sequenceService;

  /** Получение эвента. */
  public Event one(Long id)
  {
    return eventRepo.one(id);
  }

  /** Получение эвента с прогрузкой полей. */
  public Event deepOne(Long id) throws HandleException
  {
    String[] fields = {"params", "scenarios", "sequence"};
    Event event = eventRepo.one(id, fields);

    if (event.getSequence() == null) {
      this.defineSequence(event);
    }

    return event;
  }

  /** Получение списка эвентов. */
  public List<Event> list()
  {
    return eventRepo.list(false);
  }

  /** Создание эвента. */
  public void insert(Event event)
  {
    String title = event.getTitle();

    if (title != null) {
      event.setTitle(title.trim());
    }

    eventRepo.save(event);
  }

  /** Изменение эвента. */
  public void update(Event event) throws HandleException
  {
    Event previous = eventRepo.one(event.getId());

    if (previous == null) {
      throw new HandleException(Messages.EVENT_UPDATE_MISSED);
    }

    previous.setTitle(event.getTitle());
    previous.setModality(event.getModality());
    previous.setRadicals(event.getRadicals());
    previous.setMoney(event.getMoney());

    previous.trim();

    eventRepo.update(previous);
  }

  /** Изменение эвента. */
  public void merge(Event event) throws HandleException
  {
    Event previous = eventRepo.one(event.getId());

    if (previous == null) {
      throw new HandleException(Messages.EVENT_UPDATE_MISSED);
    }

    previous.setScenarios(event.getScenarios());

    eventRepo.update(previous);
  }

  /** Удаление эвента. */
  public void delete(Long id)
  {
    eventRepo.delete(new Event(id));
  }

  /** Определение секвенции для эвента. */
  private void defineSequence(Event event) throws HandleException
  {
    String profile = event.getRadicals();
    List<Sequence> sequences = sequenceService.list();

    for (Sequence sequence : sequences) {
      if (profile.equals(sequence.getRadicals())) {
        event.setSequence(sequence);
        break;
      }
    }

    if (event.getSequence() == null) {
      throw new HandleException(Messages.EVENT_SEQUENCE_MISSED);
    }

    eventRepo.update(event);
  }

  /** Сохранение идеальной сущности и расчет эвента. */
  public ArrayList<CalcCar> calculate(Event event) throws HandleException
  {
    Event previous = eventRepo.one(event.getId());

    if (previous == null) {
      throw new HandleException(Messages.EVENT_UPDATE_MISSED);
    }

    Long request = requestHash();

    perfectService.insert(previous.perfects(), request);
    eventRepo.calculate(request, previous.getId());
    return null;
  }

  /** Сохранение идеальной сущности и расчет эвента. */
  public SXSSFWorkbook searchCarXlsx(Event event) throws HandleException
  {
    Event previous = eventRepo.one(event.getId());

    if (previous == null) {
      throw new HandleException(Messages.EVENT_UPDATE_MISSED);
    }

    Long request = requestHash();

    perfectService.insert(previous.perfects(), request);

    if (true) {
      return eventRepo.calculate2(request, previous.getId()).getWorkbook();
    }

    Calculate cars = eventRepo.calculate(request, previous.getId());

    Logger log = Logger.getLogger(this.getClass());
    try {
      SXSSFWorkbook workbook = new SXSSFWorkbook(3000);
      Sheet sheet = workbook.createSheet("Sheet");
      int headrow = 1;
      int headCol = 1;
      Row rowHeader = sheet.createRow(headrow);
      rowHeader.createCell(headCol).setCellValue("Характеристики/Авто");

      //Создаем характеристики
      int curRow = headrow + 1;
      for (CalcProp prop : cars.getProps().values()) {
        Row row = sheet.createRow(curRow);
        row.createCell(headCol - 1).setCellValue(prop.ext_id);
        row.createCell(headCol).setCellValue(prop.title + " [" + prop.location + "]");
        prop.row = row;
        curRow++;
      }

      int curCol = headCol + 1;
      for (CalcCar car : cars.getCars().values()) {
        rowHeader.createCell(curCol).setCellValue(car.title);
        for (Map.Entry<CalcProp, CalcPropValue> vals : car.props.entrySet()) {
          vals.getKey().row.createCell(curCol).setCellValue(vals.getValue().data_value);
        }
        curCol++;
      }

      return workbook;
    } catch (Exception e) {
      log.warn("getXlsx()", e);
    }

    return null;
  }

  /** Создание хэша запроса. */
  private static Long requestHash()
  {
    return new Date().getTime();
  }
}