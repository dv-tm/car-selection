package spring.services;

import app.HandleException;
import app.Messages;
import entities.main.Scenario;
import entities.params.Param;
import entities.params.ParamQuality;
import entities.params.ParamType;
import entities.params.ParamVersion;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import spring.repositories.SR_Param;
import spring.repositories.SR_Scenario;
import spring.repositories.SR_Version;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
@Transactional
public class SS_Scenario
{
  @Autowired
  private SR_Scenario scenarioRepo;

  @Autowired
  private SR_Param paramRepo;

  @Autowired
  private SR_Version versionRepo;

  /** Получение сценария. */
  public Scenario one(Long id)
  {
    return scenarioRepo.one(id);
  }

  /** Получение списка сценариев. */
  public List<Scenario> list()
  {
    return scenarioRepo.list(true, "uid");
  }

  /** Получение списка сценариев. */
  public List<Scenario> deepList()
  {
    String[] orders = {}, fields = {"params"};
    return scenarioRepo.list(true, orders, fields);
  }

  /** Создание сценария. */
  public void insert(Scenario scenario) throws HandleException
  {
    scenario.trim();

    if (scenarioRepo.one(scenario.getUid()) != null) {
      throw new HandleException(Messages.SCENARIO_ALREADY_USED);
    }

    scenarioRepo.save(scenario);
  }

  /** Изменение сценария. */
  public void update(Scenario scenario) throws HandleException
  {
    Scenario previous = scenarioRepo.one(scenario.getId());

    if (previous == null) {
      throw new HandleException(Messages.SCENARIO_UPDATE_MISSED);
    }

    previous.setUid(scenario.getUid());
    previous.setTitle(scenario.getTitle());

    previous.trim();

    scenarioRepo.update(previous);
  }

  /** Удаление сценария. */
  public void delete(Long id)
  {
    scenarioRepo.delete(new Scenario(id));
  }

  /** Экспорт сценариев. */
  public XSSFWorkbook export()
  {
    XSSFWorkbook book = new XSSFWorkbook();
    Sheet sheetScenarios = book.createSheet("Сценарии");
    Sheet sheetParams = book.createSheet("Параметры");

    XSSFFont fontHeader = book.createFont();
    XSSFCellStyle styleHeader = book.createCellStyle();
    XSSFCellStyle styleElements = book.createCellStyle();

    fontHeader.setBold(true);

    styleHeader.setWrapText(true);
    styleHeader.setAlignment(CellStyle.ALIGN_CENTER);
    styleHeader.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
    styleHeader.setFont(fontHeader);

    styleElements.setAlignment(CellStyle.ALIGN_CENTER);
    styleElements.setVerticalAlignment(CellStyle.VERTICAL_CENTER);

    int line = 0, col = 0; // Нумерация строк и столбцов.

    Row row = sheetScenarios.createRow(line++);

    row.createCell(col++).setCellValue("UID");
    row.createCell(col++).setCellValue("Наименование сценария");

    for (Cell cell : row) {
      cell.setCellStyle(styleHeader);
    }

    List<Scenario> scenarios = this.deepList();

    for (Scenario scenario : scenarios) {
      row = sheetScenarios.createRow(line++);
      col = 0; // Сброс номера стобца.

      row.createCell(col++).setCellValue(scenario.getUid());
      row.createCell(col++).setCellValue(scenario.getTitle());

      for (Cell cell : row) {
        int columnIndex = cell.getColumnIndex();
        if (columnIndex != 0 && columnIndex != 1) {
          cell.setCellStyle(styleElements);
        }
      }
    }

    line = 0; // Сброс номера строки.
    col = 0; // Сброс номера столбца

    row = sheetParams.createRow(line++);

    row.createCell(col++).setCellValue("Сценарий");

    row.createCell(col++).setCellValue("UID");
    row.createCell(col++).setCellValue("Наименование параметра");
    row.createCell(col++).setCellValue("Тип");

    row.createCell(col++).setCellValue("Дельта\nминимум");
    row.createCell(col++).setCellValue("Минимум");
    row.createCell(col++).setCellValue("Максимум");
    row.createCell(col++).setCellValue("Дельта\nмаксимум");
    row.createCell(col++).setCellValue("Качество");

    row.createCell(col++).setCellValue("А");
    row.createCell(col++).setCellValue("В");
    row.createCell(col++).setCellValue("К");

    for (Cell cell : row) {
      cell.setCellStyle(styleHeader);
    }

    for (Scenario scenario : scenarios) {
      for (ParamVersion param : scenario.getParams()) {
        row = sheetParams.createRow(line++);
        col = 0; // Сброс номера стобца.

        row.createCell(col++).setCellValue(scenario.getUid());

        row.createCell(col++).setCellValue(param.getUid());
        row.createCell(col++).setCellValue(param.getTitle());
        row.createCell(col++).setCellValue(param.getType().name());

        row.createCell(col++).setCellValue(param.getMinDelta());
        row.createCell(col++).setCellValue(param.getMin());
        row.createCell(col++).setCellValue(param.getMax());
        row.createCell(col++).setCellValue(param.getMaxDelta());
        row.createCell(col++).setCellValue(param.getQuality().name());

        row.createCell(col++).setCellValue(param.getMa());
        row.createCell(col++).setCellValue(param.getMv());
        row.createCell(col++).setCellValue(param.getMk());
      }
    }

    sheetScenarios.createFreezePane(0, 1);
    sheetParams.createFreezePane(0, 1);

    Row rowHeader = sheetScenarios.getRow(0);
    int columns = rowHeader.getPhysicalNumberOfCells();

    for (int k = 0; k < columns; k++) {
      sheetScenarios.autoSizeColumn(k);
    }

    rowHeader = sheetParams.getRow(0);
    columns = rowHeader.getPhysicalNumberOfCells();

    for (int k = 0; k < columns; k++) {
      sheetParams.autoSizeColumn(k);
    }

    return book;
  }

  /** Парсинг списка сценариев из файла. */
  public void parse(InputStream is) throws HandleException
  {
    Map<String, Scenario> scenarios = new HashMap<>();

    try {
      XSSFWorkbook book = new XSSFWorkbook(is);
      XSSFSheet sheetScenarios = book.getSheetAt(0);
      XSSFSheet sheetParams = book.getSheetAt(1);

      int line = 0, col = 0; // Нумерация строк и столбцов.

      for (Row row : sheetScenarios) {
        line = row.getRowNum(); // Номер строки.
        if (line == 0) continue; // Исключение заголовка.
        col = 0; // Сброс номера стобца.

        String uid = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getStringCellValue().trim();
        String title = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getStringCellValue().trim();

        if (uid.isEmpty() || title.isEmpty()) {
          continue; // Исключение пустых и ошибочных строк.
        }

        // Поиск предыдущего сценария.
        Scenario scenario = scenarioRepo.one(uid);

        if (scenario == null) {
          scenario = new Scenario();

          scenario.setUid(uid);
          scenario.setTitle(title);

          scenarioRepo.save(scenario);
        } else {

          scenario.setUid(uid);
          scenario.setTitle(title);

          // Удаление всех параметров из сценария.
          Set<ParamVersion> params = scenario.getParams();
          for (Iterator iterator = params.iterator(); iterator.hasNext(); ) {
            ParamVersion param = (ParamVersion) iterator.next();
            iterator.remove(); // Отсоединение параметра от сессии.

            versionRepo.delete(param);
          }

          scenarioRepo.update(scenario);
        }

        scenarios.put(uid, scenario);
      }

      for (Row row : sheetParams) {
        line = row.getRowNum(); // Номер строки.
        if (line == 0) continue; // Исключение заголовка.
        col = 0; // Сброс номера стобца.

        String parent = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getStringCellValue().trim();

        String uid = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getStringCellValue().trim();
        String title = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getStringCellValue().trim();
        String type = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getStringCellValue().trim();

        Double minDelta = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getNumericCellValue();
        Double min = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getNumericCellValue();
        Double max = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getNumericCellValue();
        Double maxDelta = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getNumericCellValue();
        String quality = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getStringCellValue().trim();

        Double ma = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getNumericCellValue();
        Double mv = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getNumericCellValue();
        Double mk = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getNumericCellValue();

        if (uid.isEmpty() || parent.isEmpty()) {
          continue; // Исключение пустых и ошибочных строк.
        }

        ParamType paramType = ParamType.getByName(type);
        ParamQuality paramQuality = ParamQuality.getByName(quality);

        if (paramType == null || paramQuality == null) {
          throw new HandleException(String.format(Messages.PARAM_PARSE_TYPE_ERROR, line + 1));
        }

        // Поиск родительского сценария.
        Scenario scenario = scenarios.get(parent);

        if (scenario == null) {
          continue; // Для параметра не определен сценарий.
        }

        // Поиск родильского параметра.
        Param param = paramRepo.one(uid);

        if (param == null) {
          continue; // Параметра не существует в базе данных.
        }

        if (param.getType() != paramType || param.getQuality() != paramQuality) {
          throw new HandleException(String.format(Messages.PARAM_PARSE_TYPE_WRONG, line + 1));
        }

        ParamVersion version = new ParamVersion();

        version.setMin(Math.round(min));
        version.setMax(Math.round(max));
        version.setMinDelta(Math.round(minDelta));
        version.setMaxDelta(Math.round(maxDelta));

        version.setParam(param);
        version.setScenario(scenario);

        versionRepo.save(version);
      }

    } catch (Exception ex) {
      if (ex instanceof HandleException) {
        throw new HandleException(ex.getMessage());
      }

      throw new HandleException(Messages.SCENARIO_PARSE_ERROR);
    }
  }
}