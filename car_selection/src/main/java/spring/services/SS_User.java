package spring.services;

import app.HandleException;
import app.Messages;
import entities.users.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import spring.repositories.SR_User;

import java.util.List;

@Service
@Transactional
public class SS_User
{
  @Autowired
  private SR_User userRepo;

  /** Получение пользователя. */
  public User get(String username)
  {
    return userRepo.one(username);
  }

  /** Получение списка пользователей. */
  public List<User> list()
  {
    return userRepo.list(false);
  }

  /** Создание пользователя. */
  public void insert(User user) throws HandleException
  {
    User previous = this.get(user.getUsername());

    if (previous != null) {
      throw new HandleException(Messages.USER_ALREADY_USED);
    }

    user.setAlias(generateAlias(user));
    user.setHash(generateHash(user.getPassword()));

    userRepo.save(user);
  }

  /** Изменение данных пользователя. */
  public void update(User user) throws HandleException
  {
    User previous = userRepo.one(user.getId());

    if (previous == null) {
      throw new HandleException(Messages.USER_UPDATE_MISSED);
    }

    String pass = user.getPassword();

    if (pass != null) {
      previous.setHash(generateHash(pass));
    }

    previous.setPassword("1234");
    previous.setAlias(generateAlias(user));

    userRepo.update(previous);
  }

  /** Удаление пользователя. */
  public void delete(Long id)
  {
    userRepo.delete(new User(id));
  }

  public static String generateHash(String password)
  {
    ShaPasswordEncoder encoder = new ShaPasswordEncoder(256);
    return encoder.encodePassword(password, "");
  }

  public static String generateAlias(User user)
  {
    String alias = user.getAlias();

    if (alias == null || alias.trim().isEmpty()) {
      return user.getUsername().split("@")[0];
    }

    return alias.trim();
  }
}