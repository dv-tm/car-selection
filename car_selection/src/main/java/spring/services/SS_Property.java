package spring.services;

import entities.cars.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import spring.repositories.SR_Property;

import java.util.List;

@Service
@Transactional
public class SS_Property
{
  @Autowired
  private SR_Property propRepo;

  /** Получение списка характеристик. */
  public List<Property> list()
  {
    return propRepo.list(true, "parent_id", "title");
  }

  /** Получение списка характеристик по группе. */
  public List<Property> listByGroup(Integer groupId)
  {
    return propRepo.listByGroupId(groupId);
  }
}