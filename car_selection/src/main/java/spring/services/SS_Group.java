package spring.services;

import entities.cars.PropertyGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import spring.repositories.SR_Group;

import java.util.List;

@Service
@Transactional
public class SS_Group
{
  @Autowired
  private SR_Group propertyRepo;

  /** Получение списка групп характеристик. */
  public List<PropertyGroup> list()
  {
    return propertyRepo.list(true, "title");
  }
}