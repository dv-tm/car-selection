package spring.services;

import app.HandleException;
import app.Messages;
import entities.main.Sequence;
import entities.params.Param;
import entities.params.ParamQuality;
import entities.params.ParamType;
import entities.params.ParamVersion;
import entities.rules.ARule;
import entities.rules.BRule;
import entities.rules.PRule;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import spring.repositories.SR_Param;
import spring.repositories.SR_Sequence;
import spring.repositories.SR_Version;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
@Transactional
public class SS_Sequence
{
  @Autowired
  private SR_Sequence sequenceRepo;

  @Autowired
  private SR_Param paramRepo;

  @Autowired
  private SR_Version versionRepo;

  /** Получение секвенции. */
  public Sequence one(Long id)
  {
    return sequenceRepo.one(id);
  }

  /** Получение списка секвенций. */
  public List<Sequence> list()
  {
    return sequenceRepo.list(true, "radicals");
  }

  /** Получение списка секвенций. */
  public List<Sequence> deepList()
  {
    String[] orders = {"radicals"}, fields = {"params"};
    return sequenceRepo.list(true, orders, fields);
  }

  /** Создание секвенции. */
  public void insert(Sequence sequence) throws HandleException
  {
    sequence.trim();

    if (sequenceRepo.one(sequence.getRadicals()) != null) {
      throw new HandleException(Messages.SEQUENCE_ALREADY_USED);
    }

    sequenceRepo.save(sequence);
  }

  /** Изменение секвенции. */
  public void update(Sequence sequence) throws HandleException
  {
    Sequence previous = sequenceRepo.one(sequence.getId());

    if (previous == null) {
      throw new HandleException(Messages.SEQUENCE_UPDATE_MISSED);
    }

    previous.setTitle(sequence.getTitle());
    previous.setRadicals(sequence.getRadicals());
    previous.setARule(sequence.getARule());
    previous.setBRule(sequence.getBRule());
    previous.setPRule(sequence.getPRule());

    previous.trim();

    sequenceRepo.update(previous);
  }

  /** Удаление секвенции. */
  public void delete(Long id)
  {
    sequenceRepo.delete(new Sequence(id));
  }

  /** Экспорт секвенций. */
  public XSSFWorkbook export()
  {
    XSSFWorkbook book = new XSSFWorkbook();
    Sheet sheetSequences = book.createSheet("Секвенции");
    Sheet sheetParams = book.createSheet("Параметры");

    XSSFFont fontHeader = book.createFont();
    XSSFCellStyle styleHeader = book.createCellStyle();
    XSSFCellStyle styleParams = book.createCellStyle();

    fontHeader.setBold(true);

    styleHeader.setWrapText(true);
    styleHeader.setAlignment(CellStyle.ALIGN_CENTER);
    styleHeader.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
    styleHeader.setFont(fontHeader);

    styleParams.setAlignment(CellStyle.ALIGN_CENTER);
    styleParams.setVerticalAlignment(CellStyle.VERTICAL_CENTER);

    int line = 0, col = 0; // Нумерация строк и столбцов.

    Row row = sheetSequences.createRow(line++);

    row.createCell(col++).setCellValue("Радикалы");
    row.createCell(col++).setCellValue("Наименование секвенции");
    row.createCell(col++).setCellValue("Ранг A");
    row.createCell(col++).setCellValue("Ранг B");
    row.createCell(col++).setCellValue("Ресурсы");

    for (Cell cell : row) {
      cell.setCellStyle(styleHeader);
    }

    List<Sequence> sequences = this.deepList();

    for (Sequence sequence : sequences) {
      row = sheetSequences.createRow(line++);
      col = 0; // Сброс номера стобца.

      row.createCell(col++).setCellValue(sequence.getRadicals());
      row.createCell(col++).setCellValue(sequence.getTitle());
      row.createCell(col++).setCellValue(sequence.getARule().name());
      row.createCell(col++).setCellValue(sequence.getBRule().name());
      row.createCell(col++).setCellValue(sequence.getPRule().name());

      for (Cell cell : row) {
        int columnIndex = cell.getColumnIndex();
        if (columnIndex != 0 && columnIndex != 1) {
          cell.setCellStyle(styleParams);
        }
      }
    }

    line = 0; // Сброс номера строки.
    col = 0; // Сброс номера столбца

    row = sheetParams.createRow(line++);

    row.createCell(col++).setCellValue("Секвенция");

    row.createCell(col++).setCellValue("UID");
    row.createCell(col++).setCellValue("Наименование параметра");
    row.createCell(col++).setCellValue("Тип");

    row.createCell(col++).setCellValue("Дельта\nминимум");
    row.createCell(col++).setCellValue("Минимум");
    row.createCell(col++).setCellValue("Максимум");
    row.createCell(col++).setCellValue("Дельта\nмаксимум");
    row.createCell(col++).setCellValue("Качество");

    row.createCell(col++).setCellValue("А");
    row.createCell(col++).setCellValue("В");
    row.createCell(col++).setCellValue("К");

    for (Cell cell : row) {
      cell.setCellStyle(styleHeader);
    }

    for (Sequence sequence : sequences) {
      for (ParamVersion param : sequence.getParams()) {
        row = sheetParams.createRow(line++);
        col = 0; // Сброс номера стобца.

        row.createCell(col++).setCellValue(sequence.getRadicals());

        row.createCell(col++).setCellValue(param.getUid());
        row.createCell(col++).setCellValue(param.getTitle());
        row.createCell(col++).setCellValue(param.getType().name());

        row.createCell(col++).setCellValue(param.getMinDelta());
        row.createCell(col++).setCellValue(param.getMin());
        row.createCell(col++).setCellValue(param.getMax());
        row.createCell(col++).setCellValue(param.getMaxDelta());
        row.createCell(col++).setCellValue(param.getQuality().name());

        row.createCell(col++).setCellValue(param.getMa());
        row.createCell(col++).setCellValue(param.getMv());
        row.createCell(col++).setCellValue(param.getMk());
      }
    }

    sheetSequences.createFreezePane(0, 1);
    sheetParams.createFreezePane(0, 1);

    Row rowHeader = sheetSequences.getRow(0);
    int columns = rowHeader.getPhysicalNumberOfCells();

    for (int k = 0; k < columns; k++) {
      sheetSequences.autoSizeColumn(k);
    }

    rowHeader = sheetParams.getRow(0);
    columns = rowHeader.getPhysicalNumberOfCells();

    for (int k = 0; k < columns; k++) {
      sheetParams.autoSizeColumn(k);
    }

    return book;
  }

  /** Парсинг списка секвенций из файла. */
  public void parse(InputStream is) throws HandleException
  {
    Map<String, Sequence> sequences = new HashMap<>();

    try {
      XSSFWorkbook book = new XSSFWorkbook(is);
      XSSFSheet sheetSequences = book.getSheetAt(0);
      XSSFSheet sheetParams = book.getSheetAt(1);

      int line = 0, col = 0; // Нумерация строк и столбцов.

      for (Row row : sheetSequences) {
        line = row.getRowNum(); // Номер строки.
        if (line == 0) continue; // Исключение заголовка.
        col = 0; // Сброс номера стобца.

        String radicals = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getStringCellValue().trim();
        String title = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getStringCellValue().trim();
        String ruleA = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getStringCellValue().trim();
        String ruleB = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getStringCellValue().trim();
        String ruleP = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getStringCellValue().trim();

        if (radicals.isEmpty() || title.isEmpty()) {
          continue; // Исключение пустых и ошибочных строк.
        }

        ARule aRule = ARule.getByName(ruleA);
        BRule bRule = BRule.getByName(ruleB);
        PRule pRule = PRule.getByName(ruleP);

        if (aRule == null || bRule == null || pRule == null) {
          continue; // Исключение секвенций, для которых не заданы правила.
        }

        // Поиск предыдущей секвенции.
        Sequence sequence = sequenceRepo.one(radicals);

        if (sequence == null) {
          sequence = new Sequence();

          sequence.setRadicals(radicals);
          sequence.setTitle(title);
          sequence.setARule(aRule);
          sequence.setBRule(bRule);
          sequence.setPRule(pRule);

          sequenceRepo.save(sequence);
        } else {

          sequence.setRadicals(radicals);
          sequence.setTitle(title);
          sequence.setARule(aRule);
          sequence.setBRule(bRule);
          sequence.setPRule(pRule);

          // Удаление всех параметров из секвенции.
          Set<ParamVersion> params = sequence.getParams();
          for (Iterator iterator = params.iterator(); iterator.hasNext(); ) {
            ParamVersion param = (ParamVersion) iterator.next();
            iterator.remove(); // Отсоединение параметра от сессии.

            versionRepo.delete(param);
          }

          sequenceRepo.update(sequence);
        }

        sequences.put(radicals, sequence);
      }

      for (Row row : sheetParams) {
        line = row.getRowNum(); // Номер строки.
        if (line == 0) continue; // Исключение заголовка.
        col = 0; // Сброс номера стобца.

        String parent = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getStringCellValue().trim();

        String uid = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getStringCellValue().trim();
        String title = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getStringCellValue().trim();
        String type = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getStringCellValue().trim();

        Double minDelta = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getNumericCellValue();
        Double min = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getNumericCellValue();
        Double max = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getNumericCellValue();
        Double maxDelta = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getNumericCellValue();
        String quality = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getStringCellValue().trim();

        Double ma = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getNumericCellValue();
        Double mv = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getNumericCellValue();
        Double mk = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getNumericCellValue();

        if (uid.isEmpty() || parent.isEmpty()) {
          continue; // Исключение пустых и ошибочных строк.
        }

        ParamType paramType = ParamType.getByName(type);
        ParamQuality paramQuality = ParamQuality.getByName(quality);

        if (paramType == null || paramQuality == null) {
          throw new HandleException(String.format(Messages.PARAM_PARSE_TYPE_ERROR, line + 1));
        }

        // Поиск родительской секвенции.
        Sequence sequence = sequences.get(parent);

        if (sequence == null) {
          continue; // Для параметра не определена секвенция.
        }

        // Поиск родильского параметра.
        Param param = paramRepo.one(uid);

        if (param == null) {
          continue; // Параметра не существует в базе данных.
        }

        if (param.getType() != paramType || param.getQuality() != paramQuality) {
          throw new HandleException(String.format(Messages.PARAM_PARSE_TYPE_WRONG, line + 1));
        }

        ParamVersion version = new ParamVersion();

        version.setMin(Math.round(min));
        version.setMax(Math.round(max));
        version.setMinDelta(Math.round(minDelta));
        version.setMaxDelta(Math.round(maxDelta));

        version.setParam(param);
        version.setSequence(sequence);

        versionRepo.save(version);
      }

    } catch (Exception ex) {
      if (ex instanceof HandleException) {
        throw new HandleException(ex.getMessage());
      }

      throw new HandleException(Messages.SEQUENCE_PARSE_ERROR);
    }
  }
}