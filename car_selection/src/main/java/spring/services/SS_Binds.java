package spring.services;

import app.HandleException;
import app.Messages;
import entities.binds.PropertyPair;
import entities.cars.Property;
import entities.params.ParamType;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import spring.repositories.SR_Binds;
import spring.repositories.SR_Property;
import utils.ParseCellValue;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
@Transactional
public class SS_Binds
{
  @Autowired
  private SR_Binds bindsRepo;
  @Autowired
  private SR_Property propsRepo;

  /** Получение соответствий. */
  public List<PropertyPair> list()
  {
    return bindsRepo.list(false);
  }

  /** Получение соответствий. */
  public List<PropertyPair> list(Long propsId, Long paramId)
  {
    if (propsId == null) {
      return new ArrayList<>();
    } else if (paramId == null) {
      return bindsRepo.list(propsId, null);
    }

    ArrayList<PropertyPair> propertyPairs = new ArrayList<>();
    propertyPairs.addAll(bindsRepo.list(propsId, paramId));

    List<Object[]> vals = bindsRepo.listWithNoParams(propsId, paramId);
    if (bindsRepo != null && vals.size() > 0) {
      Property prop = propsRepo.one(propsId);
      if (prop == null) {
        return new ArrayList<>();
      }
      for (Object[] val : vals) {
        PropertyPair propPair = new PropertyPair();
        propPair.setLocation((String) val[1]);
        propPair.setPropValue((String) val[0]);
        propPair.setProp(prop);
        propertyPairs.add(propPair);
      }
    }
    return propertyPairs;
  }

  public SXSSFWorkbook getAllValues()
  {
    int maxXlsRow = 30000;
    Logger log = Logger.getLogger(this.getClass());
    int currentSheet = 0;
    try {
      SXSSFWorkbook workbook = new SXSSFWorkbook(100);
      Sheet sheet = workbook.createSheet("Sheet" + ++currentSheet);
      Row rowHeader = sheet.createRow((short) 0);
      int rowHeadNum = 0;
      rowHeader.createCell(rowHeadNum++).setCellValue("ВНЕШНИЙ КОД");
      rowHeader.createCell(rowHeadNum++).setCellValue("ITEM");
      rowHeader.createCell(rowHeadNum++).setCellValue("ХАРАКТЕРИСТИКА");
      rowHeader.createCell(rowHeadNum++).setCellValue("РАСПОЛОЖЕНИЕ");
      rowHeader.createCell(rowHeadNum++).setCellValue("ОПИСАНИЕ");
      rowHeader.createCell(rowHeadNum++).setCellValue("ЗНАЧ. ХАР-КИ");
      rowHeader.createCell(rowHeadNum++).setCellValue("ЗНАЧ. ПАР-РА");
      rowHeader.createCell(rowHeadNum++).setCellValue("UID");
      rowHeader.createCell(rowHeadNum++).setCellValue("ТИП");
      rowHeader.createCell(rowHeadNum++).setCellValue("ПАРАМЕТР");

      List<Object[]> values = bindsRepo.listDifferentValues();
      if (values.isEmpty()) {
        return workbook;
      }

      int rowNum = 1;
      for (Object[] value : values) {
        String extId = String.valueOf(value[0]);
        String item = "0".equals(String.valueOf(value[12])) ? "+" : "";
        String prop_title = String.valueOf(value[3]);
        String prop_loc = String.valueOf(value[4]);
        String prop_val_title = String.valueOf(value[5]);
        String prop_val = String.valueOf(value[6]);
        String param_val = String.valueOf(value[7]);
        String uid = String.valueOf(value[8]);
        ParamType paramType = ParamType.getByName(String.valueOf(value[9]));
        String type = paramType != null ? paramType.getTitle() : String.valueOf(value[9]);
        String par_title = String.valueOf(value[10]);

        Row rowBody = sheet.createRow(rowNum);
        rowNum++;
        int celNum = 0;
        rowBody.createCell(celNum++).setCellValue(extId);
        rowBody.createCell(celNum++).setCellValue(item);
        rowBody.createCell(celNum++).setCellValue(prop_title);
        rowBody.createCell(celNum++).setCellValue(prop_loc);
        rowBody.createCell(celNum++).setCellValue(prop_val_title);
        rowBody.createCell(celNum++).setCellValue(prop_val);
        rowBody.createCell(celNum++).setCellValue(param_val);
        rowBody.createCell(celNum++).setCellValue(uid);
        rowBody.createCell(celNum++).setCellValue(type);
        rowBody.createCell(celNum++).setCellValue(par_title);

        if (rowNum > maxXlsRow) {
          sheet = workbook.createSheet("Sheet" + ++currentSheet);
          rowHeader = sheet.createRow(0);
          rowHeadNum = 0;
          rowHeader.createCell(rowHeadNum++).setCellValue("ВНЕШНИЙ КОД");
          rowHeader.createCell(rowHeadNum++).setCellValue("ITEM");
          rowHeader.createCell(rowHeadNum++).setCellValue("ХАРАКТЕРИСТИКА");
          rowHeader.createCell(rowHeadNum++).setCellValue("РАСПОЛОЖЕНИЕ");
          rowHeader.createCell(rowHeadNum++).setCellValue("ОПИСАНИЕ");
          rowHeader.createCell(rowHeadNum++).setCellValue("ЗНАЧ. ХАР-КИ");
          rowHeader.createCell(rowHeadNum++).setCellValue("ЗНАЧ. ПАР-РА");
          rowHeader.createCell(rowHeadNum++).setCellValue("UID");
          rowHeader.createCell(rowHeadNum++).setCellValue("ТИП");
          rowHeader.createCell(rowHeadNum++).setCellValue("ПАРАМЕТР");
          rowNum = 1;
        }
      }

      return workbook;
    } catch (Exception e) {
      log.warn("getXlsx()", e);
    }

    return null;
  }

  /** Создание соответствия. */
  public void insert(PropertyPair pair)
  {
    bindsRepo.save(pair);
  }

  /** Изменение соответствия. */
  public void update(PropertyPair pair) throws HandleException
  {
    PropertyPair previous = bindsRepo.one(pair.getId());

    if (previous == null) {
      throw new HandleException(Messages.PAIR_UPDATE_MISSED);
    }

    previous.setPropValue(pair.getPropValue());
    previous.setParamValue(pair.getParamValue());

    bindsRepo.update(previous);
  }

  /** Удаление соответствия. */
  public void delete(Long id)
  {
    bindsRepo.delete(new PropertyPair(id));
  }

  /** Загрузка значений из EXCEL внешним ID*/
  public void loadValuesFromXlsx(InputStream inputStream) {
    try {
      XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
      for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
        XSSFSheet sheet = workbook.getSheetAt(i);
        Iterator<Row> it = sheet.iterator();
        while (it.hasNext()) {
          Row row = it.next();
          String location = row.getCell(3)  != null ? ParseCellValue.getStringValue(row.getCell(3)) : null;
          Long param_value =  row.getCell(6)  != null ? ParseCellValue.getLongValue(row.getCell(6)) : null;
          String prop_value = row.getCell(5)  != null ? ParseCellValue.getStringValue(row.getCell(5)) : null;
          String uuid = row.getCell(7) != null ? ParseCellValue.getStringValue(row.getCell(7)) : null;
          Long ext_id = row.getCell(0)  != null ? ParseCellValue.getLongValue(row.getCell(0)) : null;

          if (uuid == null || uuid.trim().length() < 1) {
            continue;
          }
          if (ext_id == null || ext_id < 1) {
            continue;
          }
          if (param_value == null || prop_value == null) {
            continue;
          }
          bindsRepo.savePropBindbyExtId(location,param_value,prop_value,uuid,ext_id);
        }
      }
    } catch (Exception e) {

    }
  }

}