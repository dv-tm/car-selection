package spring.services;

import entities.params.ParamPerfect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import spring.repositories.SR_Perfect;

import java.util.List;

@Service
@Transactional
public class SS_Perfect
{
  @Autowired
  private SR_Perfect perfectRepo;

  /**
   * Сохранение идеальной сущности.
   * Без валидации, максимум скорости.
   */
  public void insert(List<ParamPerfect> perfects, Long request)
  {
    for (ParamPerfect perfect : perfects) {
      perfect.setRequest(request);
      perfectRepo.save(perfect);
    }
  }
}