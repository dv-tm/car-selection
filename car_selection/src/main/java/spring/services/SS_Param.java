package spring.services;

import app.HandleException;
import app.Messages;
import entities.params.Param;
import entities.params.ParamQuality;
import entities.params.ParamType;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import spring.repositories.SR_Param;

import java.io.InputStream;
import java.util.List;

@Service
@Transactional
public class SS_Param
{
  @Autowired
  private SR_Param paramRepo;

  /** Получение списка параметров. */
  public List<Param> list()
  {
    return paramRepo.list(false, "uid");
  }

  /** Создание параметра. */
  public void insert(Param param) throws HandleException
  {
    param.trim();

    if (paramRepo.one(param.getUid()) != null) {
      throw new HandleException(Messages.PARAM_ALREADY_USED);
    }

    paramRepo.save(param);
  }

  /** Изменение параметра. */
  public void update(Param param) throws HandleException
  {
    Param previous = paramRepo.one(param.getId());

    if (previous == null) {
      throw new HandleException(Messages.PARAM_UPDATE_MISSED);
    }

    previous.setUid(param.getUid());
    previous.setTitle(param.getTitle());
    previous.setType(param.getType());

    previous.setMin(param.getMin());
    previous.setMax(param.getMax());
    previous.setMinDelta(param.getMinDelta());
    previous.setMaxDelta(param.getMaxDelta());
    previous.setQuality(param.getQuality());

    previous.setMa(param.getMa());
    previous.setMv(param.getMv());
    previous.setMk(param.getMk());

    previous.trim();

    paramRepo.update(previous);
  }

  /** Удаление параметра. */
  public void delete(Long id)
  {
    paramRepo.delete(new Param(id));
  }

  /** Экспорт параметров. */
  public XSSFWorkbook export()
  {
    XSSFWorkbook book = new XSSFWorkbook();
    Sheet sheet = book.createSheet("Параметры");

    XSSFFont fontHeader = book.createFont();
    XSSFCellStyle styleHeader = book.createCellStyle();
    XSSFCellStyle styleParams = book.createCellStyle();

    fontHeader.setBold(true);

    styleHeader.setWrapText(true);
    styleHeader.setAlignment(CellStyle.ALIGN_CENTER);
    styleHeader.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
    styleHeader.setFont(fontHeader);

    styleParams.setAlignment(CellStyle.ALIGN_CENTER);
    styleParams.setVerticalAlignment(CellStyle.VERTICAL_CENTER);

    int line = 0, col = 0; // Нумерация строк и столбцов.

    Row row = sheet.createRow(line++);

    row.createCell(col++).setCellValue("UID");
    row.createCell(col++).setCellValue("Наименование параметра");
    row.createCell(col++).setCellValue("Тип");

    row.createCell(col++).setCellValue("Дельта\nминимум");
    row.createCell(col++).setCellValue("Минимум");
    row.createCell(col++).setCellValue("Максимум");
    row.createCell(col++).setCellValue("Дельта\nмаксимум");
    row.createCell(col++).setCellValue("Качество");

    row.createCell(col++).setCellValue("А");
    row.createCell(col++).setCellValue("В");
    row.createCell(col++).setCellValue("К");

    for (Cell cell : row) {
      cell.setCellStyle(styleHeader);
    }

    for (Param param : list()) {
      row = sheet.createRow(line++);
      col = 0; // Сброс номера стобца.

      row.createCell(col++).setCellValue(param.getUid());
      row.createCell(col++).setCellValue(param.getTitle());
      row.createCell(col++).setCellValue(param.getType().name());

      row.createCell(col++).setCellValue(param.getMinDelta());
      row.createCell(col++).setCellValue(param.getMin());
      row.createCell(col++).setCellValue(param.getMax());
      row.createCell(col++).setCellValue(param.getMaxDelta());
      row.createCell(col++).setCellValue(param.getQuality().name());

      row.createCell(col++).setCellValue(param.getMa());
      row.createCell(col++).setCellValue(param.getMv());
      row.createCell(col++).setCellValue(param.getMk());

      for (Cell cell : row) {
        if (cell.getColumnIndex() != 1) {
          cell.setCellStyle(styleParams);
        }
      }
    }

    sheet.createFreezePane(0, 1);

    Row rowHeader = sheet.getRow(0);
    int columns = rowHeader.getPhysicalNumberOfCells();

    for (int k = 0; k < columns; k++) {
      sheet.autoSizeColumn(k);
    }

    return book;
  }

  /** Парсинг списка параметров из файла. */
  public void parse(InputStream is) throws HandleException
  {
    try {
      XSSFWorkbook book = new XSSFWorkbook(is);
      XSSFSheet sheet = book.getSheetAt(0);

      int line = 0, col = 0; // Нумерация строк и столбцов.

      for (Row row : sheet) {
        line = row.getRowNum(); // Номер строки.
        if (line == 0) continue; // Исключение заголовка.
        col = 0; // Сброс номера стобца.

        String uid = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getStringCellValue().trim();
        String title = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getStringCellValue().trim();
        String type = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getStringCellValue().trim();

        Double minDelta = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getNumericCellValue();
        Double min = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getNumericCellValue();
        Double max = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getNumericCellValue();
        Double maxDelta = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getNumericCellValue();
        String quality = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getStringCellValue().trim();

        Double ma = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getNumericCellValue();
        Double mv = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getNumericCellValue();
        Double mk = row.getCell(col++, Row.CREATE_NULL_AS_BLANK).getNumericCellValue();

        if (uid.isEmpty() || title.isEmpty()) {
          continue; // Исключение пустых и ошибочных строк.
        }

        ParamType paramType = ParamType.getByName(type);
        ParamQuality paramQuality = ParamQuality.getByName(quality);

        if (paramType == null || paramQuality == null) {
          throw new HandleException(String.format(Messages.PARAM_PARSE_TYPE_ERROR, line + 1));
        }

        // Поиск предыдущего параметра.
        Param previous = paramRepo.one(uid);

        if (previous == null) {
          Param param = new Param();

          param.setUid(uid);
          param.setTitle(title);
          param.setType(paramType);

          param.setMin(Math.round(min));
          param.setMax(Math.round(max));
          param.setMinDelta(Math.round(minDelta));
          param.setMaxDelta(Math.round(maxDelta));
          param.setQuality(paramQuality);

          param.setMa(Math.round(ma));
          param.setMv(Math.round(mv));
          param.setMk(Math.round(mk));

          paramRepo.save(param);

        } else {
          if (previous.getType() != paramType || previous.getQuality() != paramQuality) {
            throw new HandleException(String.format(Messages.PARAM_PARSE_TYPE_WRONG, line + 1));
          }

          previous.setTitle(title);
          previous.setType(paramType);

          previous.setMin(Math.round(min));
          previous.setMax(Math.round(max));
          previous.setMinDelta(Math.round(minDelta));
          previous.setMaxDelta(Math.round(maxDelta));
          previous.setQuality(paramQuality);

          previous.setMa(Math.round(ma));
          previous.setMv(Math.round(mv));
          previous.setMk(Math.round(mk));

          paramRepo.update(previous);
        }
      }

    } catch (Exception ex) {
      if (ex instanceof HandleException) {
        throw new HandleException(ex.getMessage());
      }

      throw new HandleException(Messages.PARAM_PARSE_ERROR);
    }
  }
}