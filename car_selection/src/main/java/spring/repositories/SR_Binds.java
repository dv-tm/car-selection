package spring.repositories;

import entities.base.Repo;
import entities.binds.PropertyPair;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SR_Binds extends Repo<PropertyPair>
{
  @Override
  public Class support()
  {
    return PropertyPair.class;
  }

  public List<Object[]> listWithNoParams(Long propsId, Long paramId)
  {
    Query query = session().createSQLQuery("select  tv.data_value val1, tv.location loc from" +
      "  (select DISTINCT cp.prop_id ,cp.location, cp.data_value, cp.value_title" +
      " from car_props cp where cp.prop_id = :propId) tv" +
      " left join  prop_binds pb on pb.prop_id = tv.prop_id and pb.prop_value = tv.data_value and pb.location = tv.location  and pb.param_id = :paramId" +
      " where pb.id is null;")
      .setParameter("propId", propsId)
      .setParameter("paramId", paramId);
    return query.list();
  }

  public List<PropertyPair> list(Long propsId, Long paramId)
  {
    Query query = null;
    if (paramId == null) {
      query = session().createQuery("select pp from  PropertyPair pp inner join pp.prop pr inner join pp.param pa where pr.id = :propId")
        .setParameter("propId", propsId);
    } else {
      query = session().createQuery("select pp from  PropertyPair pp inner join pp.prop pr inner join pp.param pa where pr.id = :propId and pa.id=:paramId")
        .setParameter("propId", propsId).setParameter("paramId", paramId);
    }
    return query.list();
  }

  /** Возвращет значения для маппинга */
  public List<Object[]> listDifferentValues()
  {
    Query query = session().createSQLQuery("SELECT" +
      "  p1.ext_id, p1.id prop_id, p1.parent_id, p1.title prop_title, tv.location, tv.value_title, tv.data_value, pb.param_value, par.uid uid, par.type, par.title par_title," +
      "  CASE WHEN p1.parent_id IS NULL" +
      "    THEN p1.id" +
      "  WHEN p1.id = p1.parent_id" +
      "    THEN p1.id" +
      "  ELSE p1.parent_id" +
      "  END sort1," +
      "  CASE WHEN p1.parent_id IS NULL" +
      "    THEN 0" +
      "  WHEN p1.id = p1.parent_id" +
      "    THEN 0" +
      "  ELSE p1.id" +
      "  END sort2" +
      "  FROM props p1" +
      "  LEFT JOIN props p2 ON p1.parent_id = p2.id OR p1.parent_id IS NULL AND p2.id = p1.id" +
      "  left join (SELECT distinct cp.prop_id, cp.data_value, cp.location, cp.value_title from car_props cp ) tv on tv.prop_id = p1.id" +
      "  left join prop_binds pb on pb.prop_id = p1.id and tv.location = pb.location and pb.prop_value = tv.data_value" +
      "  left join params par on par.id = pb.param_id" +
      " ORDER BY sort1 ASC, sort2 ASC, p1.id;");
    return query.list();
  }

  public void savePropBind(String location, Long param_value, String prop_value, String param_id, Long prop_id)
  {
    Query query = session().createSQLQuery(" with upd as" +
      " (update prop_binds set (date_update, param_value) = (current_timestamp, :param_value)" +
      " where" +
      " (:location is null and location is null  or location = :location)" +
      " and (:prop_value is null and prop_value is null or prop_value=:prop_value )" +
      " and param_id = :param_id" +
      " and prop_id = :prop_id" +
      " RETURNING id)" +
      " INSERT INTO prop_binds (date_insert, date_update, location, param_value, prop_value, param_id, prop_id)" +
      "  select current_timestamp,current_timestamp,:location, :param_value, , :prop_value, :param_id, :prop_id" +
      "  where not exists(SELECT * from upd)")
      .setParameter("location", location)
      .setParameter("param_value", param_value)
      .setParameter("prop_value", prop_value)
      .setParameter("param_id", param_id)
      .setParameter("prop_id", prop_id);
    query.executeUpdate();
  }

  public void savePropBindbyExtId(String location, Long param_value, String prop_value, String uid, Long ext_id)
  {
    Query query = session().createSQLQuery("with upd as" +
      " (update prop_binds set (date_update, param_value) = (current_timestamp, :param_value)" +
      " where" +
      " (:location is null and location is null  or location = :location)" +
      " and (:prop_value is null and prop_value is null or prop_value=:prop_value )" +
      " and param_id = (select pr.id from params pr where pr.uid = :uid)" +
      " and prop_id = (select p.id from props p where p.ext_id = :ext_id)" +
      " RETURNING id)" +
      " INSERT INTO prop_binds (date_insert, date_update, location, param_value, prop_value, param_id, prop_id)" +
      "  select current_timestamp,current_timestamp,:location, :param_value, :prop_value, par.id, pp.id" +
      " FROM props pp " +
      " join params par on 1=1" +
      " where par.uid = :uid and pp.ext_id = :ext_id and not exists(SELECT * from upd)")
      .setParameter("location", location)
      .setParameter("param_value", param_value)
      .setParameter("prop_value", prop_value)
      .setParameter("uid", uid)
      .setParameter("ext_id", ext_id);
    query.executeUpdate();
  }
}