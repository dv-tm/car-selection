package spring.repositories;

import entities.base.Repo;
import entities.cars.PropertyGroup;
import org.springframework.stereotype.Repository;

@Repository
public class SR_Group extends Repo<PropertyGroup>
{
  @Override
  public Class support()
  {
    return PropertyGroup.class;
  }
}