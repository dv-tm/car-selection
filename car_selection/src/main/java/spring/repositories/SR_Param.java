package spring.repositories;

import entities.base.Repo;
import entities.params.Param;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SR_Param extends Repo<Param>
{
  @Override
  public Class support()
  {
    return Param.class;
  }

  public Param one(String uid)
  {
    Criteria criteria = this.criteria();
    criteria.add(Restrictions.eq("uid", uid));
    List<Param> params = criteria.list();

    return params.isEmpty() ? null : params.get(0);
  }
}