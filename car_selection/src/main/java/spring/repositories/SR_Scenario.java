package spring.repositories;

import entities.base.Repo;
import entities.main.Scenario;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SR_Scenario extends Repo<Scenario>
{
  @Override
  public Class support()
  {
    return Scenario.class;
  }

  public Scenario one(String uid)
  {
    Criteria criteria = this.criteria();
    criteria.add(Restrictions.eq("uid", uid));
    List<Scenario> scenarios = criteria.list();

    return scenarios.isEmpty() ? null : scenarios.get(0);
  }
}