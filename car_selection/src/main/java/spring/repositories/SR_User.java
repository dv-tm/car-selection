package spring.repositories;

import entities.base.Repo;
import entities.users.User;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SR_User extends Repo<User>
{
  @Override
  public Class support()
  {
    return User.class;
  }

  public User one(String username)
  {
    Criteria criteria = this.criteria();
    criteria.add(Restrictions.eq("username", username));
    List<User> users = criteria.list();

    return users.isEmpty() ? null : users.get(0);
  }
}