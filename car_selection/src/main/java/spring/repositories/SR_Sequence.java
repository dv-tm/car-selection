package spring.repositories;

import entities.base.Repo;
import entities.main.Sequence;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SR_Sequence extends Repo<Sequence>
{
  @Override
  public Class support()
  {
    return Sequence.class;
  }

  public Sequence one(String radicals)
  {
    Criteria criteria = this.criteria();
    criteria.add(Restrictions.eq("radicals", radicals));
    List<Sequence> sequences = criteria.list();

    return sequences.isEmpty() ? null : sequences.get(0);
  }
}