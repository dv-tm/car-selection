package spring.repositories;

import db.Calculate;
import db.Calculate3;
import entities.base.Repo;
import entities.main.Event;
import org.springframework.stereotype.Repository;

@Repository
public class SR_Event extends Repo<Event>
{
  @Override
  public Class support()
  {
    return Event.class;
  }

  public Calculate calculate(Long requestId, Long eventId)
  {
    Calculate calculation = new Calculate(requestId, eventId);
    session().doWork(calculation);
    return calculation;
  }

  public Calculate3 calculate2(Long requestId, Long eventId)
  {
    Calculate3 calculation = new Calculate3(requestId, eventId);
    session().doWork(calculation);
    return calculation;
  }

}