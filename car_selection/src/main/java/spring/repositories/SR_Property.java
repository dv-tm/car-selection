package spring.repositories;

import entities.base.Repo;
import entities.cars.Property;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SR_Property extends Repo<Property>
{
  @Override
  public Class support()
  {
    return Property.class;
  }

  public List<Property> listByGroupId(Integer groupId)
  {
    Query query;
    if (groupId == null) {
      query = session().createQuery("select p from Property p inner join p.group pg where pg.id is null order by p.parent_id asc, p.id asc");
    } else {
//      SQLQuery q = session().createSQLQuery("SELECT {prop.*} FROM from t_property p inner join p.group pg where pg.id is null order by p.parent_id asc, p.id asc\"")
//        .addEntity("cat", CProperty.class)
//        .addEntity("mother", CProperty.class);

      query = session().createSQLQuery("SELECT p1.*,\n" +
        "  CASE WHEN p1.parent_id IS NULL\n" +
        "    THEN p1.id\n" +
        "  WHEN p1.id = p1.parent_id\n" +
        "    THEN p1.id\n" +
        "  ELSE p1.parent_id\n" +
        "  END sort1,\n" +
        "  CASE WHEN p1.parent_id IS NULL\n" +
        "    THEN 0\n" +
        "  WHEN p1.id = p1.parent_id\n" +
        "    THEN 0\n" +
        "  ELSE p1.id\n" +
        "  END sort2\n" +
        "FROM props p1\n" +
        "  LEFT JOIN props p2 ON p1.parent_id = p2.id OR p1.parent_id IS NULL AND p2.id = p1.id\n" +
        "WHERE p2.group_id = :groupId\n" +
        "ORDER BY p2.id ASC, p1.id ASC;").addEntity(Property.class).setParameter("groupId", groupId);
    }
    return query.list();
  }
}