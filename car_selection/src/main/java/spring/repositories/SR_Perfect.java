package spring.repositories;

import entities.base.Repo;
import entities.params.ParamPerfect;
import org.springframework.stereotype.Repository;

@Repository
public class SR_Perfect extends Repo<ParamPerfect>
{
  @Override
  public Class support()
  {
    return ParamPerfect.class;
  }
}