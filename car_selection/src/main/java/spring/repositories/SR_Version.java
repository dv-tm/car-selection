package spring.repositories;

import entities.base.Repo;
import entities.params.ParamVersion;
import org.springframework.stereotype.Repository;

@Repository
public class SR_Version extends Repo<ParamVersion>
{
  @Override
  public Class support()
  {
    return ParamVersion.class;
  }
}