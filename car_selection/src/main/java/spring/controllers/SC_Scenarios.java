package spring.controllers;

import app.Exceptions;
import app.Messages;
import entities.main.Scenario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import spring.services.SS_Scenario;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/scenarios")
public class SC_Scenarios
{
  @Autowired
  private SS_Scenario scenarioService;

  @RequestMapping(value = "/show")
  public String scenarios()
  {
    return "scenarios/scenarios";
  }

  @RequestMapping(value = "/show", params = {"scenario"})
  public String one(Map<String, Object> models, @RequestParam Long scenario)
  {
    List<String> errors = new ArrayList<>();
    models.put("errors", errors);

    try {
      models.put("scenario", scenarioService.one(scenario));

    } catch (Exception ex) {
      Exceptions.handle(ex, errors, Messages.SCENARIO_LOAD_ERROR);
    }

    return "scenarios/scenario";
  }

  @ResponseBody
  @RequestMapping(value = "/list")
  public Map<String, Object> list()
  {
    List<String> errors = new ArrayList<>();
    Map<String, Object> models = new HashMap<>();
    models.put("errors", errors);

    this.scenarioList(models, errors);

    return models;
  }

  /** Единый обработчик для возврата списка сценариев. */
  private void scenarioList(Map<String, Object> models, List<String> errors)
  {
    try {
      models.put("scenarios", scenarioService.list());

    } catch (Exception ex) {
      Exceptions.handle(ex, errors, Messages.SCENARIO_LIST_ERROR);
    }
  }

  @ResponseBody
  @RequestMapping(value = "/insert")
  public Map<String, Object> insert(@RequestBody Scenario scenario)
  {
    List<String> errors = new ArrayList<>();
    Map<String, Object> models = new HashMap<>();
    models.put("errors", errors);

    try {
      scenarioService.insert(scenario);

    } catch (Exception ex) {
      Exceptions.handle(ex, errors, Messages.SCENARIO_INSERT_ERROR);
    }

    this.scenarioList(models, errors);

    return models;
  }

  @ResponseBody
  @RequestMapping(value = "/update")
  public Map<String, Object> update(@RequestBody Scenario scenario)
  {
    List<String> errors = new ArrayList<>();
    Map<String, Object> models = new HashMap<>();
    models.put("errors", errors);

    try {
      scenarioService.update(scenario);

    } catch (Exception ex) {
      Exceptions.handle(ex, errors, Messages.SCENARIO_UPDATE_ERROR);
    }

    return models;
  }

  @ResponseBody
  @RequestMapping(value = "/delete")
  public Map<String, Object> delete(@RequestParam Long scenario)
  {
    List<String> errors = new ArrayList<>();
    Map<String, Object> models = new HashMap<>();
    models.put("errors", errors);

    try {
      scenarioService.delete(scenario);

    } catch (Exception ex) {
      Exceptions.handle(ex, errors, Messages.SCENARIO_DELETE_ERROR);
    }

    return models;
  }

  @RequestMapping("/export")
  public void export(HttpServletResponse response) throws Exception
  {
    response.setContentType("application/octet-stream");
    response.setHeader("Content-Disposition", "attachment; filename=scenarios.xlsx");
    scenarioService.export().write(response.getOutputStream());
  }

  @RequestMapping("/import")
  public String parse(@RequestParam MultipartFile file, RedirectAttributes ras)
  {
    List<String> errors = new ArrayList<>();

    if (file == null || file.isEmpty()) {
      errors.add(Messages.SCENARIO_READ_ERROR);
      ras.addFlashAttribute("errors", errors);

    } else {
      ras.addFlashAttribute("errors", errors);

      try {
        scenarioService.parse(file.getInputStream());

      } catch (Exception ex) {
        Exceptions.handle(ex, errors, Messages.SCENARIO_PARSE_ERROR);
      }
    }

    return "redirect:/scenarios/show";
  }
}