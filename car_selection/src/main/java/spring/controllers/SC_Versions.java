package spring.controllers;

import app.Exceptions;
import app.Messages;
import entities.params.Param;
import entities.params.ParamVersion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import spring.services.SS_Event;
import spring.services.SS_Param;
import spring.services.SS_Scenario;
import spring.services.SS_Sequence;
import spring.services.SS_Version;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Controller
@RequestMapping(value = "/versions")
public class SC_Versions
{
  @Autowired
  private SS_Version versionService;

  @Autowired
  private SS_Param paramService;

  @Autowired
  private SS_Event eventService;

  @Autowired
  private SS_Scenario scenarioService;

  @Autowired
  private SS_Sequence sequenceService;

  @ResponseBody
  @RequestMapping(value = "/show")
  public Map<String, Object> params(@RequestParam("id") Long id, @RequestParam("type") String type)
  {
    Map<String, Object> models = new HashMap<>();
    List<String> errors = new ArrayList<>();

    models.put("errors", errors);

    try {
      List<Param> params = paramService.list();
      Set<ParamVersion> versions = new LinkedHashSet<>();

      if ("EVENT".equals(type)) {
        versions = eventService.one(id).getParams();
      } else if ("SCENARIO".equals(type)) {
        versions = scenarioService.one(id).getParams();
      } else if ("SEQUENCE".equals(type)) {
        versions = sequenceService.one(id).getParams();
      }

      SC_Versions.exclude(params, versions);

      models.put("params", params);
      models.put("versions", versions);

    } catch (Exception ex) {
      Exceptions.handle(ex, errors, Messages.EVENT_LOAD_ERROR); // TODO: Текст ошибки.
    }

    return models;
  }

  @ResponseBody
  @RequestMapping(value = "/insert")
  public Map<String, Object> insert(@RequestBody ParamVersion version)
  {
    Map<String, Object> models = new HashMap<>();
    List<String> errors = new ArrayList<>();

    models.put("errors", errors);

    try {
      versionService.insert(version);

    } catch (Exception ex) {
      Exceptions.handle(ex, errors, Messages.VERSION_INSERT_ERROR);
    }

    return models;
  }

  @ResponseBody
  @RequestMapping(value = "/update")
  public Map<String, Object> update(@RequestBody ParamVersion version)
  {
    Map<String, Object> models = new HashMap<>();
    List<String> errors = new ArrayList<>();

    models.put("errors", errors);

    try {
      versionService.update(version);

    } catch (Exception ex) {
      Exceptions.handle(ex, errors, Messages.VERSION_UPDATE_ERROR);
    }

    return models;
  }

  @ResponseBody
  @RequestMapping(value = "/delete")
  public Map<String, Object> delete(@RequestParam("version") Long id)
  {
    Map<String, Object> models = new HashMap<>();
    List<String> errors = new ArrayList<>();

    models.put("errors", errors);

    try {
      versionService.delete(id);

    } catch (Exception ex) {
      Exceptions.handle(ex, errors, Messages.VERSION_DELETE_ERROR);
    }

    return models;
  }

  public static void exclude(List<Param> params, Set<ParamVersion> versions)
  {
    for (ParamVersion version : versions) {
      for (Iterator<Param> it = params.iterator(); it.hasNext(); ) {
        Param param = it.next();
        if (param.getId().equals(version.getParam().getId())) {
          it.remove();
          break;
        }
      }
    }
  }
}