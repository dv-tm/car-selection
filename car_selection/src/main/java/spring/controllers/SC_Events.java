package spring.controllers;

import app.Exceptions;
import app.Messages;
import entities.main.Event;
import entities.main.Modality;
import entities.main.Scenario;
import entities.params.Param;
import entities.params.ParamVersion;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import spring.services.SS_Event;
import spring.services.SS_Param;
import spring.services.SS_Scenario;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Controller
@RequestMapping(value = "/events")
public class SC_Events
{
  @Autowired
  private SS_Event eventService;

  @Autowired
  private SS_Scenario scenarioService;

  @Autowired
  private SS_Param paramService;

  @RequestMapping(value = "/show")
  public String events()
  {
    return "events/events";
  }

  @RequestMapping(value = "/show", params = {"event"})
  public String event()
  {
    return "events/event";
  }

  @ResponseBody
  @RequestMapping(value = "/one")
  public Map<String, Object> one(@RequestParam Long event)
  {
    List<String> errors = new ArrayList<>();
    Map<String, Object> models = new HashMap<>();
    models.put("errors", errors);

    try {
      // Получение эвента с прогруженными данными.
      Event deepEvent = eventService.deepOne(event);
      Set<ParamVersion> versions = deepEvent.getParams();

      // Получение списков параметров и сценариев.
      List<Scenario> scenarios = scenarioService.list();
      List<Param> params = paramService.list();

      // Исключение параметров по версиям.
      SC_Versions.exclude(params, versions);

      models.put("event", deepEvent);
      models.put("scenarios", scenarios);
      models.put("params", params);

    } catch (Exception ex) {
      Exceptions.handle(ex, errors, Messages.EVENT_LOAD_ERROR);
    }

    return models;
  }

  @ResponseBody
  @RequestMapping(value = "/list")
  public Map<String, Object> list()
  {
    List<String> errors = new ArrayList<>();
    Map<String, Object> models = new HashMap<>();
    models.put("errors", errors);

    this.eventList(models, errors);

    return models;
  }

  @ResponseBody
  @RequestMapping(value = "/insert")
  public Map<String, Object> insert(@RequestBody Event event)
  {
    List<String> errors = new ArrayList<>();
    Map<String, Object> models = new HashMap<>();
    models.put("errors", errors);

    try {
      eventService.insert(event);

    } catch (Exception ex) {
      Exceptions.handle(ex, errors, Messages.EVENT_INSERT_ERROR);
    }

    this.eventList(models, errors);

    return models;
  }

  /** Единый обработчик для возврата списка эвентов. */
  private void eventList(Map<String, Object> models, List<String> errors)
  {
    try {
      models.put("modalities", Modality.values());
      models.put("events", eventService.list());

    } catch (Exception ex) {
      Exceptions.handle(ex, errors, Messages.EVENT_LIST_ERROR);
    }
  }

  @ResponseBody
  @RequestMapping(value = "/update")
  public Map<String, Object> update(@RequestBody Event event)
  {
    List<String> errors = new ArrayList<>();
    Map<String, Object> models = new HashMap<>();
    models.put("errors", errors);

    try {
      eventService.update(event);

    } catch (Exception ex) {
      Exceptions.handle(ex, errors, Messages.EVENT_UPDATE_ERROR);
    }

    return models;
  }

  @ResponseBody
  @RequestMapping(value = "/merge")
  public Map<String, Object> merge(@RequestBody Event event)
  {
    List<String> errors = new ArrayList<>();
    Map<String, Object> models = new HashMap<>();
    models.put("errors", errors);

    try {
      eventService.merge(event);

      Event previous = eventService.one(event.getId());

      models.put("perfects", previous.perfects());

    } catch (Exception ex) {
      Exceptions.handle(ex, errors, Messages.EVENT_MERGE_ERROR);
    }

    return models;
  }

  @ResponseBody
  @RequestMapping(value = "/delete")
  public Map<String, Object> delete(@RequestParam("event") Long event)
  {
    List<String> errors = new ArrayList<>();
    Map<String, Object> models = new HashMap<>();
    models.put("errors", errors);

    try {
      eventService.delete(event);

    } catch (Exception ex) {
      Exceptions.handle(ex, errors, Messages.EVENT_DELETE_ERROR);
    }

    return models;
  }

  @ResponseBody
  @RequestMapping(value = "/calculate")
  public Map<String, Object> calc(@RequestParam("event") Long event)
  {
    List<String> errors = new ArrayList<>();
    Map<String, Object> models = new HashMap<>();
    models.put("errors", errors);

    try {
      models.put("cars", eventService.calculate(new Event(event)));

    } catch (Exception ex) {
      Exceptions.handle(ex, errors, Messages.EVENT_CALCULATE_ERROR);
    }

    return models;
  }

  @ResponseBody
  @RequestMapping(value = "/searchCar")
  public Map<String, Object> searchCar(@RequestParam("event") Long event)
  {
    List<String> errors = new ArrayList<>();
    Map<String, Object> models = new HashMap<>();
    models.put("errors", errors);

    try {
      models.put("cars", eventService.calculate(new Event(event)));

    } catch (Exception ex) {
      Exceptions.handle(ex, errors, Messages.EVENT_CALCULATE_ERROR);
    }

    return models;
  }

  @RequestMapping("/searchCarXlsx")
  public void getXls(Map<String, Object> model, HttpServletResponse response, @RequestParam("event") Long event) throws Exception
  {
    response.setContentType("application/octet-stream");
    response.setHeader("Content-Disposition", "attachment; filename=Cars.xlsx");
    SXSSFWorkbook fo = eventService.searchCarXlsx(new Event(event));
    fo.write(response.getOutputStream());
    fo.dispose();
  }
}