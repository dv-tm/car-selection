package spring.controllers;

import app.Exceptions;
import app.Messages;
import entities.params.Param;
import entities.params.ParamQuality;
import entities.params.ParamType;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import spring.services.SS_Binds;
import spring.services.SS_Group;
import spring.services.SS_Param;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/params")
public class SC_Params
{
  @Autowired
  private SS_Param paramService;

  @Autowired
  private SS_Group groupService;

  @Autowired
  private SS_Binds bindsService;

  @RequestMapping(value = "/show")
  public String params()
  {
    return "params/params";
  }

  @RequestMapping(value = "/binds")
  public String binds(Map<String, Object> models)
  {
    List<String> errors = new ArrayList<>();
    models.put("errors", errors);

    try {
      models.put("params", paramService.list());
      models.put("groups", groupService.list());
      models.put("pairs", bindsService.list());

    } catch (Exception ex) {
      Exceptions.handle(ex, errors, Messages.BINDS_LISTS_LOAD_ERROR);
    }

    return "params/binds";
  }

  @ResponseBody
  @RequestMapping(value = "/list")
  public Map<String, Object> list()
  {
    List<String> errors = new ArrayList<>();
    Map<String, Object> models = new HashMap<>();
    models.put("errors", errors);

    this.paramList(models, errors);

    return models;
  }

  /** Единый обработчик для возврата списка параметров. */
  private void paramList(Map<String, Object> models, List<String> errors)
  {
    try {
      models.put("params", paramService.list());
      models.put("qualities", ParamQuality.values());
      models.put("types", ParamType.values());

    } catch (Exception ex) {
      Exceptions.handle(ex, errors, Messages.PARAM_LIST_ERROR);
    }
  }

  @ResponseBody
  @RequestMapping(value = "/insert")
  public Map<String, Object> insert(@RequestBody Param param)
  {
    List<String> errors = new ArrayList<>();
    Map<String, Object> models = new HashMap<>();
    models.put("errors", errors);

    try {
      paramService.insert(param);

    } catch (Exception ex) {
      Exceptions.handle(ex, errors, Messages.PARAM_INSERT_ERROR);
    }

    this.paramList(models, errors);

    return models;
  }

  @ResponseBody
  @RequestMapping(value = "/update")
  public Map<String, Object> update(@RequestBody Param param)
  {
    List<String> errors = new ArrayList<>();
    Map<String, Object> models = new HashMap<>();
    models.put("errors", errors);

    try {
      paramService.update(param);

    } catch (Exception ex) {
      Exceptions.handle(ex, errors, Messages.PARAM_UPDATE_ERROR);
    }

    return models;
  }

  @ResponseBody
  @RequestMapping(value = "/delete")
  public Map<String, Object> delete(@RequestParam Long param)
  {
    List<String> errors = new ArrayList<>();
    Map<String, Object> models = new HashMap<>();
    models.put("errors", errors);

    try {
      paramService.delete(param);

    } catch (Exception ex) {
      Exceptions.handle(ex, errors, Messages.PARAM_DELETE_ERROR);
    }

    return models;
  }

  @ResponseBody
  @RequestMapping(value = "/pairs")
  public Map<String, Object> pairs(@RequestParam Long propId, @RequestParam Long paramId)
  {
    Map<String, Object> models = new HashMap<>();

    models.put("pairs", bindsService.list(propId, paramId));

    return models;
  }

  @RequestMapping("/getXlsx")
  public void getXls(Map<String, Object> model, HttpServletResponse response) throws Exception
  {
    response.setContentType("application/octet-stream");
    response.setHeader("Content-Disposition", "attachment; filename=prop_binds.xlsx");
    SXSSFWorkbook fo = bindsService.getAllValues();
    fo.write(response.getOutputStream());
    fo.dispose();
  }

  @RequestMapping("/loadXlsx")
  public String loadXml(Map<String, Object> model, @RequestParam(value = "xlsFile", required = false) MultipartFile file, RedirectAttributes ras)
  {
    if (file == null || file.isEmpty()) {
      ras.addFlashAttribute("error", "File not found");
    } else {
      List<String> errors = new ArrayList<>();
      model.put("errors", errors);
      try {
        bindsService.loadValuesFromXlsx(file.getInputStream());
      } catch (IOException e) {
        Exceptions.handle(e, errors, Messages.UNKNOWN_ERROR);
      }
    }
    return "redirect:/params/binds";
  }

  @RequestMapping("/export")
  public void export(HttpServletResponse response) throws Exception
  {
    response.setContentType("application/octet-stream");
    response.setHeader("Content-Disposition", "attachment; filename=params.xlsx");
    paramService.export().write(response.getOutputStream());
  }

  @RequestMapping("/import")
  public String parse(@RequestParam MultipartFile file, RedirectAttributes ras)
  {
    List<String> errors = new ArrayList<>();

    if (file == null || file.isEmpty()) {
      errors.add(Messages.PARAM_READ_ERROR);
      ras.addFlashAttribute("errors", errors);

    } else {
      ras.addFlashAttribute("errors", errors);

      try {
        paramService.parse(file.getInputStream());

      } catch (Exception ex) {
        Exceptions.handle(ex, errors, Messages.PARAM_PARSE_ERROR);
      }
    }

    return "redirect:/params/show";
  }
}