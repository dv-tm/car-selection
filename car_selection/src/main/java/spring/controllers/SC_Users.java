package spring.controllers;

import app.Exceptions;
import app.Messages;
import entities.users.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import spring.services.SS_User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/users")
public class SC_Users
{
  @Autowired
  private SS_User userService;

  @RequestMapping(value = "/list")
  public String list(Map<String, Object> models)
  {
    models.put("user", new User());
    models.put("users", userService.list());

    return "users/users";
  }

  @RequestMapping(value = "/insert")
  public String insert(@ModelAttribute("user") User user, RedirectAttributes ras)
  {
    List<String> errors = new ArrayList<>();

    try {
      userService.insert(user);

    } catch (Exception ex) {
      Exceptions.handle(ex, errors, Messages.USER_INSERT_ERROR);
    }

    ras.addFlashAttribute("errors", errors);

    return "redirect:/users/list";
  }

  @ResponseBody
  @RequestMapping(value = "/update")
  public Map<String, Object> update(@RequestBody User user)
  {
    Map<String, Object> models = new HashMap<>();
    List<String> errors = new ArrayList<>();

    models.put("errors", errors);

    try {
      userService.update(user);

    } catch (Exception ex) {
      Exceptions.handle(ex, errors, Messages.USER_UPDATE_ERROR);
    }

    return models;
  }

  @ResponseBody
  @RequestMapping(value = "/delete")
  public Map<String, Object> delete(@RequestParam Long id)
  {
    Map<String, Object> models = new HashMap<>();
    List<String> errors = new ArrayList<>();

    models.put("errors", errors);

    try {
      userService.delete(id);

    } catch (Exception ex) {
      Exceptions.handle(ex, errors, Messages.USER_DELETE_ERROR);
    }

    return models;
  }
}