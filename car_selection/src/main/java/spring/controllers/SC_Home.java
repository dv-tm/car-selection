package spring.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SC_Home
{
  @RequestMapping(value = "/")
  public String home()
  {
    return "index";
  }

  @RequestMapping(value = "/login")
  public String login()
  {
    return "login";
  }
}