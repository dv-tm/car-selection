package spring.controllers;

import app.Exceptions;
import app.Messages;
import entities.main.Sequence;
import entities.rules.ARule;
import entities.rules.BRule;
import entities.rules.PRule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import spring.services.SS_Sequence;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/sequences")
public class SC_Sequences
{
  @Autowired
  private SS_Sequence sequenceService;

  @RequestMapping(value = "/show")
  public String sequences()
  {
    return "sequences/sequences";
  }

  @RequestMapping(value = "/show", params = {"sequence"})
  public String one(Map<String, Object> models, @RequestParam Long sequence)
  {
    List<String> errors = new ArrayList<>();
    models.put("errors", errors);

    try {
      models.put("sequence", sequenceService.one(sequence));

    } catch (Exception ex) {
      Exceptions.handle(ex, errors, Messages.SEQUENCE_LOAD_ERROR);
    }

    return "sequences/sequence";
  }

  @ResponseBody
  @RequestMapping(value = "/list")
  public Map<String, Object> list()
  {
    List<String> errors = new ArrayList<>();
    Map<String, Object> models = new HashMap<>();
    models.put("errors", errors);

    this.sequenceList(models, errors);

    return models;
  }

  /** Единый обработчик для возврата списка секвенций. */
  private void sequenceList(Map<String, Object> models, List<String> errors)
  {
    try {
      models.put("sequences", sequenceService.list());
      models.put("ARules", ARule.values());
      models.put("BRules", BRule.values());
      models.put("PRules", PRule.values());

    } catch (Exception ex) {
      Exceptions.handle(ex, errors, Messages.SEQUENCE_LIST_ERROR);
    }
  }

  @ResponseBody
  @RequestMapping(value = "/insert")
  public Map<String, Object> insert(@RequestBody Sequence sequence)
  {
    List<String> errors = new ArrayList<>();
    Map<String, Object> models = new HashMap<>();
    models.put("errors", errors);

    try {
      sequenceService.insert(sequence);

    } catch (Exception ex) {
      Exceptions.handle(ex, errors, Messages.SEQUENCE_INSERT_ERROR);
    }

    this.sequenceList(models, errors);

    return models;
  }

  @ResponseBody
  @RequestMapping(value = "/update")
  public Map<String, Object> update(@RequestBody Sequence sequence)
  {
    List<String> errors = new ArrayList<>();
    Map<String, Object> models = new HashMap<>();
    models.put("errors", errors);

    try {
      sequenceService.update(sequence);

    } catch (Exception ex) {
      Exceptions.handle(ex, errors, Messages.SEQUENCE_UPDATE_ERROR);
    }

    return models;
  }

  @ResponseBody
  @RequestMapping(value = "/delete")
  public Map<String, Object> delete(@RequestParam Long sequence)
  {
    List<String> errors = new ArrayList<>();
    Map<String, Object> models = new HashMap<>();
    models.put("errors", errors);

    try {
      sequenceService.delete(sequence);

    } catch (Exception ex) {
      Exceptions.handle(ex, errors, Messages.SEQUENCE_DELETE_ERROR);
    }

    return models;
  }

  @RequestMapping("/export")
  public void export(HttpServletResponse response) throws Exception
  {
    response.setContentType("application/octet-stream");
    response.setHeader("Content-Disposition", "attachment; filename=sequences.xlsx");
    sequenceService.export().write(response.getOutputStream());
  }

  @RequestMapping("/import")
  public String parse(@RequestParam MultipartFile file, RedirectAttributes ras)
  {
    List<String> errors = new ArrayList<>();

    if (file == null || file.isEmpty()) {
      errors.add(Messages.SEQUENCE_READ_ERROR);
      ras.addFlashAttribute("errors", errors);

    } else {
      ras.addFlashAttribute("errors", errors);

      try {
        sequenceService.parse(file.getInputStream());

      } catch (Exception ex) {
        Exceptions.handle(ex, errors, Messages.SEQUENCE_PARSE_ERROR);
      }
    }

    return "redirect:/sequences/show";
  }
}