package spring.controllers;

import app.Exceptions;
import app.Messages;
import entities.main.Event;
import entities.main.Modality;
import entities.params.Param;
import entities.params.ParamVersion;
import entities.users.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import spring.services.SS_Event;
import spring.services.SS_Param;
import spring.services.SS_User;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping(value = "/init")
public class SC_Launch
{
  @Autowired
  private SS_User userService;

  @Autowired
  private SS_Param paramService;

  @Autowired
  private SS_Event eventService;

  @RequestMapping(value = "/users")
  public String users()
  {
    List<String> errors = new ArrayList<>();

    try {

      User user = new User();

      user.setAlias("Администратор");
      user.setUsername("admin@truenorth.pro");
      user.setPassword("admin");

      userService.insert(user);

    } catch (Exception ex) {
      Exceptions.handle(ex, errors, Messages.USER_INSERT_ERROR);
    }

    return "login";
  }
}