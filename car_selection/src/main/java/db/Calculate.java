package db;

import org.hibernate.jdbc.Work;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;

public class Calculate implements Work
{
  private Long requestId;
  private Long eventId;
  private HashMap<Long, CalcProp> props = new HashMap<>();
  private HashMap<Long, CalcCar> cars = new HashMap<>();

  public Calculate(Long requestId, Long eventId)
  {
    this.requestId = requestId;
    this.eventId = eventId;
  }

  @Override
  public void execute(Connection connection) throws SQLException
  {

    CallableStatement statement = connection.prepareCall("{ call find_cars( ?, ?, ?, ?, ? ) }");

    //statement.registerOutParameter(1, Types.OTHER);
    statement.setLong(1, requestId);
    statement.setLong(2, eventId);
    statement.registerOutParameter(3, Types.OTHER);
    statement.registerOutParameter(4, Types.OTHER);
    statement.registerOutParameter(5, Types.OTHER);
    statement.execute();


//    ResultSet res = (ResultSet) statement.getObject(1);
//    res.next();
    ResultSet car_props_ref = (ResultSet)statement.getObject(3);
    ResultSet car_val_ref = (ResultSet)statement.getObject(4);
    ResultSet cars_ref = (ResultSet)statement.getObject(5);

    try {
      while (cars_ref.next()) {
        CalcCar car = new CalcCar(
          cars_ref.getLong("id"),
          cars_ref.getString("title"),
          cars_ref.getDouble("price"),
          cars_ref.getDouble("full_price")
        );
        cars.put(car.id, car);
      }

      while (car_props_ref.next()) {
        CalcProp prop = new CalcProp(
          car_props_ref.getLong("id"),
          car_props_ref.getString("title"),
          car_props_ref.getLong("ext_id"),
          car_props_ref.getString("location")
        );
        props.put(prop.id, prop);
      }

      while (car_val_ref.next()) {
        CalcPropValue propVal = new CalcPropValue(
          car_val_ref.getLong("id"),
          car_val_ref.getLong("prop_id"),
          car_val_ref.getLong("car_id"),
          car_val_ref.getString("location"),
          car_val_ref.getString("data_value"),
          car_val_ref.getLong("option_id")
        );

        CalcCar car = cars.get(propVal.car_id);
        CalcProp prop = props.get(propVal.prop_id);

        if (car != null && prop != null) {
          car.props.put(prop, propVal);
        }
        props.put(prop.id, prop);
      }
    } finally {

      try {
        car_props_ref.close();
      } catch (Throwable e) {
      }
      try {
        cars_ref.close();
      } catch (Throwable e) {
      }
      try {
        car_val_ref.close();
      } catch (Throwable e) {
      }
    }
  }

  public HashMap<Long, CalcProp> getProps()
  {
    return props;
  }

  public HashMap<Long, CalcCar> getCars()
  {
    return cars;
  }
}
