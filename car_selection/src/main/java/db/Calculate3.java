package db;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.hibernate.jdbc.Work;

import entities.params.ParamRank;

public class Calculate3 implements Work
{
  private Long requestId;
  private Long eventId;
  private SXSSFWorkbook workbook;

  public Calculate3(Long requestId, Long eventId)
  {
    this.requestId = requestId;
    this.eventId = eventId;
  }

  @Override
  public void execute(Connection connection) throws SQLException
  {

    HashMap<String, Integer> propsCols = new HashMap<>();
    HashMap<String, CarsRate> cars = new HashMap<>();
    HashMap<Long, Param> paramRows = new HashMap<>();

    connection.setAutoCommit(false);
    CallableStatement statement = connection.prepareCall("{ call calculate_cars_limit(?, ?) }");
    statement.setLong(1, requestId);
    statement.setLong(2, eventId);
    statement.execute();

    try {

      Logger log = Logger.getLogger(this.getClass());
      try {
        workbook = new SXSSFWorkbook(5000);
        Sheet sheet = workbook.createSheet("Sheet");
        Sheet carParamsSheet = workbook.createSheet("CarParams");
        int headrow = 1;
        int headCol = 1;
        Row rowHeader = sheet.createRow(headrow);
        Row paramRowHeader = carParamsSheet.createRow(headrow);
        Row paramRowHeaderPrev = carParamsSheet.createRow(headrow-1);
        Row rowPrevHeader = sheet.createRow(headrow-1);
        CellStyle myStyle = workbook.createCellStyle();
        myStyle.setRotation((short)90);
        rowHeader.createCell(headCol).setCellValue("Авто/Характеристики");
        paramRowHeader.createCell(headCol).setCellValue("Параметры/Авто");



        Statement statement2 = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
        statement2.setFetchSize(100);
        ResultSet cars_ref = statement2.executeQuery("SELECT tc.*, c.title, c.ext_id FROM temp_cars tc JOIN car c ON c.id = tc.id where tc.state = 0 ORDER BY tc.id, tc.car_num");

        //Располагаем Авто
        Integer curRow = headrow + 1;
        Integer curCol = headCol + 2;
        while (cars_ref.next()) {
          CarsRate carsRate = new CarsRate();
          //Для страницы авто
          carsRate.carRow = sheet.createRow(curRow);
          carsRate.carRow.createCell(headCol).setCellValue(cars_ref.getString("title"));
          cars.put(cars_ref.getLong("car_num") + "_" + cars_ref.getLong("id"), carsRate);

          //Для страницы параметров
          paramRowHeaderPrev.createCell(curCol).setCellValue(cars_ref.getString("title"));
          paramRowHeader.createCell(curCol).setCellValue(cars_ref.getString("ext_id"));
          carsRate.paramCarCol = curCol;
          carsRate.price = cars_ref.getDouble("price");
          carsRate.price_a = cars_ref.getDouble("price_a");
          carsRate.price_b = cars_ref.getDouble("price_b");

          curRow++;
          curCol++;
        }

        try {
          cars_ref.close();
        } catch (Throwable e) {
        }



        //Создаем характеристики
        curCol = headCol + 1;
        ResultSet car_props_ref = statement2.executeQuery(
          "SELECT DISTINCT " +
          "      p.id, " +
          "      p.title, " +
          "      p.ext_id, " +
          "      tp.location " +
          "    FROM tmp_props tp " +
          "      JOIN props p ON tp.prop_id = p.id " +
          "    ORDER BY p.id ");

        while (car_props_ref.next()) {
          Cell cell;
          cell = rowPrevHeader.createCell(curCol);
          cell.setCellStyle(myStyle);
          cell.setCellValue(car_props_ref.getString("title") + " [" + car_props_ref.getString("location") + "]");
          rowHeader.createCell(curCol).setCellValue(car_props_ref.getLong("ext_id"));
          propsCols.put(car_props_ref.getLong("id") + "_" + car_props_ref.getString("location"), curCol);
          curCol++;
        }

        try {
          car_props_ref.close();
        } catch (Throwable e) {
        }



        ResultSet car_val_ref = statement2.executeQuery("SELECT * FROM tmp_props");

        while (car_val_ref.next()) {
          CarsRate car = cars.get(car_val_ref.getLong("car_num") + "_" +  car_val_ref.getLong("car_id"));
          if (car == null) {
            continue;
          }

          Integer col = propsCols.get(car_val_ref.getLong("prop_id") + "_" + car_val_ref.getString("location"));
          if (col == null) {
            continue;
          }
          car.carRow.createCell(col).setCellValue(car_val_ref.getString("data_value"));
        }

        try {
          car_val_ref.close();
        } catch (Throwable e) {
        }



        //Параметры авто
        ResultSet car_params = statement2.executeQuery("SELECT tp.*, p.title FROM temp_perfects tp join params p on p.id = tp.param_id where tp.rank in ('A','B','C') ORDER BY tp.rank");
        curRow = headrow + 1;
        while (car_params.next()) {
          Row row = carParamsSheet.createRow(curRow);
          row.createCell(headCol).setCellValue(car_params.getString("title"));
          row.createCell(headCol+1).setCellValue(car_params.getString("rank"));
          paramRows.put(car_params.getLong("param_id"), new Param(row, ParamRank.getByName(car_params.getString("rank"))));
          curRow++;
        }

        try {
          car_params.close();
        } catch (Throwable e) {
        }

        //Релевантность авто
        ResultSet car_param_rate = statement2.executeQuery(
          "select  car_num, car_id, param_id, avg(rate) rate " +
          "    from " +
          "      tmp_props " +
          "    where param_id is not null " +
          "    GROUP BY car_num, car_id, param_id;");

        while (car_param_rate.next()) {
          CarsRate car = cars.get(car_param_rate.getLong("car_num") + "_" + car_param_rate.getLong("car_id"));

          if (car == null) {
            continue;
          }

          Param param = paramRows.get(car_param_rate.getLong("param_id"));
          if (param == null) {
            continue;
          }

          param.row.createCell(car.paramCarCol).setCellValue(car_param_rate.getDouble("rate"));
          car.sumRate(param.rank, car_param_rate.getDouble("rate"));
        }

        try {
          car_param_rate.close();
        } catch (Throwable e) {
        }

        //Итоговые результаты
        Row sumRateRow = carParamsSheet.createRow(headrow + paramRows.size() + 3);
        Row rateARow = carParamsSheet.createRow(headrow + paramRows.size() + 4);
        Row rateBRow = carParamsSheet.createRow(headrow + paramRows.size() + 5);
        Row rateCRow = carParamsSheet.createRow(headrow + paramRows.size() + 6);

        Row fullPriceRow = carParamsSheet.createRow(headrow + paramRows.size() + 9);
        Row priceRow = carParamsSheet.createRow(headrow + paramRows.size() + 10);
        Row priceARow = carParamsSheet.createRow(headrow + paramRows.size() + 11);
        Row priceBRow = carParamsSheet.createRow(headrow + paramRows.size() + 12);
        Row priceCRow = carParamsSheet.createRow(headrow + paramRows.size() + 13);

        sumRateRow.createCell(headCol+1).setCellValue("Релевантность авто");
        rateARow.createCell(headCol+1).setCellValue("Релевантность по рангу A");
        rateBRow.createCell(headCol+1).setCellValue("Релевантность по Рангу B");
        rateCRow.createCell(headCol+1).setCellValue("Релевантность по Рангу C");

        fullPriceRow.createCell(headCol+1).setCellValue("Полная стоимость авто");
        priceRow.createCell(headCol + 1).setCellValue("Базовая стоимость авто");
        priceARow.createCell(headCol + 1).setCellValue("Стоимость опций Ранга А");
        priceBRow.createCell(headCol + 1).setCellValue("Стоимость опций Ранга B");
        priceCRow.createCell(headCol + 1).setCellValue("Стоимость опций Ранга C");

        for (CarsRate car : cars.values()) {
          sumRateRow.createCell(car.paramCarCol).setCellValue(car.rate_a + car.rate_b + car.rate_c);
          rateARow.createCell(car.paramCarCol).setCellValue(car.rate_a);
          rateBRow.createCell(car.paramCarCol).setCellValue(car.rate_b);
          rateCRow.createCell(car.paramCarCol).setCellValue(car.rate_c);
          fullPriceRow.createCell(car.paramCarCol).setCellValue(car.price + car.price_a + car.price_b);
          priceRow.createCell(car.paramCarCol).setCellValue(car.price);
          priceARow.createCell(car.paramCarCol).setCellValue(car.price_a);
          priceBRow.createCell(car.paramCarCol).setCellValue(car.price_b);
          priceCRow.createCell(car.paramCarCol).setCellValue(car.price_c);
        }


      } catch (Exception e) {
        log.warn("getXlsx()", e);
      }

    } finally {

    }
  }


  public SXSSFWorkbook getWorkbook()
  {
    return workbook;
  }


  private static final class CarsRate {
    public Double rate_a = 0d;
    public Double rate_b = 0d;
    public Double rate_c = 0d;

    public Double price = 0d;
    public Double price_a = 0d;
    public Double price_b = 0d;
    public Double price_c = 0d;
    public Row carRow;
    public Integer paramCarCol;

    public void sumRate(ParamRank rank, Double val) {
      if (rank == null || val == null) {
        return;
      } else if (rank == ParamRank.A) {
        rate_a += val;
      } else if (rank == ParamRank.B) {
        rate_b += val;
      } else if (rank == ParamRank.C) {
        rate_c += val;
      }
    }
  }

  private static final class Param {
    public Row row;
    public ParamRank rank;

    public Param(Row row, ParamRank rank)
    {
      this.row = row;
      this.rank = rank;
    }
  }
}
