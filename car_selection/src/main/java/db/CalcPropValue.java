package db;

/**
 * Created by Иван on 30.07.2015.
 * Просто хранилка данных.
 */
public class CalcPropValue
{
    public Long cp_id;
    public Long prop_id;
    public Long car_id;
    public String location;
    public String data_value;
    public Long option_id;

    public CalcPropValue(Long cp_id, Long prop_id, Long car_id, String location, String data_value, Long option_id)
    {
        this.cp_id = cp_id;
        this.prop_id = prop_id;
        this.car_id = car_id;
        this.location = location;
        this.data_value = data_value;
        this.option_id = option_id;
    }
}
