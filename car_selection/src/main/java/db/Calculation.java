//package db;
//
//import entities.params.ParamPerfect;
//import org.hibernate.jdbc.Work;
//
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.sql.Struct;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Set;
//
//public class Calculation implements Work
//{
//  private Long sum;
//  private Set<ParamPerfect> params;
//  private ArrayList<CalcCar> cars = new ArrayList<>();
//
//  public Calculation(Long sum, Set<ParamPerfect> params)
//  {
//    this.sum = sum;
//    this.params = params;
//  }
//
//  @Override
//  public void execute(Connection connection) throws SQLException
//  {
//
//    List<Struct> structs = new ArrayList<>();
//
//    for (ParamPerfect param : params) {
//      structs.add(connection.createStruct("perfect_param", new Object[]{
//          param.getParam().getId(),
//          param.getMin(),
//          param.getMax(),
//          param.getMinDelta(),
//          param.getMaxDelta(),
//          param.getRank().name(),
//          param.getPriority()
//        }
//      ));
//    }
//
//    PreparedStatement statement = connection.prepareStatement(
//      " SELECT c.id, c.title, c.price, t.state, t.reason, p.id prop_id, p.title prop_title, p.ext_id, " +
//        "  cp.location, cp.data_value prop_value, op.id opt_id," +
//        "  op.title opt_title, op.price opt_price, par.uid, par.title par_title" +
//        "  from" +
//        "  car c" +
//        "  join test4(?, ?) t on t.id = c.id" +
//        "  left join car_props cp on cp.id = t.cp_id" +
//        "  left join props p on p.id = cp.prop_id" +
//        "  left join options op on op.id = cp.option_id" +
//        "  left join prop_binds pb on pb.id = t.pb_id" +
//        "  left join params par on par.id = pb.param_id" +
//        "  order by c.id asc, p.id");
//    statement.setLong(1, sum);
//    statement.setArray(2, connection.createArrayOf("perfect_param", structs.toArray(new Object[0])));
//    ResultSet res = statement.executeQuery();
//
//    CalcCar currentCar = new CalcCar(-1L, null, null, null, null, null);
//    while (res.next()) {
//      Long carId = res.getLong("id");
//      Long propId = res.getLong("prop_id");
//      if (carId == null || carId < 1) {
//        continue;
//      }
//      if (!carId.equals(currentCar.id)) {
//        currentCar = new CalcCar(
//          carId,
//          res.getString("title"),
//          res.getLong("price"),
//          res.getInt("state"),
//          res.getString("reason"),
//          new ArrayList<CalcProp>());
//        cars.add(currentCar);
//      }
//
//      if (propId != null && propId > 0) {
//        currentCar.props.add(new CalcProp(
//          propId,
//          res.getLong("ext_id"),
//          res.getString("prop_title"),
//          res.getString("location"),
//          res.getString("prop_value"),
//          res.getLong("opt_id"),
//          res.getString("opt_title"),
//          res.getLong("opt_price"),
//          res.getString("uid")
//        ));
//      }
//    }
//    res.close();
//  }
//
//  public ArrayList<CalcCar> getCars()
//  {
//    return cars;
//  }
//}