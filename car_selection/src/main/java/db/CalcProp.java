package db;

import org.apache.poi.ss.usermodel.Row;

/**
 * Created by Иван on 30.07.2015.
 * Просто хранилка данных.
 */
public class CalcProp {
    public Long id;
    public String title;
    public Long ext_id;
    public String location;

    public Row row;

  public CalcProp(Long id, String title, Long ext_id, String location)
  {
    this.id = id;
    this.title = title;
    this.ext_id = ext_id;
    this.location = location;
  }
}
