package db;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Иван on 30.07.2015.
 * Просто хранилка данных.
 */
public class CalcCar {
    public Long id;
    public String title;
    public Double price;
    public Double full_price;
    public HashMap<CalcProp, CalcPropValue> props = new HashMap<>();

    public int colId;

    public CalcCar(Long id, String title, Double price, Double full_price)
    {
        this.id = id;
        this.title = title;
        this.price = price;
        this.full_price = full_price;
    }
}
