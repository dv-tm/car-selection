package app;

public class Patterns
{
  public static final String EMAIL =  "^([A-Za-z0-9_.-]+)@([A-Za-z0-9_.-]+)\\.([A-Za-z.]{2,6})$";
  public static final String PASSWORD =  "^([a-zA-Z0-9]{4,16})$";
  public static final String RADICALS_SEQUENCE =  "^(?!.*([1-7]).*\\1)[1-7*]{7}$";
  public static final String RADICALS_PROFILE =  "^(?!.*([1-7]).*\\1)[1-7\\s(){}\\[\\]]{1,20}$";
}