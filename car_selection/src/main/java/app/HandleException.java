package app;

public class HandleException extends Exception
{
  public HandleException()
  {
    super();
  }

  public HandleException(String message)
  {
    super(message);
  }
}