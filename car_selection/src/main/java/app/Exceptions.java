package app;

import org.springframework.security.authentication.BadCredentialsException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Exceptions
{
  /** Обработка ошибок при авторизации. */
  public static String auth(Exception ex)
  {
    if (ex instanceof BadCredentialsException) {
      return Messages.AUTH_DATA_WRONG;
    } else {
      return Messages.UNKNOWN_ERROR;
    }
  }

  /** Обработка всевозможных ошибок в контроллерах. */
  public static void handle(Exception ex, List<String> errors, String message)
  {
    if (ex instanceof ConstraintViolationException) {
      errors.addAll(parse((ConstraintViolationException) ex));
    } else if (ex instanceof HandleException) {
      errors.add(ex.getMessage());
    } else {
      errors.add(message);
    }
  }

  /** Парсинг ошибок валидации в строковый список. */
  private static List<String> parse(ConstraintViolationException ex)
  {
    List<String> messages = new ArrayList<>();

    for (Iterator iterator = ex.getConstraintViolations().iterator(); iterator.hasNext(); ) {
      ConstraintViolation next = (ConstraintViolation) iterator.next();

      messages.add(next.getMessage());
    }

    return messages;
  }
}