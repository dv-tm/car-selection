package app;

public class Messages
{
  public static final String UNKNOWN_ERROR = "Произошла ошибка...";
  public static final String AUTH_DATA_WRONG = "Неверная авторизация...";

  public static final String USER_NAME_WRONG = "Логин не соответствует шаблону адреса электронной почты.";
  public static final String USER_NAME_LENGTH = "Максимальная длина логина - 40 символов.";
  public static final String USER_PASSWORD_WRONG = "Пароль должен состоять из латинских букв и цифр, и быть длинной от 4 до 16 символов.";
  public static final String USER_ALIAS_WRONG = "Максимальная длина псевдонима - 40 символов.";

  public static final String USER_ALREADY_USED = "Указанный логин пользователя уже используется.";
  public static final String USER_INSERT_ERROR = "При добавлении пользователя произошла ошибка!";
  public static final String USER_UPDATE_ERROR = "При изменении данных пользователя произошла ошибка!";
  public static final String USER_UPDATE_MISSED = "Пользователь с указанным идентификатором не найден.";
  public static final String USER_DELETE_ERROR = "При удалении пользователя произошла ошибка!";

  public static final String EVENT_TITLE_WRONG = "Допустимая длина наименования эвента - от 2 до 80 символов.";
  public static final String EVENT_RADICALS_WRONG = "Допустимый радикальный профиль - 20 символов.";
  public static final String EVENT_MODALITY_WRONG = "Неверно задана модальность.";
  public static final String EVENT_MONEY_WRONG = "Не задано ограничение по денежной сумме.";

  public static final String EVENT_LOAD_ERROR = "При загрузке эвента произошла ошибка.";
  public static final String EVENT_LIST_ERROR = "При получении списка эвентов произошла ошибка.";
  public static final String EVENT_INSERT_ERROR = "При добавлении эвента произошла ошибка.";
  public static final String EVENT_UPDATE_ERROR = "При изменении эвента произошла ошибка.";
  public static final String EVENT_UPDATE_MISSED = "Эвент с указанным идентификатором не найден.";
  public static final String EVENT_MERGE_ERROR = "При уточнении параметров эвента произошла ошибка.";
  public static final String EVENT_SEQUENCE_MISSED = "Не удалось определить секвенцию по радикальному профилю.";
  public static final String EVENT_DELETE_ERROR = "При удалении эвента произошла ошибка.";
  public static final String EVENT_CALCULATE_ERROR = "При расчете эвента произошла ошибка.";

  public static final String SCENARIO_UID_WRONG = "Не задан уникальный идентификатор сценария.";
  public static final String SCENARIO_TITLE_WRONG = "Допустимая длина наименования сценария - от 2 до 80 символов.";

  public static final String SCENARIO_LOAD_ERROR = "При загрузке сценария произошла ошибка.";
  public static final String SCENARIO_LIST_ERROR = "При получении списка сценариев произошла ошибка.";
  public static final String SCENARIO_ALREADY_USED = "Указанный уникальный идентификатор сценария уже используется.";
  public static final String SCENARIO_INSERT_ERROR = "При добавлении сценария произошла ошибка.";
  public static final String SCENARIO_UPDATE_ERROR = "При изменении сценария произошла ошибка.";
  public static final String SCENARIO_UPDATE_MISSED = "Сценарий с указанным идентификатором не найден.";
  public static final String SCENARIO_DELETE_ERROR = "При удалении сценария произошла ошибка.";

  public static final String SCENARIO_READ_ERROR = "При чтении файла сценариев произошла ошибка.";
  public static final String SCENARIO_PARSE_ERROR = "При парсинге файла сценариев произошла ошибка.";

  public static final String SEQUENCE_TITLE_WRONG = "Допустимая длина описания секвенции - от 2 до 80 символов.";
  public static final String SEQUENCE_RADICALS_WRONG = "Допустимая радикальная последовательность - 20 символов.";
  public static final String SEQUENCE_RULE_A_WRONG = "Не задана стратегия для A ранга.";
  public static final String SEQUENCE_RULE_B_WRONG = "Не задана стратегия для B ранга.";
  public static final String SEQUENCE_RULE_P_WRONG = "Не задана стратегия для динамических ресурсов.";

  public static final String SEQUENCE_LOAD_ERROR = "При загрузке секвенции произошла ошибка.";
  public static final String SEQUENCE_LIST_ERROR = "При получении списка секвенций произошла ошибка.";
  public static final String SEQUENCE_ALREADY_USED = "Указанная радикальная последовательность уже используется.";
  public static final String SEQUENCE_INSERT_ERROR = "При добавлении секвенции произошла ошибка.";
  public static final String SEQUENCE_UPDATE_ERROR = "При изменении секвенции произошла ошибка.";
  public static final String SEQUENCE_UPDATE_MISSED = "Секвенция с указанным идентификатором не найдена.";
  public static final String SEQUENCE_DELETE_ERROR = "При удалении секвенции произошла ошибка.";

  public static final String SEQUENCE_READ_ERROR = "При чтении файла секвенций произошла ошибка.";
  public static final String SEQUENCE_PARSE_ERROR = "При парсинге файла секвенций произошла ошибка.";

  public static final String PARAM_UID_WRONG = "Не задан уникальный идентификатор параметра.";
  public static final String PARAM_TITLE_WRONG = "Допустимая длина наименования параметра - от 2 до 80 символов.";
  public static final String PARAM_TYPE_WRONG = "Неверно задан тип параметра.";
  public static final String PARAM_VALUE_WRONG = "Неверно заданы значения параметра.";
  public static final String PARAM_QUALITY_WRONG = "Неверно задано направление качества параметра.";
  public static final String PARAM_MODALITY_WRONG = "Неверно заданы значения модальности для параметра.";

  public static final String PARAM_LIST_ERROR = "При получении списка параметров произошла ошибка.";
  public static final String PARAM_ALREADY_USED = "Указанный уникальный идентификатор параметра уже используется.";
  public static final String PARAM_INSERT_ERROR = "При добавлении параметра произошла ошибка.";
  public static final String PARAM_UPDATE_MISSED = "Параметр с указанным идентификатором не найден.";
  public static final String PARAM_UPDATE_ERROR = "При изменении параметра произошла ошибка.";
  public static final String PARAM_DELETE_ERROR = "При удалении параметра произошла ошибка.";

  public static final String PARAM_READ_ERROR = "При чтении файла параметров произошла ошибка.";
  public static final String PARAM_PARSE_ERROR = "При парсинге файла параметров произошла ошибка.";
  public static final String PARAM_PARSE_TYPE_ERROR = "Неверно заданы тип или качество для парамера на %s строке.";
  public static final String PARAM_PARSE_TYPE_WRONG = "Тип и качество параметра на %s строке не совместимы с существующими.";

  public static final String VERSION_INSERT_ERROR = "При добавлении параметра произошла ошибка.";
  public static final String VERSION_UPDATE_ERROR = "При изменении параметра произошла ошибка.";
  public static final String VERSION_UPDATE_MISSED = "Параметр с указанным идентификатором не найден.";
  public static final String VERSION_MERGE_ERROR = "При слиянии диапазонов параметров произошла ошибка.";
  public static final String VERSION_DELETE_ERROR = "При удалении параметра произошла ошибка.";

  public static final String PERFECT_REQUEST_WRONG = "Не задан идентификатор запроса для идеального параметра.";
  public static final String PERFECT_VALUE_WRONG = "Неверно заданы значения идеального параметра.";
  public static final String PERFECT_RANK_WRONG = "Не задан ранг идеального параметра.";
  public static final String PERFECT_PRIORITY_WRONG = "Не задан приоритет идеального параметра.";
  public static final String PERFECT_RATE_WRONG = "Неверно задана релевантность идеального параметра.";

  public static final String MAP_PROPERTY_MISSED = "Характеристика с указанным идентификатором не найдена.";
  public static final String MAP_PARAM_MISSED = "Параметр с указанным идентификатором не найден.";

  public static final String BINDS_LISTS_LOAD_ERROR = "При загрузке списков параметров и характеристик произошла ошибка.";
  public static final String PAIR_UPDATE_MISSED = "Соответствие с указанным идентификатором не найдено.";

  public static final String ACCESS_URL_NOT_FOUND = "По указанному адресу ничего не найдено.";
}