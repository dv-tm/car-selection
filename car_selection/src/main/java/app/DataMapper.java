package app;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module;
import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module.Feature;

public class DataMapper extends ObjectMapper
{
  public DataMapper()
  {
    Hibernate4Module module = new Hibernate4Module();
    module.disable(Feature.USE_TRANSIENT_ANNOTATION);

    this.registerModule(module);
  }
}