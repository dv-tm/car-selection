package utils;

import org.apache.poi.ss.usermodel.Cell;

/**
 * Created by Иван on 28.07.2015.
 */
public class ParseCellValue {

    private static String NULL_VALUE = "null";

    public static Long getLongValue (Cell cell) {
        if (cell == null) {
            return null;
        }
        if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
            String val  = cell.getStringCellValue();
            if (val != null && val.trim().length() > 0) {
              if (val.equalsIgnoreCase(NULL_VALUE)) {
                return null;
              }
                try {
                    return Long.valueOf(val);
                } catch (NumberFormatException e) {
                    return null;
                }
            } else {
                return null;
            }
        } else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
            return Math.round(cell.getNumericCellValue());
        }
        return null;
    }

    public static String getStringValue (Cell cell) {
        if (cell == null) {
            return null;
        }
        if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
          if (NULL_VALUE.equalsIgnoreCase(cell.getStringCellValue())) {
            return null;
          }
          return cell.getStringCellValue();
        } else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
            return String.valueOf(cell.getNumericCellValue());
        }
        return null;
    }
}
