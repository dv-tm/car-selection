package entities.params;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

@JsonFormat(shape = Shape.OBJECT)
public enum ParamType
{
  PERCENT("Процентный"),
  DISCRETE("Дискретный"),
  STATIC("Статический");

  private String title;

  ParamType(String title)
  {
    this.title = title;
  }

  public String getTitle()
  {
    return title;
  }

  public String getName()
  {
    return this.name();
  }

  public static ParamType getByName(String name)
  {
    if (name != null) {
      for (ParamType value : values()) {
        if (name.equals(value.name())) {
          return value;
        }
      }
    }

    return null;
  }
}