package entities.params;

import app.HandleException;
import app.Messages;
import entities.base.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.validator.constraints.Range;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "param_perfects",
  indexes = {@Index(name = "param_perfects_request__idx", columnList = "request")})
public class ParamPerfect extends Data
{
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @NotNull(message = Messages.PERFECT_REQUEST_WRONG)
  @Column(name = "request", nullable = false)
  private Long request;

  @ManyToOne(fetch = FetchType.EAGER)
  @OnDelete(action = OnDeleteAction.CASCADE)
  @NotNull(message = Messages.PARAM_UPDATE_MISSED)
  @JoinColumn(name = "param_id")
  private Param param;

  @NotNull(message = Messages.PERFECT_VALUE_WRONG)
  @Column(name = "min", nullable = false)
  private Long min;

  @NotNull(message = Messages.PERFECT_VALUE_WRONG)
  @Column(name = "max", nullable = false)
  private Long max;

  @NotNull(message = Messages.PERFECT_VALUE_WRONG)
  @Column(name = "min_delta", nullable = false)
  private Long minDelta;

  @NotNull(message = Messages.PERFECT_VALUE_WRONG)
  @Column(name = "max_delta", nullable = false)
  private Long maxDelta;

  @Enumerated(EnumType.STRING)
  @NotNull(message = Messages.PERFECT_RANK_WRONG)
  @Column(name = "rank", nullable = false)
  private ParamRank rank;

  @NotNull(message = Messages.PERFECT_PRIORITY_WRONG)
  @Column(name = "priority", nullable = false)
  private Long priority = 1L;

  @NotNull(message = Messages.PERFECT_RATE_WRONG)
  @Range(min = 0L, max = 100L, message = Messages.PERFECT_RATE_WRONG)
  @Column(name = "rate", nullable = false)
  private Long rate = 100L;

  public ParamPerfect()
  {
    // Конструктор по-умолчанию.
  }

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public Long getRequest()
  {
    return request;
  }

  public void setRequest(Long request)
  {
    this.request = request;
  }

  public Param getParam()
  {
    return param;
  }

  public void setParam(Param param)
  {
    this.param = param;
  }

  public String getTitle()
  {
    return param.getTitle();
  }

  public ParamQuality getQuality()
  {
    return param.getQuality();
  }

  public ParamType getType()
  {
    return param.getType();
  }

  public String getRadicals()
  {
    return param.getRadicals();
  }

  public Long getMin()
  {
    return min;
  }

  public void setMin(Long min)
  {
    this.min = min;
  }

  public Long getMax()
  {
    return max;
  }

  public void setMax(Long max)
  {
    this.max = max;
  }

  public Long getMinDelta()
  {
    return minDelta;
  }

  public void setMinDelta(Long minDelta)
  {
    this.minDelta = minDelta;
  }

  public Long getMaxDelta()
  {
    return maxDelta;
  }

  public void setMaxDelta(Long maxDelta)
  {
    this.maxDelta = maxDelta;
  }

  public ParamRank getRank()
  {
    return rank;
  }

  public void setRank(ParamRank rank)
  {
    this.rank = rank;
  }

  public Long getPriority()
  {
    return priority;
  }

  public void setPriority(Long priority)
  {
    this.priority = priority;
  }

  public void increasePriority()
  {
    this.priority++;
  }

  public Long getRate()
  {
    return rate;
  }

  public void setRate(Long rate)
  {
    this.rate = rate;
  }

  public void update(ParamVersion version, ParamRank rank)
  {
    this.min = version.getMin();
    this.max = version.getMax();
    this.minDelta = version.getMinDelta();
    this.maxDelta = version.getMaxDelta();
    this.param = version.getParam();
    this.rank = rank;
  }

  public void merge(ParamVersion version) throws HandleException
  {
    if (version.getType() == ParamType.STATIC) {
      return; // Исключение статических версиий параметров.
    }

    Long min = this.min;
    Long max = this.max;

    Long minDelta = min - this.minDelta;
    Long maxDelta = max + this.maxDelta;

    Long minVersion = version.getMin();
    Long maxVersion = version.getMax();

    Long minDeltaVersion = minVersion - version.getMinDelta();
    Long maxDeltaVersion = maxVersion + version.getMaxDelta();

    // Расчет пересечения для Min и Max.
    min = minVersion > min ? minVersion : min;
    max = maxVersion < max ? maxVersion : max;

    // Расчет пересечения для дельт Min и Max.
    minDelta = minDeltaVersion > minDelta ? minDeltaVersion : minDelta;
    maxDelta = maxDeltaVersion < maxDelta ? maxDeltaVersion : maxDelta;

    Long ratio = 1L; // Отношение новой релевантности к старой.

    if (min > max) {
      if (minDelta > maxDelta) {
        throw new HandleException(Messages.VERSION_MERGE_ERROR);
      } else {
        ratio = (maxDelta - minDelta) / ((maxDelta - max) + (min - minDelta));
        min = max = minDelta + (min - minDelta) * ratio;
      }
    }

    this.min = min;
    this.max = max;

    this.minDelta = min - minDelta;
    this.maxDelta = maxDelta - max;

    this.rate = this.rate * ratio;
  }
}