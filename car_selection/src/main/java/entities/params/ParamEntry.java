package entities.params;

import app.Messages;
import com.fasterxml.jackson.annotation.JsonIgnore;
import entities.base.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name = "param_entries")
public class ParamEntry extends Data
{
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @JsonIgnore
  @ManyToOne(fetch = FetchType.LAZY)
  @OnDelete(action = OnDeleteAction.CASCADE)
  @NotNull(message = Messages.PARAM_UPDATE_MISSED)
  @JoinColumn(name = "param_id")
  private Param param;

  @NotNull(message = Messages.PARAM_TITLE_WRONG)
  @Size(min = 2, max = 80, message = Messages.PARAM_TITLE_WRONG)
  @Column(name = "title", length = 80, nullable = false)
  private String title;

  @NotNull(message = Messages.PARAM_VALUE_WRONG)
  @Column(name = "value", nullable = false)
  private Long value = 0L;

  @NotNull(message = Messages.PARAM_VALUE_WRONG)
  @Column(name = "radicals", nullable = false)
  private String radicals = "***";

  @CreationTimestamp
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "date_insert", nullable = false, updatable = false)
  private Date dateInsert;

  @UpdateTimestamp
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "date_update", nullable = false, updatable = true)
  private Date dateUpdate;

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public Param getParam()
  {
    return param;
  }

  public void setParam(Param param)
  {
    this.param = param;
  }

  public String getTitle()
  {
    return title;
  }

  public void setTitle(String title)
  {
    this.title = title;
  }

  public Long getValue()
  {
    return value;
  }

  public void setValue(Long value)
  {
    this.value = value;
  }

  public String getRadicals()
  {
    return radicals;
  }

  public void setRadicals(String radicals)
  {
    this.radicals = radicals;
  }

  public Date getDateInsert()
  {
    return dateInsert;
  }

  public void setDateInsert(Date date)
  {
    this.dateInsert = date;
  }

  public Date getDateUpdate()
  {
    return dateUpdate;
  }

  public void setDateUpdate(Date date)
  {
    this.dateUpdate = date;
  }
}