package entities.params;

import app.Messages;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import entities.base.Data;
import entities.main.Event;
import entities.main.Scenario;
import entities.main.Sequence;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "param_versions", uniqueConstraints = {
  @UniqueConstraint(columnNames = {"param_id", "event_id", "scenario_id", "sequence_id"})
})
public class ParamVersion extends Data
{
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @ManyToOne(fetch = FetchType.EAGER)
  @OnDelete(action = OnDeleteAction.CASCADE)
  @NotNull(message = Messages.PARAM_UPDATE_MISSED)
  @JoinColumn(name = "param_id")
  private Param param;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "event_id")
  private Event event;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "scenario_id")
  private Scenario scenario;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "sequence_id")
  private Sequence sequence;

  @NotNull(message = Messages.PARAM_VALUE_WRONG)
  @Column(name = "min", nullable = false)
  private Long min = 0L;

  @NotNull(message = Messages.PARAM_VALUE_WRONG)
  @Column(name = "max", nullable = false)
  private Long max = 0L;

  @NotNull(message = Messages.PARAM_VALUE_WRONG)
  @Column(name = "min_delta", nullable = false)
  private Long minDelta = 0L;

  @NotNull(message = Messages.PARAM_VALUE_WRONG)
  @Column(name = "max_delta", nullable = false)
  private Long maxDelta = 0L;

  @CreationTimestamp
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "date_insert", nullable = false, updatable = false)
  private Date dateInsert;

  @UpdateTimestamp
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "date_update", nullable = false, updatable = true)
  private Date dateUpdate;

  public ParamVersion()
  {
    // Конструктор по-умолчанию.
  }

  public ParamVersion(Long id)
  {
    this.id = id;
  }

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public Param getParam()
  {
    return param;
  }

  public void setParam(Param param)
  {
    this.param = param;
  }

  @JsonIgnore
  public Event getEvent()
  {
    return event;
  }

  @JsonProperty
  public void setEvent(Event event)
  {
    this.event = event;
  }

  @JsonIgnore
  public Scenario getScenario()
  {
    return scenario;
  }

  @JsonProperty
  public void setScenario(Scenario scenario)
  {
    this.scenario = scenario;
  }

  @JsonIgnore
  public Sequence getSequence()
  {
    return sequence;
  }

  @JsonProperty
  public void setSequence(Sequence sequence)
  {
    this.sequence = sequence;
  }

  public String getUid()
  {
    return param.getUid();
  }

  public String getTitle()
  {
    return param.getTitle();
  }

  public ParamQuality getQuality()
  {
    return param.getQuality();
  }

  public ParamType getType()
  {
    return param.getType();
  }

  public String getRadicals()
  {
    return param.getRadicals();
  }

  public Long getMa()
  {
    return param.getMa();
  }

  public Long getMv()
  {
    return param.getMv();
  }

  public Long getMk()
  {
    return param.getMk();
  }

  public Long getMin()
  {
    return min;
  }

  public void setMin(Long min)
  {
    this.min = min;
  }

  public Long getMax()
  {
    return max;
  }

  public void setMax(Long max)
  {
    this.max = max;
  }

  public Long getMinDelta()
  {
    return minDelta;
  }

  public void setMinDelta(Long minDelta)
  {
    this.minDelta = minDelta;
  }

  public Long getMaxDelta()
  {
    return maxDelta;
  }

  public void setMaxDelta(Long maxDelta)
  {
    this.maxDelta = maxDelta;
  }

  public Date getDateInsert()
  {
    return dateInsert;
  }

  public void setDateInsert(Date date)
  {
    this.dateInsert = date;
  }

  public Date getDateUpdate()
  {
    return dateUpdate;
  }

  public void setDateUpdate(Date date)
  {
    this.dateUpdate = date;
  }
}