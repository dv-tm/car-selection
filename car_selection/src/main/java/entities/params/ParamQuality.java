package entities.params;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

@JsonFormat(shape = Shape.OBJECT)
public enum ParamQuality
{
  FALL("Ниже"),
  RISE("Выше");

  private String title;

  ParamQuality(String title)
  {
    this.title = title;
  }

  public String getTitle()
  {
    return title;
  }

  public String getName()
  {
    return this.name();
  }

  public static ParamQuality getByName(String name)
  {
    if (name != null) {
      for (ParamQuality value : values()) {
        if (name.equals(value.name())) {
          return value;
        }
      }
    }

    return null;
  }
}