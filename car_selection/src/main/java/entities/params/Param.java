package entities.params;

import app.Messages;
import entities.base.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.validator.constraints.Range;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "params")
public class Param extends Data
{
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @NotNull(message = Messages.PARAM_UID_WRONG)
  @Size(min = 1, max = 12, message = Messages.PARAM_UID_WRONG)
  @Column(name = "uid", length = 12, unique = true, nullable = false)
  private String uid;

  @NotNull(message = Messages.PARAM_TITLE_WRONG)
  @Size(min = 2, max = 80, message = Messages.PARAM_TITLE_WRONG)
  @Column(name = "title", length = 80, nullable = false)
  private String title;

  @Enumerated(EnumType.STRING)
  @NotNull(message = Messages.PARAM_TYPE_WRONG)
  @Column(name = "type", length = 10, nullable = false)
  private ParamType type;

  @NotNull(message = Messages.PARAM_VALUE_WRONG)
  @Column(name = "min", nullable = false)
  private Long min = 0L;

  @NotNull(message = Messages.PARAM_VALUE_WRONG)
  @Column(name = "max", nullable = false)
  private Long max = 100L;

  @NotNull(message = Messages.PARAM_VALUE_WRONG)
  @Column(name = "min_delta", nullable = false)
  private Long minDelta = 0L;

  @NotNull(message = Messages.PARAM_VALUE_WRONG)
  @Column(name = "max_delta", nullable = false)
  private Long maxDelta = 0L;

  @Enumerated(EnumType.STRING)
  @NotNull(message = Messages.PARAM_QUALITY_WRONG)
  @Column(name = "quality", nullable = false)
  private ParamQuality quality;

  @NotNull(message = Messages.PARAM_MODALITY_WRONG)
  @Range(min = 0L, max = 100L, message = Messages.PARAM_MODALITY_WRONG)
  @Column(name = "m_a", nullable = false)
  private Long ma = 100L;

  @NotNull(message = Messages.PARAM_MODALITY_WRONG)
  @Range(min = 0L, max = 100L, message = Messages.PARAM_MODALITY_WRONG)
  @Column(name = "m_v", nullable = false)
  private Long mv = 100L;

  @NotNull(message = Messages.PARAM_MODALITY_WRONG)
  @Range(min = 0L, max = 100L, message = Messages.PARAM_MODALITY_WRONG)
  @Column(name = "m_k", nullable = false)
  private Long mk = 100L;

  @NotNull(message = Messages.PARAM_VALUE_WRONG)
  @Column(name = "radicals", nullable = false)
  private String radicals = "***";

  @OneToMany(mappedBy = "param", cascade = CascadeType.REMOVE, orphanRemoval = true)
  private Set<ParamEntry> entries;

  @CreationTimestamp
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "date_insert", nullable = false, updatable = false)
  private Date dateInsert;

  @UpdateTimestamp
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "date_update", nullable = false, updatable = true)
  private Date dateUpdate;

  public Param()
  {
    // Конструктор по-умолчанию.
  }

  public Param(Long id)
  {
    this.id = id;
  }

  public Param(String uid)
  {
    this.uid = uid;
  }

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public String getUid()
  {
    return uid;
  }

  public void setUid(String uid)
  {
    this.uid = uid;
  }

  public String getTitle()
  {
    return title;
  }

  public void setTitle(String title)
  {
    this.title = title;
  }

  public ParamType getType()
  {
    return type;
  }

  public void setType(ParamType type)
  {
    this.type = type;
  }

  public Long getMin()
  {
    return min;
  }

  public void setMin(Long min)
  {
    this.min = min;
  }

  public Long getMax()
  {
    return max;
  }

  public void setMax(Long max)
  {
    this.max = max;
  }

  public Long getMinDelta()
  {
    return minDelta;
  }

  public void setMinDelta(Long minDelta)
  {
    this.minDelta = minDelta;
  }

  public Long getMaxDelta()
  {
    return maxDelta;
  }

  public void setMaxDelta(Long maxDelta)
  {
    this.maxDelta = maxDelta;
  }

  public ParamQuality getQuality()
  {
    return quality;
  }

  public void setQuality(ParamQuality quality)
  {
    this.quality = quality;
  }

  public Long getMa()
  {
    return ma;
  }

  public void setMa(Long ma)
  {
    this.ma = ma;
  }

  public Long getMv()
  {
    return mv;
  }

  public void setMv(Long mv)
  {
    this.mv = mv;
  }

  public Long getMk()
  {
    return mk;
  }

  public void setMk(Long mk)
  {
    this.mk = mk;
  }

  public String getRadicals()
  {
    return radicals;
  }

  public void setRadicals(String radicals)
  {
    this.radicals = radicals;
  }

  public Set<ParamEntry> getEntries()
  {
    return entries;
  }

  public void setEntries(Set<ParamEntry> entries)
  {
    this.entries = entries;
  }

  public Date getDateInsert()
  {
    return dateInsert;
  }

  public void setDateInsert(Date date)
  {
    this.dateInsert = date;
  }

  public Date getDateUpdate()
  {
    return dateUpdate;
  }

  public void setDateUpdate(Date date)
  {
    this.dateUpdate = date;
  }

  public void trim()
  {
    this.uid = (uid != null ? uid.trim() : null);
    this.title = (title != null ? title.trim() : null);
  }
}