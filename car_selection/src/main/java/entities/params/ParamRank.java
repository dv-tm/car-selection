package entities.params;

public enum ParamRank
{
  E(1L),
  A(2L),
  B(3L),
  C(4L);

  private Long order;

  ParamRank(Long order)
  {
    this.order = order;
  }

  public Long getOrder()
  {
    return order;
  }

  public static ParamRank getByName(String name)
  {
    if (name != null) {
      for (ParamRank value : values()) {
        if (name.equals(value.name())) {
          return value;
        }
      }
    }

    return null;
  }
}