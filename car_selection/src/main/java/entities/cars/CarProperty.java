package entities.cars;

import entities.base.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "car_props",
indexes = {@Index(name = "prop_val_idx", columnList = "data_value,location")})
public class CarProperty extends Data
{
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @ManyToOne
  @JoinColumn(name = "car_id")
  private Car car;

  @ManyToOne
  @JoinColumn(name = "prop_id")
  private Property property;

  @ManyToOne
  @JoinColumn(name = "option_id")
  private Option option;

  @Column(name = "data_value", updatable = false)
  private String data_value;

  @Column(name = "value_title", updatable = false)
  private String value_title;

  @Column(name = "location", updatable = false)
  private String location;

  @Column(name = "ext_id", updatable = false)
  private Long ext_id;

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public Car getCar()
  {
    return car;
  }

  public void setCar(Car car)
  {
    this.car = car;
  }

  public Property getProperty()
  {
    return property;
  }

  public void setProperty(Property property)
  {
    this.property = property;
  }

  public Option getOption()
  {
    return option;
  }

  public void setOption(Option option)
  {
    this.option = option;
  }

  public String getData_value()
  {
    return data_value;
  }

  public void setData_value(String data_value)
  {
    this.data_value = data_value;
  }

  public Long getExt_id()
  {
    return ext_id;
  }

  public void setExt_id(Long ext_id)
  {
    this.ext_id = ext_id;
  }

  public String getLocation()
  {
    return location;
  }

  public void setLocation(String location)
  {
    this.location = location;
  }

  public String getValue_title()
  {
    return value_title;
  }

  public void setValue_title(String value_title)
  {
    this.value_title = value_title;
  }
}