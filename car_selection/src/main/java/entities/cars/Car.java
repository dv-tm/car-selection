package entities.cars;

import entities.base.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

@Entity
@Table(name = "car",
  indexes = {@Index(name = "car_ext_id_idx", columnList = "ext_id")})
public class Car extends Data
{
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @Column(name = "title", updatable = false)
  private String title;

  @Column(name = "mark", updatable = false)
  private String mark;

  @Column(name = "model", updatable = false)
  private String model;

  @Column(name = "doors", updatable = false)
  private Integer doors;

  @Column(name = "body", updatable = false)
  private String body;

  @Column(name = "year", updatable = false)
  private String year;

  @Column(name = "unique_id", updatable = false)
  private String unique_id;

  @Column(name = "price", updatable = false)
  private Double price;

  @Column(name = "ext_id", updatable = false)
  private Long ext_id;

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public String getTitle()
  {
    return title;
  }

  public void setTitle(String title)
  {
    this.title = title;
  }

  public String getMark()
  {
    return mark;
  }

  public void setMark(String mark)
  {
    this.mark = mark;
  }

  public String getModel()
  {
    return model;
  }

  public void setModel(String model)
  {
    this.model = model;
  }

  public Integer getDoors()
  {
    return doors;
  }

  public void setDoors(Integer doors)
  {
    this.doors = doors;
  }

  public String getBody()
  {
    return body;
  }

  public void setBody(String body)
  {
    this.body = body;
  }

  public String getYear()
  {
    return year;
  }

  public void setYear(String year)
  {
    this.year = year;
  }

  public String getUnique_id()
  {
    return unique_id;
  }

  public void setUnique_id(String unique_id)
  {
    this.unique_id = unique_id;
  }

  public Double getPrice()
  {
    return price;
  }

  public void setPrice(Double price)
  {
    this.price = price;
  }

  public Long getExt_id()
  {
    return ext_id;
  }

  public void setExt_id(Long ext_id)
  {
    this.ext_id = ext_id;
  }
}