package entities.cars;

import entities.base.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

@Entity
@Table(name = "options",
  indexes = {@Index(name = "opt_ext_id_idx", columnList = "ext_id,ext_id2")})
public class Option extends Data
{
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @Column(name = "code", updatable = false)
  private String code;

  @Column(name = "title", updatable = false)
  private String title;

  @Column(name = "price", updatable = false)
  private Double price;

  //0 - опция. 1 - пакет
  @Column(name = "type", updatable = false)
  private Integer type;

  @Column(name = "ext_id", updatable = false)
  private Long ext_id;

  @Column(name = "ext_id2", updatable = false)
  private Long ext_id2;

  public Option()
  {
    // Конструктор по-умолчанию.
  }

  public Option(String code, String title, Double price, Integer type, Long ext_id, Long ext_id2)
  {
    this.code = code;
    this.title = title;
    this.price = price;
    this.type = type;
    this.ext_id = ext_id;
    this.ext_id2 = ext_id2;
  }

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public String getTitle()
  {
    return title;
  }

  public void setTitle(String title)
  {
    this.title = title;
  }

  public Double getPrice()
  {
    return price;
  }

  public void setPrice(Double price)
  {
    this.price = price;
  }

  public Integer getType()
  {
    return type;
  }

  public void setType(Integer type)
  {
    this.type = type;
  }

  public Long getExt_id()
  {
    return ext_id;
  }

  public void setExt_id(Long ext_id)
  {
    this.ext_id = ext_id;
  }
}