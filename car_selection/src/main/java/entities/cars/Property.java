package entities.cars;

import com.fasterxml.jackson.annotation.JsonIgnore;
import entities.base.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "props",
  indexes = {@Index(name = "prop_ext_id_idx", columnList = "ext_id")})
public class Property extends Data
{
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @Column(name = "parent_id")
  private Long parent_id;

  @Column(name = "title", updatable = false)
  private String title;

  @JsonIgnore
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "group_id")
  private PropertyGroup group;

  @Column(name = "ext_id", updatable = false)
  private Long ext_id;

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public Long getParent_id()
  {
    return parent_id;
  }

  public void setParent_id(Long parent_id)
  {
    this.parent_id = parent_id;
  }

  public String getTitle()
  {
    return title;
  }

  public void setTitle(String title)
  {
    this.title = title;
  }

  public PropertyGroup getGroup()
  {
    return group;
  }

  public void setGroup(PropertyGroup group)
  {
    this.group = group;
  }

  public Long getExt_id()
  {
    return ext_id;
  }

  public void setExt_id(Long ext_id)
  {
    this.ext_id = ext_id;
  }
}