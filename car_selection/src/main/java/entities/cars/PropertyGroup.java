package entities.cars;

import entities.base.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Table(name = "prop_groups")
public class PropertyGroup extends Data
{
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Integer id;

  @Column(name = "title", updatable = false)
  private String title;

  @OneToMany(mappedBy = "group", fetch = FetchType.EAGER)
  private Set<Property> props;

  public Integer getId()
  {
    return id;
  }

  public void setId(Integer id)
  {
    this.id = id;
  }

  public String getTitle()
  {
    return title;
  }

  public void setTitle(String title)
  {
    this.title = title;
  }

  public Set<Property> getProps()
  {
    return props;
  }

  public void setProps(Set<Property> props)
  {
    this.props = props;
  }
}