package entities.rules;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ARule
{
  MIN("A >> Min"),
  MAX("A >> Max");

  private String title;

  ARule(String title)
  {
    this.title = title;
  }

  public String getTitle()
  {
    return title;
  }

  public String getName()
  {
    return this.name();
  }

  public static ARule getByName(String name)
  {
    if (name != null) {
      for (ARule value : values()) {
        if (name.equals(value.name())) {
          return value;
        }
      }
    }

    return null;
  }
}