package entities.rules;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum PRule
{
  PROFIT("Profit"),
  NONE("None");

  private String title;

  PRule(String title)
  {
    this.title = title;
  }

  public String getTitle()
  {
    return title;
  }

  public String getName()
  {
    return this.name();
  }

  public static PRule getByName(String name)
  {
    if (name != null) {
      for (PRule value : values()) {
        if (name.equals(value.name())) {
          return value;
        }
      }
    }

    return null;
  }
}