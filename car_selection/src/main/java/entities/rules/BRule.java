package entities.rules;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum BRule
{
  ZERO("B >> 0"),
  MIN("B >> Min"),
  MAX("B >> Max");

  private String title;

  BRule(String title)
  {
    this.title = title;
  }

  public String getTitle()
  {
    return title;
  }

  public String getName()
  {
    return this.name();
  }

  public static BRule getByName(String name)
  {
    if (name != null) {
      for (BRule value : values()) {
        if (name.equals(value.name())) {
          return value;
        }
      }
    }

    return null;
  }
}