package entities.base;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public abstract class Repo<T extends Data>
{
  @Autowired
  private SessionFactory sessionFactory;

  /** Поддерживаемый класс. */
  public abstract Class support();

  /** Получение сессии. */
  protected Session session()
  {
    return sessionFactory.getCurrentSession();
  }

  /** Создание критериев выборки. */
  protected Criteria criteria()
  {
    return session().createCriteria(this.support());
  }

  /** Получение объекта. */
  public T one(Long id)
  {
    return (T) session().get(support(), id);
  }

  /** Получение объекта с прогруженными полями. */
  public T one(Long id, String[] fields)
  {
    Criteria criteria = this.criteria();

    for (String name : fields) {
      criteria.setFetchMode(name, FetchMode.JOIN);
    }

    criteria.add(Restrictions.eq("id", id));
    List<T> objects = criteria.list();

    return objects.isEmpty() ? null : objects.get(0);
  }

  /** Сохранение объекта. */
  public void save(T object)
  {
    session().save(object);
  }

  /** Изменение объекта. */
  public void update(T object)
  {
    session().update(object);
  }

  /** Удаление объекта. */
  public void delete(T object)
  {
    session().delete(object);
  }

  /** Поиск объектов. */
  public List<T> find(T object)
  {
    return find(object, new String[]{});
  }

  /** Поиск объектов и прогрузка их полей. */
  public List<T> find(T object, String[] fields)
  {
    Criteria criteria = this.criteria();

    for (String name : fields) {
      criteria.setFetchMode(name, FetchMode.JOIN);
    }

    criteria.add(Example.create(object).excludeZeroes());
    criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

    return criteria.list();
  }

  /** Получение объекта. */
  public T first(T object)
  {
    return this.first(object, new String[]{});
  }

  /** Получение объекта с прогруженными полями. */
  public T first(T object, String[] fields)
  {
    List<T> objects = this.find(object, fields);

    return objects.isEmpty() ? null : objects.get(0);
  }

  /** Получение списка объектов. */
  public List<T> list(boolean direct, String... orders)
  {
    return this.list(direct, orders, new String[]{});
  }

  /** Получение списка объектов с прогруженными полями. */
  public List<T> list(boolean direct, String[] orders, String[] fields)
  {
    Criteria criteria = this.criteria();

    if (direct) {
      for (String order : orders) {
        criteria.addOrder(Order.asc(order));
      }
    } else {
      if (orders.length == 0) {
        orders = new String[]{"id"};
      }

      for (String order : orders) {
        criteria.addOrder(Order.desc(order));
      }
    }

    for (String name : fields) {
      criteria.setFetchMode(name, FetchMode.JOIN);
    }

    criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

    return criteria.list();
  }
}