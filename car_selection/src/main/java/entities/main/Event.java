package entities.main;

import app.HandleException;
import app.Messages;
import app.Patterns;
import entities.base.Data;
import entities.params.ParamPerfect;
import entities.params.ParamRank;
import entities.params.ParamVersion;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Entity
@Table(name = "events")
public class Event extends Data
{
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @NotNull(message = Messages.EVENT_TITLE_WRONG)
  @Size(min = 2, max = 80, message = Messages.EVENT_TITLE_WRONG)
  @Column(name = "title", length = 80, nullable = false)
  private String title;

  @Enumerated(EnumType.STRING)
  @NotNull(message = Messages.EVENT_MODALITY_WRONG)
  @Column(name = "modality", nullable = false)
  private Modality modality;

  @NotNull(message = Messages.EVENT_RADICALS_WRONG)
  @Size(min = 1, max = 20, message = Messages.EVENT_RADICALS_WRONG)
  @Column(name = "radicals", length = 20, nullable = false)
  private String radicals;

  @NotNull(message = Messages.EVENT_MONEY_WRONG)
  @Column(name = "money", nullable = false)
  private Long money;

  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(name = "event_scenarios", joinColumns = @JoinColumn(name = "event_id"),
    inverseJoinColumns = @JoinColumn(name = "scenario_id"))
  private Set<Scenario> scenarios;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "sequence_id")
  private Sequence sequence;

  @OnDelete(action = OnDeleteAction.CASCADE)
  @OneToMany(mappedBy = "event", fetch = FetchType.LAZY)
  private Set<ParamVersion> params;

  @CreationTimestamp
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "date_insert", nullable = false, updatable = false)
  private Date dateInsert;

  @UpdateTimestamp
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "date_update", nullable = false, updatable = true)
  private Date dateUpdate;

  public Event()
  {
    // Конструктор по-умолчанию.
  }

  public Event(Long id)
  {
    this.id = id;
  }

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public String getTitle()
  {
    return title;
  }

  public void setTitle(String title)
  {
    this.title = title;
  }

  public Modality getModality()
  {
    return modality;
  }

  public void setModality(Modality modality)
  {
    this.modality = modality;
  }

  public String getRadicals()
  {
    return radicals;
  }

  public void setRadicals(String radicals)
  {
    this.radicals = radicals;
  }

  public Long getMoney()
  {
    return money;
  }

  public void setMoney(Long money)
  {
    this.money = money;
  }

  public Set<Scenario> getScenarios()
  {
    return scenarios;
  }

  public void setScenarios(Set<Scenario> scenarios)
  {
    this.scenarios = scenarios;
  }

  public Sequence getSequence()
  {
    return sequence;
  }

  public void setSequence(Sequence sequence)
  {
    this.sequence = sequence;
  }

  public Set<ParamVersion> getParams()
  {
    return params;
  }

  public void setParams(Set<ParamVersion> params)
  {
    this.params = params;
  }

  public Date getDateInsert()
  {
    return dateInsert;
  }

  public void setDateInsert(Date date)
  {
    this.dateInsert = date;
  }

  public Date getDateUpdate()
  {
    return dateUpdate;
  }

  public void setDateUpdate(Date date)
  {
    this.dateUpdate = date;
  }

  public void trim()
  {
    this.title = (title != null ? title.trim() : null);
    this.radicals = (radicals != null ? radicals.trim() : null);
  }

  // Получение идеальной сущности - списка параметров.
  public List<ParamPerfect> perfects() throws HandleException
  {
    List<ParamPerfect> perfects = new ArrayList<>();
    Map<Long, ParamPerfect> perfectsMap = new HashMap<>();

    Long id;
    ParamPerfect perfect;

    // Версии параметров из сценариев.
    for (Scenario scenario : scenarios) {
      for (ParamVersion version : scenario.getParams()) {
        id = version.getParam().getId();
        perfect = perfectsMap.get(id);

        if (perfect == null) {
          perfect = new ParamPerfect();
          perfect.update(version, ParamRank.B);
          perfectsMap.put(id, perfect);
        } else {
          perfect.increasePriority();
          perfect.merge(version);
        }
      }
    }

    // Версии параметров из секвенции.
    for (ParamVersion version : sequence.getParams()) {
      id = version.getParam().getId();
      perfect = perfectsMap.get(id);

      if (perfect == null) {
        perfect = new ParamPerfect();
        perfect.update(version, ParamRank.C);
        perfectsMap.put(id, perfect);
      } else {
        perfect.increasePriority();

        // Параметр есть в сценариях, значения из секвенции приоритетнее.
        // Значения из секвенции более соответствуют радикальному профилю.
        if (perfect.getRank() == ParamRank.B) {
          perfect.update(version, ParamRank.A);
        }
      }
    }

    // Версии параметров из эвента.
    for (ParamVersion version : params) {
      perfect = new ParamPerfect();
      perfect.update(version, ParamRank.E);
      perfects.add(perfect);

      { // Поиск в рангах A, B, C.
        id = version.getParam().getId();
        ParamPerfect param = perfectsMap.get(id);

        // Параметр присутствует в сценариях и секвенции.
        if (param != null && param.getRank() == ParamRank.A) {
          perfect.setMin(param.getMin());
          perfect.setMax(param.getMax());
          perfect.setMinDelta(param.getMinDelta());
          perfect.setMaxDelta(param.getMaxDelta());
          perfect.merge(version);
        }
      }
    }

    perfects.addAll(perfectsMap.values());

    Collections.sort(perfects, new Comparator<ParamPerfect>()
    {
      @Override
      public int compare(ParamPerfect prev, ParamPerfect next)
      {
        Long prevRank = prev.getRank().getOrder();
        Long nextRank = next.getRank().getOrder();

        if (prevRank.equals(nextRank)) {
          return prev.getPriority() >= next.getPriority() ? 1 : -1;
        }

        return prevRank > nextRank ? 1 : -1;
      }
    });

    return perfects;
  }
}