package entities.main;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum Modality
{
  A("Аудиал"),
  V("Визуал"),
  K("Кинистет");

  private String title;

  Modality(String title)
  {
    this.title = title;
  }

  public String getTitle()
  {
    return title;
  }

  public String getName()
  {
    return this.name();
  }

  public static Modality getByName(String name)
  {
    if (name != null) {
      for (Modality value : values()) {
        if (name.equals(value.name())) {
          return value;
        }
      }
    }

    return null;
  }
}