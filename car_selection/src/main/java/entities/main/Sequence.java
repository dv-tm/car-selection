package entities.main;

import app.Messages;
import com.fasterxml.jackson.annotation.JsonProperty;
import entities.base.Data;
import entities.params.ParamVersion;
import entities.rules.ARule;
import entities.rules.BRule;
import entities.rules.PRule;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "sequences")
public class Sequence extends Data
{
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @NotNull(message = Messages.SEQUENCE_TITLE_WRONG)
  @Size(min = 2, max = 80, message = Messages.SEQUENCE_TITLE_WRONG)
  @Column(name = "title", length = 80, nullable = false)
  private String title;

  @NotNull(message = Messages.SEQUENCE_RADICALS_WRONG)
  @Size(min = 1, max = 20, message = Messages.SEQUENCE_RADICALS_WRONG)
  @Column(name = "radicals", length = 20, unique = true, nullable = true)
  private String radicals;

  @OnDelete(action = OnDeleteAction.CASCADE)
  @OneToMany(mappedBy = "sequence", cascade = CascadeType.ALL)
  private Set<ParamVersion> params;

  @JsonProperty("ARule")
  @Enumerated(EnumType.STRING)
  @NotNull(message = Messages.SEQUENCE_RULE_A_WRONG)
  @Column(name = "rule_a", length = 16, nullable = false)
  private ARule aRule;

  @JsonProperty("BRule")
  @Enumerated(EnumType.STRING)
  @NotNull(message = Messages.SEQUENCE_RULE_B_WRONG)
  @Column(name = "rule_b", length = 16, nullable = false)
  private BRule bRule;

  @JsonProperty("PRule")
  @Enumerated(EnumType.STRING)
  @NotNull(message = Messages.SEQUENCE_RULE_P_WRONG)
  @Column(name = "rule_p", length = 16, nullable = false)
  private PRule pRule;

  @CreationTimestamp
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "date_insert", nullable = false, updatable = false)
  private Date dateInsert;

  @UpdateTimestamp
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "date_update", nullable = false, updatable = true)
  private Date dateUpdate;

  public Sequence()
  {
    // Конструктор по-умолчанию.
  }

  public Sequence(Long id)
  {
    this.id = id;
  }

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public String getTitle()
  {
    return title;
  }

  public void setTitle(String title)
  {
    this.title = title;
  }

  public String getRadicals()
  {
    return radicals;
  }

  public void setRadicals(String radicals)
  {
    this.radicals = radicals;
  }

  public Set<ParamVersion> getParams()
  {
    return params;
  }

  public void setParams(Set<ParamVersion> params)
  {
    this.params = params;
  }

  public ARule getARule()
  {
    return aRule;
  }

  public void setARule(ARule rule)
  {
    this.aRule = rule;
  }

  public BRule getBRule()
  {
    return bRule;
  }

  public void setBRule(BRule rule)
  {
    this.bRule = rule;
  }

  public PRule getPRule()
  {
    return pRule;
  }

  public void setPRule(PRule rule)
  {
    this.pRule = rule;
  }

  public Date getDateInsert()
  {
    return dateInsert;
  }

  public void setDateInsert(Date date)
  {
    this.dateInsert = date;
  }

  public Date getDateUpdate()
  {
    return dateUpdate;
  }

  public void setDateUpdate(Date date)
  {
    this.dateUpdate = date;
  }

  public void trim()
  {
    this.title = (title != null ? title.trim() : null);
    this.radicals = (radicals != null ? radicals.trim() : null);
  }
}