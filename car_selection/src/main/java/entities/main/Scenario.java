package entities.main;

import app.Messages;
import entities.base.Data;
import entities.params.ParamVersion;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "scenarios")
public class Scenario extends Data
{
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @NotNull(message = Messages.SCENARIO_UID_WRONG)
  @Size(min = 1, max = 12, message = Messages.SCENARIO_UID_WRONG)
  @Column(name = "uid", length = 12, unique = true, nullable = false)
  private String uid;

  @NotNull(message = Messages.SCENARIO_TITLE_WRONG)
  @Size(min = 2, max = 80, message = Messages.SCENARIO_TITLE_WRONG)
  @Column(name = "title", length = 80, nullable = false)
  private String title;

  @OnDelete(action = OnDeleteAction.CASCADE)
  @OneToMany(mappedBy = "scenario", cascade = CascadeType.ALL)
  private Set<ParamVersion> params;

  @CreationTimestamp
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "date_insert", nullable = false, updatable = false)
  private Date dateInsert;

  @UpdateTimestamp
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "date_update", nullable = false, updatable = true)
  private Date dateUpdate;

  public Scenario()
  {
    // Конструктор по-умолчанию.
  }

  public Scenario(Long id)
  {
    this.id = id;
  }

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public String getUid()
  {
    return uid;
  }

  public void setUid(String uid)
  {
    this.uid = uid;
  }

  public String getTitle()
  {
    return title;
  }

  public void setTitle(String title)
  {
    this.title = title;
  }

  public Set<ParamVersion> getParams()
  {
    return params;
  }

  public void setParams(Set<ParamVersion> params)
  {
    this.params = params;
  }

  public Date getDateInsert()
  {
    return dateInsert;
  }

  public void setDateInsert(Date date)
  {
    this.dateInsert = date;
  }

  public Date getDateUpdate()
  {
    return dateUpdate;
  }

  public void setDateUpdate(Date date)
  {
    this.dateUpdate = date;
  }

  public void trim()
  {
    this.uid = (uid != null ? uid.trim() : null);
    this.title = (title != null ? title.trim() : null);
  }
}