package entities.binds;

import app.Messages;
import entities.base.Data;
import entities.cars.Property;
import entities.params.Param;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "prop_binds", uniqueConstraints = {
  @UniqueConstraint(columnNames = {"prop_id", "param_id", "prop_value", "location"})
})
public class PropertyPair extends Data
{
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @ManyToOne(fetch = FetchType.EAGER)
  @OnDelete(action = OnDeleteAction.CASCADE)
  @NotNull(message = Messages.MAP_PROPERTY_MISSED)
  @JoinColumn(name = "prop_id")
  private Property prop;

  @ManyToOne(fetch = FetchType.EAGER)
  @OnDelete(action = OnDeleteAction.CASCADE)
  @NotNull(message = Messages.MAP_PARAM_MISSED)
  @JoinColumn(name = "param_id")
  private Param param;

  @NotNull(message = Messages.PARAM_VALUE_WRONG)
  @Column(name = "prop_value", nullable = false)
  private String propValue;

  private String value_title;

  @Column(name = "param_value")
  private Long paramValue;

  @Column(name = "location")
  private String location;

  @CreationTimestamp
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "date_insert", nullable = false, updatable = false)
  private Date dateInsert;

  @UpdateTimestamp
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "date_update", nullable = false, updatable = true)
  private Date dateUpdate;

  public PropertyPair()
  {
    // Конструктор по-умолчанию.
  }

  public PropertyPair(Long id)
  {
    this.id = id;
  }

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public Property getProp()
  {
    return prop;
  }

  public void setProp(Property prop)
  {
    this.prop = prop;
  }

  public Param getParam()
  {
    return param;
  }

  public void setParam(Param param)
  {
    this.param = param;
  }

  public String getPropValue()
  {
    return propValue;
  }

  public void setPropValue(String propValue)
  {
    this.propValue = propValue;
  }

  public Long getParamValue()
  {
    return paramValue;
  }

  public void setParamValue(Long paramValue)
  {
    this.paramValue = paramValue;
  }

  public String getLocation()
  {
    return location;
  }

  public void setLocation(String location)
  {
    this.location = location;
  }

  public Date getDateInsert()
  {
    return dateInsert;
  }

  public void setDateInsert(Date date)
  {
    this.dateInsert = date;
  }

  public Date getDateUpdate()
  {
    return dateUpdate;
  }

  public void setDateUpdate(Date date)
  {
    this.dateUpdate = date;
  }

  public String getValue_title()
  {
    return value_title;
  }
}