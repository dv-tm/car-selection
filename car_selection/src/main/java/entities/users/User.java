package entities.users;

import app.Messages;
import app.Patterns;
import com.fasterxml.jackson.annotation.JsonIgnore;
import entities.base.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name = "users")
public class User extends Data
{
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @NotNull(message = Messages.USER_NAME_WRONG)
  @Size(max = 40, message = Messages.USER_NAME_LENGTH)
  @Pattern(regexp = Patterns.EMAIL, message = Messages.USER_NAME_WRONG)
  @Column(name = "username", length = 40, unique = true, nullable = false, updatable = false)
  private String username;

  @Transient
  @NotNull(message = Messages.USER_PASSWORD_WRONG)
  @Pattern(regexp = Patterns.PASSWORD, message = Messages.USER_PASSWORD_WRONG)
  private String password;

  @JsonIgnore
  @NotNull(message = Messages.USER_PASSWORD_WRONG)
  @Column(name = "password", length = 100, nullable = false)
  private String hash;

  @NotNull(message = Messages.USER_ALIAS_WRONG)
  @Size(max = 40, message = Messages.USER_ALIAS_WRONG)
  @Column(name = "alias", length = 40, nullable = false)
  private String alias;

  @CreationTimestamp
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "date_insert", nullable = false, updatable = false)
  private Date dateInsert;

  @UpdateTimestamp
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "date_update", nullable = false, updatable = true)
  private Date dateUpdate;

  public User()
  {
    // Конструктор по-умолчанию.
  }

  public User(Long id)
  {
    this.id = id;
  }

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public String getUsername()
  {
    return username;
  }

  public void setUsername(String username)
  {
    this.username = username;
  }

  public String getPassword()
  {
    return password;
  }

  public void setPassword(String password)
  {
    this.password = password;
  }

  @JsonIgnore
  public String getHash()
  {
    return hash;
  }

  public void setHash(String hash)
  {
    this.hash = hash;
  }

  public String getAlias()
  {
    return alias;
  }

  public void setAlias(String alias)
  {
    this.alias = alias;
  }

  public Date getDateInsert()
  {
    return dateInsert;
  }

  public void setDateInsert(Date date)
  {
    this.dateInsert = date;
  }

  public Date getDateUpdate()
  {
    return dateUpdate;
  }

  public void setDateUpdate(Date date)
  {
    this.dateUpdate = date;
  }
}